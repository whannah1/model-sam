begin

    src_dir = "/maloney-scratch/whannah/SAM/6.9.5_ent_LP/"
    des_dir = "/maloney-scratch/whannah/SAM/"

    if .not.isvar("case_stub")
    case_stub = "BUBBLE"
    case_tail = "500_144x64_04km_"+(/"0.5k_3g","1.0k_3g","0.5k_1g","1.0k_1g"/)
    end if

    case = case_stub +"_"+ case_tail


    nc_only = False

do c = 0,dimsizes(case)-1
    ;------------------------------
    ; Check for directory tree
    ;------------------------------
    odir   = des_dir+case(c)+"/"
    odir2D = odir+"OUT_2D/"
    odir3D = odir+"OUT_3D/"
    odirST = odir+"OUT_STAT/"
    if .not.isfilepresent(odir) then
    system("mkdir "+odir)
    end if
    if .not.isfilepresent(odir2D) then system("mkdir "+odir2D) end if
    if .not.isfilepresent(odir3D) then system("mkdir "+odir3D) end if
    if .not.isfilepresent(odirST) then system("mkdir "+odirST) end if
    ;------------------------------
    ; Move data
    ;------------------------------
    if nc_only then
        ifile = src_dir+"/OUT_2D/"+case(c)+"*.nc"
        system("mv "+ifile+" "+odir2D)
        ifile = src_dir+"/OUT_STAT/"+case(c)+"*.nc"
        system("mv "+ifile+" "+odirST)
        ifile = src_dir+"/OUT_3D/*"+case(c)+"*.nc"
        system("mv "+ifile+" "+odir3D)
        ifile = src_dir+"/OUT_3D/*"+case_stub+"*ENTRAIN*"+case_tail(c)+"*nc"
        system("mv "+ifile+" "+odir3D)
    else 
        ifile = src_dir+"/OUT_2D/*"+case(c)+"*"
        system("mv "+ifile+" "+odir2D)
        ifile = src_dir+"/OUT_STAT/*"+case(c)+"*"
        system("mv "+ifile+" "+odirST)
        ifile = src_dir+"/OUT_3D/*"+case(c)+"*"
        system("mv "+ifile+" "+odir3D)  
        ifile = src_dir+"/OUT_3D/*"+case_stub+"*ENTRAIN*"+case_tail(c)+"*nc"
        system("mv "+ifile+" "+odir3D)
    end if
    ;------------------------------
    ;------------------------------
end do

end
