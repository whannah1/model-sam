MODULE lagrangian_parcel_tracker

! Version 3.8.2
! Takanobu Yamaguchi
! tak.yamaguchi@noaa.gov
!
! List of modified SAM SRC files (Consider dependency to "USE" LPT):
! - main.f90: USE LPT, CALL lpt_set, and CALL lpt
! - params.f90: dolpt (logical switch)
! - setparm.f90: dolpt in namelist
! - tke_full.f90: collect SGS TKE dissipation rate
!
! What is new since 3.8.1
! - Updated for SAM6.9.6

	USE domain, ONLY: YES3D, nx_gl, ny_gl, nsubdomains_x, nsubdomains_y, ntracers
	USE grid, ONLY: nx, ny, nz, nzm, nsubdomains, RUN3D, nxp1, nyp1, z, pres, zi, nstep, ncycle, &
	                icycle, dtn, day, rank, ranknn, rankss, rankee, rankww, rankne, ranknw, rankse, &
	                ranksw, dompi, masterproc, case, dx, dy, dz, nstop, nelapse, dt, day0, nrad, &
	                nstat, restart_sep, nrestart_skip, output_sep, caseid
	                ! order appeared in grid.f90 
	USE vars, ONLY: u, v, w, t, tke, p, qv, qcl, qpl, qci, qpi, u0, v0, rho, gamaz, qsatw, qsati, &
	                dtqsatw, dtqsati, lenstr
	                ! order appeared in vars.f90
	USE params, ONLY: cp, ggr, rgas, fac_cond, fac_fus, fac_sub, epsv, ug, vg, dotracers
	                  ! order appeard in params.f90
	USE microphysics, ONLY: nmicro_fields, micro_field, mkname, mklongname, mkunits, mkoutputscale
	
	USE tracers, ONLY: tracer
	
	IMPLICIT NONE
	
	PRIVATE   ! Avoid confusion
	PUBLIC :: lpt_set, lpt   ! subroutines called in main.f90
	PUBLIC :: klpt, kbottom, ktop   ! integer for vertical loop used to collect data
	PUBLIC :: flag_lpt_3Dget, flag_lpt_sgs ! logical flags used to collect data
	PUBLIC :: diss3D   ! array used to collect data
!!	SAVE   ! TRY COMMENTING IN IF UNDEFINED ARRAYS EXIST
	
	!===============================================================================================
	!                                USER SPECIFIED PARAMETERS
	!===============================================================================================
	! Set either sigle (4) or double (8) precision
	INTEGER, PARAMETER :: rlpt = SELECTED_REAL_KIND(8)
	
	! Number of parcels in x & y direction for each subdomain: Set np_y=1 for 2D run.
	INTEGER, PARAMETER :: np_x = 2
	INTEGER, PARAMETER :: np_y = 2
	
	! Number of parcels placed on the same point
	INTEGER, PARAMETER :: np_xy = 1
	
	! Number of vertical level for placing parcels
	INTEGER, PARAMETER :: nzl = 4
	
	! Vertical distance for placing parcels [m]
	REAL(KIND=rlpt), PARAMETER :: dzl = 400.0
	
	! Highest vertical level to place parcels [m]
	REAL(KIND=rlpt), PARAMETER :: zl_top = 2000.0
	
	! Highest and lowest altitude limits: Parcel's vertical position will be initialized.
	! Choose ztop at least several levels below the model top.
	! Choose zbottom at least higher than the first scalar level from the surface.
	REAL(KIND=rlpt), PARAMETER :: ztop    = 25000.0
	REAL(KIND=rlpt), PARAMETER :: zbottom =    20.0
	
	! Initial placement
	! Set FALSE if parcels are placed way you code in the subroutine lpt_place_parcel.
	LOGICAL, PARAMETER :: homogeneous = .TRUE.
	
	! Introducing time step: Parcels move one time step after the initialization
	INTEGER, PARAMETER :: nstep_lpt_init = 2
	
	! Save data every nsave_lpt steps
	INTEGER, PARAMETER :: nsave_lpt = 15
	
	! Number of histories per one output file: Set -1 to append all histories in one output file.
	INTEGER, PARAMETER :: nhistory = -1
	
	! Grid space of x (y) direction = dx (dy): Set grid_space_y=grid_space_x for 2D run.
	! Grid space of z (dz = 2 * first level in grid.f90): Significant if uniforrm_z=.TRUE.
	REAL(KIND=rlpt), PARAMETER :: grid_space_x = 400.0
	REAL(KIND=rlpt), PARAMETER :: grid_space_y = 400.0
	REAL(KIND=rlpt), PARAMETER :: grid_space_z = 5.0
	
	! Uniform vertical grid between ztop and zbottom? true or false
	LOGICAL, PARAMETER :: uniform_z = .FALSE. !.TRUE.
	
	! Spatial interpolation scheme
	! 2 - trilinear interplolation
	! 3 - 3rd-order Lagrange interpolation
	! 5 - 5th-order Lagrange interpolation
	INTEGER, PARAMETER :: poly_order = 3
	
	! Number of iteration to find velocity at t=t+1/2.
	! If set 0, the parcel position is updated with the forward scheme: x(t+1)=x(t)+u(t)*dt
	INTEGER, PARAMETER :: n_uvw_iteration = 3
	
	! SGS velocity calculation?
	! Set 1 for SGS calculation, set 0 for non-SGS calculation
	INTEGER, PARAMETER :: yes_sgsuvw = 0
	
	! Subsidence for PBL simulations?
	! Set 0 or specified large scale divergence [s-1]
	! Subsidence is calculated as [ - divergence x parcel_height ].
	! More complicated subsidence requires code modifications.
	LOGICAL, PARAMETER :: subsidence = .FALSE.
	REAL(KIND=rlpt), PARAMETER :: divergence = 0.0 !3.75E-6
	
	! Lagrangian column tracking with specified horizontal velocity?
	LOGICAL, PARAMETER :: flag_fixed_uv = .FALSE.
	REAL(KIND=rlpt), PARAMETER :: fixed_u = 0.0
	REAL(KIND=rlpt), PARAMETER :: fixed_v = 0.0
	
	! Logical flags for output variables. Set true or false.
	LOGICAL, PARAMETER :: flag_lpt_pres   = .FALSE.   ! pressure [Pa]
	LOGICAL, PARAMETER :: flag_lpt_rho    = .FALSE.   ! density [kg/m3]
	LOGICAL, PARAMETER :: flag_lpt_pprime = .FALSE.   ! pressure perturbation [Pa]
	LOGICAL, PARAMETER :: flag_lpt_lwse   = .FALSE.   ! liquid water static energy [K]
	LOGICAL, PARAMETER :: flag_lpt_mse    = .TRUE.    ! moist static energy [K]
	LOGICAL, PARAMETER :: flag_lpt_dse    = .FALSE.   ! dry static energy [K]
	LOGICAL, PARAMETER :: flag_lpt_vdse   = .FALSE.   ! virtual dry static energy [K]
	LOGICAL, PARAMETER :: flag_lpt_smse   = .FALSE.   ! saturation moist static energy [K]
	LOGICAL, PARAMETER :: flag_lpt_tabs   = .FALSE.    ! absolute temperature [K]
	LOGICAL, PARAMETER :: flag_lpt_theta  = .FALSE.   ! potential temperature [K]
	LOGICAL, PARAMETER :: flag_lpt_thetal = .FALSE.   ! liquid water potential temperature [K]
	LOGICAL, PARAMETER :: flag_lpt_thetav = .FALSE.   ! virtual potential temperature [K]
	LOGICAL, PARAMETER :: flag_lpt_thetae = .FALSE.    ! equivalent potential temperature [K]
	LOGICAL, PARAMETER :: flag_lpt_qt     = .FALSE.   ! total nonprecipitating water [g/kg]
	LOGICAL, PARAMETER :: flag_lpt_qv     = .FALSE.    ! water vapor [g/kg]
	LOGICAL, PARAMETER :: flag_lpt_qcl    = .FALSE.    ! cloud water [g/kg]
	LOGICAL, PARAMETER :: flag_lpt_qci    = .FALSE.    ! cloud ice [g/kg]
	LOGICAL, PARAMETER :: flag_lpt_qn     = .FALSE.   ! cloud water + ice [g/kg]
	LOGICAL, PARAMETER :: flag_lpt_qpl    = .FALSE.   ! rain water [g/kg]
	LOGICAL, PARAMETER :: flag_lpt_qpi    = .FALSE.   ! snow + graupel [g/kg]
	LOGICAL, PARAMETER :: flag_lpt_qp     = .FALSE.   ! rain + snow + graupel [g/kg]
	LOGICAL, PARAMETER :: flag_lpt_qsat   = .FALSE.   ! saturation mixing ratio [g/kg]
	LOGICAL, PARAMETER :: flag_lpt_relh   = .FALSE.   ! relative humidity
	LOGICAL, PARAMETER :: flag_lpt_tke    = .FALSE.   ! parameterized ensemble-mean resolved scale TKE [m2/s2]
	LOGICAL, PARAMETER :: flag_lpt_tkes   = .FALSE.   ! SGS TKE [m2/s2]
	LOGICAL, PARAMETER :: flag_lpt_gradri = .FALSE.   ! gradient Richardson number
	
	! Number of output variables: count the total number of the logical flags with true.
	INTEGER, PARAMETER :: nvarout = 1
	
	! Diagnose thermodynamic variable with the code based on MICRO_SAM1MOM?
	LOGICAL, PARAMETER :: diag_micro_sam1mom = .FALSE.
	
	! Integer flags for micro_field (contains all prognostic variables in microphysics)
	! CAUTION: THIS CODE IS NOT COMPILED IF NMICRO_FIELDS IS NOT A PARAMETER. IN THIS CASE, USE THE
	!          EXACT INTEGER NUMBER IN STEAD OF NMICRO_FIELDS.
	! Example: Outut aerosol, cloud, & rain number concentration from MICRO_FIENGOLD2M.
	! INTEGER, PARAMETER :: flag_lpt_micro(nmicro_fields) = (/0,0,0,1,1,1,0/)
	INTEGER, PARAMETER :: flag_lpt_micro(nmicro_fields) = 0!(/0,0,0,0,0,0,0/)
	
	! Number of integer flags with 1 for micro_field
	INTEGER, PARAMETER :: nmicroout = 0
	
	! Logical flags for tracers
	LOGICAL, PARAMETER :: flag_lpt_tracers = .FALSE.    ! false if ntracers=0
	
	! Number of logical flags with true for tracer
	! Usefule if tracer physics is output. Require code modification.
	INTEGER, PARAMETER :: ntrout = 0
	
	
	!===============================================================================================
	!                                      NO EDIT BELOW
	!===============================================================================================
	! SAM uses either 4 or 8 byte real (=rsam). Real numbers between rsam and rlpt are manually
	! converted with RAEL(#,rkind) function to make sure consistency, so that the consistency is
	! obviously absolutely guaranteed by compiler.
	INTEGER, PARAMETER :: rsam = KIND(dt)
	
	
	! Horizontal additional grid points for u, v, w, and scalar arrays
	! - Parcels can be out of the current subdomain for the interpolation at half time step or
	!   guessed new position for resolved scale velocity iteration.
	! - SAM horizontal grid index orientation (s=scalar)
	!         ---------------v(i=1,j=2)----------------
	!        |                                         |
	!   u(i=1,j=1)     w(i=1,j=1), s(i=1,j=1)     u(i=2,j=1)
	!        |                                         |
	!         ---------------v(i=1,j=1)----------------
	! - All additional grid points have to be more than or equal to 1 for MPI exchange routine
!!!!!!!!!! THESE ADDITIONAL POINTS ARE FOR 5TH LAGRANGIAN POLYNOMIAL INTERPOLATION. !!!!!!!!!!!!!!!
!!!!!!!!!! IT WORKS FOR ALL AVAILABLE INTERPOLATION SCHEMES. !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!! ADD FREXIBILITY TO ADJUST DEPENDING ON INTERPOLATION SCHEME FOR FUTURE VERSION !!!!!!!!!
!!!!!!!!!! THIS REQUIRES ADJUSTMENT FOR INTERPOLATION FUNCTION & F216 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	INTEGER, PARAMETER :: ux1 = 3
	INTEGER, PARAMETER :: ux2 = 4
	INTEGER, PARAMETER :: uy1 = 3 * YES3D + 1
	INTEGER, PARAMETER :: uy2 = 3 * YES3D + 1
	INTEGER, PARAMETER :: vx1 = 3 + YES3D
	INTEGER, PARAMETER :: vx2 = 4
	INTEGER, PARAMETER :: vy1 = 2 * YES3D + 1
	INTEGER, PARAMETER :: vy2 = 3 * YES3D + 1
	INTEGER, PARAMETER :: wx1 = 4
	INTEGER, PARAMETER :: wx2 = 4
	INTEGER, PARAMETER :: wy1 = 3 * YES3D + 1
	INTEGER, PARAMETER :: wy2 = 3 * YES3D + 1
	INTEGER, PARAMETER :: sx1 = 4
	INTEGER, PARAMETER :: sx2 = 4
	INTEGER, PARAMETER :: sy1 = 3 * YES3D + 1
	INTEGER, PARAMETER :: sy2 = 3 * YES3D + 1
	
	! Horizontal indices for u, v, w, and scalar arrays
	! - Index for y direction is 1 for 2D run
	INTEGER, PARAMETER :: x1u =  1 - ux1
	INTEGER, PARAMETER :: x2u = nx + ux2
	INTEGER, PARAMETER :: y1u =  1 - uy1 * YES3D
	INTEGER, PARAMETER :: y2u = ny + uy2 * YES3D
	INTEGER, PARAMETER :: x1v =  1 - vx1
	INTEGER, PARAMETER :: x2v = nx + vx2
	INTEGER, PARAMETER :: y1v =  1 - vy1 * YES3D
	INTEGER, PARAMETER :: y2v = ny + vy2 * YES3D
	INTEGER, PARAMETER :: x1w =  1 - wx1
	INTEGER, PARAMETER :: x2w = nx + wx2
	INTEGER, PARAMETER :: y1w =  1 - wy1 * YES3D
	INTEGER, PARAMETER :: y2w = ny + wy2 * YES3D
	INTEGER, PARAMETER :: x1s =  1 - sx1
	INTEGER, PARAMETER :: x2s = nx + sx2
	INTEGER, PARAMETER :: y1s =  1 - sy1 * YES3D
	INTEGER, PARAMETER :: y2s = ny + sy2 * YES3D
	
	! Grid information
	! Subdomain grid position information
	INTEGER, SAVE :: nx_sb, ny_sb   ! total number of grid point to the current subdomain from (0,0)
	! Grid center
	REAL(KIND=rlpt), SAVE :: gpxc(-1:nx+1)
	REAL(KIND=rlpt), SAVE :: gpyc(-1:ny+1)
	REAL(KIND=rlpt), SAVE :: gpzc(nzm)
	! Grid side (wall) 
	REAL(KIND=rlpt), SAVE :: gpxs(0:nx+1)   ! 1 & nx+1 are at subdomain boundary
	REAL(KIND=rlpt), SAVE :: gpys(0:ny+1)
	REAL(KIND=rlpt), SAVE :: gpzs(nz)
	! Horiaontal width of whole domain
	REAL(KIND=rlpt), PARAMETER :: xwidth = nx_gl * grid_space_x
	REAL(KIND=rlpt), PARAMETER :: ywidth = ny_gl * grid_space_y
	! Inverse of grid space
	REAL(KIND=rlpt), PARAMETER :: idx = 1.0_rlpt / grid_space_x
	REAL(KIND=rlpt), PARAMETER :: idy = 1.0_rlpt / grid_space_y
	REAL(KIND=rlpt), PARAMETER :: idz = 1.0_rlpt / grid_space_z
	! Vertical index at ztop and zbottom.
	! jm2, jp3 is used to pass grid value to interpolation function.
	INTEGER :: ktop, kbottom, nzlpt
	INTEGER, PARAMETER :: jm2 = 2 * YES3D, jp3 = 3 * YES3D
	INTEGER, PARAMETER :: jm1 = 1 * YES3D, jp2 = 2 * YES3D
	
	! MPI
	INTEGER, SAVE :: newtype, newtype2
	INTEGER, DIMENSION(8) :: n_out, ranks
	INTEGER :: np_out, np_in
	! Field ID used for the subroutines task_exchange_lpt, task_dispatch_lpt
	! e.g., field_id_micro1 is the ID number for micro_field(:,:,:,1)
	!       field_id_micro2 is the ID number for micro_field(:,:,:,nmicro_fields)
	! List of IDs already taken
	! 1-3: U, V, W
	! 4-6: SGS TKE, SGS diss, resolved TKE
	! 11-17: P, TL, QV, QCL, QCI, QPL, QPI
	INTEGER, PARAMETER :: field_id_micro1 = 20
	INTEGER, PARAMETER :: field_id_micro2 = field_id_micro1 + nmicro_fields
	INTEGER, PARAMETER :: field_id_tracer1 = field_id_micro2 + 1
	INTEGER, PARAMETER :: field_id_tracer2 = field_id_tracer1 + ntracers
	INTEGER, PARAMETER :: field_id_newout1 = field_id_tracer2 + 1
	INTEGER, PARAMETER :: field_id_newout2 = field_id_newout1 ! there is only 1 variable so far.
	
	! Lagrangian parcel information
	INTEGER, PARAMETER :: np = np_x * np_y * np_xy * nzl ! # of parcels for subdomain at initial time
	INTEGER, PARAMETER :: np1 = nsubdomains * np         ! # of parcels for whole domain
	INTEGER, SAVE :: np2 ! # of parcels for subdomain at current time step
	INTEGER, PARAMETER :: nv0 = 6 ! # of default variables: position, resolved velocities
	INTEGER, PARAMETER :: nv1 = 3 * yes_sgsuvw ! # of SGS velocities
	INTEGER, PARAMETER :: nv2 = 1 ! # of other variable carried to next time step: initial z
	INTEGER, PARAMETER :: nv00 = 6 + 3 * yes_sgsuvw ! default number of output
	INTEGER, PARAMETER :: nv01 = nvarout + nmicroout + ntrout*ntracers  ! # of output vars
	INTEGER, PARAMETER :: nv = nv00 + nv01 ! total number of total output variables
	INTEGER, PARAMETER :: i7 = 1 + 6 * yes_sgsuvw ! var(i7)=var(7) if SGS velocity is on
	INTEGER, PARAMETER :: i8 = 1 + 7 * yes_sgsuvw
	INTEGER, PARAMETER :: i9 = 1 + 8 * yes_sgsuvw
	! Derived type pointer array for parcels
	! Number of parcels changes with time, so pointer-arrays and pointers are used in stead of actual
	! array. This is probably efficient because the linked-lists for outgoing parcels can be made
	! while counting outgoing parcels right before MPI exchages.
	TYPE parcel_pointer
		INTEGER :: tag   ! unique integer for each parcel
		INTEGER :: nvi   ! # of vertical initialization
		REAL(KIND=rlpt) :: var(nv0+nv1+nv2)
		TYPE(parcel_pointer), POINTER :: next
	END TYPE parcel_pointer
		! var(1-3): x, y, z position
		! var(4-6): u, v, w wind
		! var(7-9): SGS u, v, w wind
		! var(10-): initial z, and other (nv2)
	TYPE(parcel_pointer), SAVE, POINTER, DIMENSION(:) :: parcel
	TYPE(parcel_pointer), POINTER, DIMENSION(:) :: parcel_in
	TYPE(parcel_pointer), POINTER :: parcel_head, parcel_tail, &
		parcel_out_nn_head, parcel_out_nn_tail, parcel_out_ne_head, parcel_out_ne_tail, &
		parcel_out_ee_head, parcel_out_ee_tail, parcel_out_se_head, parcel_out_se_tail, &
		parcel_out_ss_head, parcel_out_ss_tail, parcel_out_sw_head, parcel_out_sw_tail, &
		parcel_out_ww_head, parcel_out_ww_tail, parcel_out_nw_head, parcel_out_nw_tail
	! Derived type parcel array for restart I/O and MPI
	TYPE parcel_array
		INTEGER :: tag
		INTEGER :: nvi
		REAL(KIND=rlpt) :: var(nv0+nv1+nv2)
	END TYPE parcel_array
	! Derived type parcel array for history output
	TYPE parcel_array_output
		INTEGER :: tag
		INTEGER :: nvi
		REAL(KIND=4) :: var(nv)   ! output variables are single precision
	END TYPE parcel_array_output
	TYPE(parcel_array_output), ALLOCATABLE, DIMENSION(:) :: parcel2
	! Derived type parcel array for sortig parcels
	TYPE parcel_array_sort
		INTEGER :: tag
		INTEGER :: index   ! index before sorted
	END TYPE parcel_array_sort
	
	! Arrays to store SAM variables
	! Store SAM values so that
	! - no modification will be made in SAM.
	! - conflict with different real kind between SAM and LPT is avoided.
	! nzlpt has not determined, so use allocatable array.
	! Default
	! Pressure at scalar level for output variable diagnosis
	REAL(KIND=rlpt), SAVE, ALLOCATABLE, DIMENSION(:) :: pres1D
	! Resolved scale velocity component
	REAL(KIND=rlpt), SAVE, ALLOCATABLE, DIMENSION(:,:,:) :: ures, vres, wres
	! Scalars
	REAL(KIND=rlpt), SAVE, ALLOCATABLE, DIMENSION(:,:,:) :: t3D, qv3D, qcl3D, qci3D, qpl3D, qpi3D
	
	! For SGS velocity calculation. See lpt_sgs_uvw
	REAL(KIND=rlpt), SAVE, ALLOCATABLE, DIMENSION(:,:,:) :: tke3D    ! emsemble mean resolved TKE
	REAL(KIND=rlpt), SAVE, ALLOCATABLE, DIMENSION(:,:,:,:) :: tkes3D ! SGS TKE, need two time levels
	REAL(KIND=rlpt), SAVE, ALLOCATABLE, DIMENSION(:,:,:) :: diss3D   ! SGS TKE dissipation rate
	REAL(KIND=rlpt), SAVE, ALLOCATABLE, DIMENSION(:,:,:) :: coef1, coef2 ! coefficient in lpt_sgs_uvw
	LOGICAL, SAVE, ALLOCATABLE, DIMENSION(:,:,:) :: coefs_computed ! true if the coefs are computed
	
	! Collect from SAM with flag
	REAL(KIND=rlpt), SAVE, ALLOCATABLE, DIMENSION(:,:,:) :: p3D
	REAL(KIND=rlpt), SAVE, ALLOCATABLE, DIMENSION(:,:,:,:) :: micro3D
	REAL(KIND=rlpt), SAVE, ALLOCATABLE, DIMENSION(:,:,:,:) :: tracer3D
	
	! Output variables: Thermodynamic variables are diagnosed with MICRO_SAM1MOM & statistics.f90
	REAL(KIND=rlpt), SAVE, ALLOCATABLE, DIMENSION(:,:,:) :: qt3D, qp3D   ! defualt
	LOGICAL, SAVE, ALLOCATABLE, DIMENSION(:,:,:) :: out_computed
	
	! New scalar variables, which will be computed somewhere in LPT
	REAL(KIND=rlpt), SAVE, ALLOCATABLE, DIMENSION(:,:,:) :: rig3D ! gradient Richardson number
	
	! Time index for two time level variables: (t1lpt,t2lpt)=(1,2)=>(2,1)=>(1,2)=>...
	INTEGER, SAVE :: t1lpt, t2lpt
	
	! Output information
	! File name and file extension
	CHARACTER (LEN=200), SAVE :: filename_lpt, filename_lpt_ext
	! Current number of total history length
	INTEGER, SAVE :: history_length
	! Variable names, short names, and unit
	CHARACTER (LEN=80), SAVE, DIMENSION(0:nv) :: var_lpt_name
	CHARACTER (LEN=40), SAVE, DIMENSION(0:nv) :: var_lpt_shrt, var_lpt_unit
	
	! Logical flags
	LOGICAL, PARAMETER :: flag_lpt_sgs = yes_sgsuvw == 1
	LOGICAL, SAVE :: flag_lpt_init = .FALSE.   ! flag to indicate initialization has been done
	LOGICAL, SAVE :: flag_lpt_3Dget = .FALSE.  ! flag to collect 3D variables from SAM
	LOGICAL, SAVE :: flag_lpt_output = .FALSE. ! flag to save LPT data
	
	! Variables used in other source code to collect 3D variables
	INTEGER :: klpt, nlpt   ! for loop
	REAL(KIND=rlpt), SAVE, DIMENSION(nx,ny,nzm) :: tmp3D   ! temporal array, which might be used
	
	
CONTAINS
	
	!==========================================================================================
	
	SUBROUTINE lpt_set
	
	! This subroutine has to be called in main.f90 when
	! - SAM starts
	! - dolpt = .TRUE.
	! Throughout this subroutine, the specified variables are cheked if an inconsistency exists.
	! 1. Set MPI_datatype = parcel_array.
	! 2. Obtain grid box information: actual distance of grid box center and side.
	! 3. Find ktop and kbottom, count nzlpt
	! 4. Allocate arrays
	! 5. Terminate if an error is found
	! 6. Setup output names, short names, and units. Check if nv01 == # of output scalar flags
	! 7. Set flag_lpt_init = .FALSE. or read restart data
	
		IMPLICIT NONE
		INTEGER :: i, j, k, ierr
		LOGICAL :: flag = .TRUE.
		LOGICAL :: flag_allocation = .TRUE.
		
		IF (masterproc) THEN
			PRINT*,''
			PRINT*,'========== Lagrangian Parcel Tracker =========='
			PRINT*,'LPT user specified parameters'
			PRINT*,'kind parameter for real number:', rlpt
			PRINT*,'Number of parcels in x direction (subdomain):', np_x
			PRINT*,'Number of parcels in y direction (subdomain):', np_y
			PRINT*,'Number of parcels on the same point:', np_xy
			PRINT*,'Number of vertical layers for placing parcels:', nzl
			PRINT*,'Initial height of parcels at the heighest vertical layer (m):', zl_top
			IF ( nzl > 1 ) PRINT*,'Distance of vertical layers (m):', dzl
			PRINT*,'Highest altitude limit (m):', ztop
			PRINT*,'Lowest altitude limit (m):', zbottom
			PRINT*,'LPT starts at:', nstep_lpt_init,'time steps.'
			PRINT*,'LPT saves data every', nsave_lpt,'time steps.'
			IF ( nhistory == -1 ) THEN
				PRINT*,'Output data will be saved in one file.'
			ELSE
				PRINT*,'Histrory length of each output data file:', nhistory
			ENDIF
			PRINT*,'Number of resolved scale velocity iteration:', n_uvw_iteration
			PRINT*,'LPT SGS is on:', flag_lpt_sgs
			PRINT*,'Subsidence is applied:', subsidence
			IF ( subsidence ) PRINT*,'Divergence:', divergence
			PRINT*,'Move with a fixed u and v:', flag_fixed_uv
			IF ( flag_fixed_uv ) THEN
				PRINT*,'Fixed u:', fixed_u
				PRINT*,'Fixed v:', fixed_v
			ENDIF
		ENDIF
		IF ( zl_top > ztop ) THEN
			IF (masterproc) PRINT*, 'LPT ERROR: zl_top has to be lower than ztop.'
			flag = .FALSE.
		ENDIF
		IF ( nzl > 1 .AND. zl_top-REAL(nzl-1,rlpt)*dzl < zbottom ) THEN
			IF (masterproc) PRINT*, 'LPT ERROR: zl_top-(nzl-1)*dzl has to be higher than zbottom.'
			flag = .FALSE.
		ENDIF 
		IF ( nstep_lpt_init < 2 ) THEN
			IF (masterproc) PRINT*, 'LPT ERROR: nsave_lpt_init has to be larger than 1.'
			flag = .FALSE.
		ENDIF
		IF ( ABS( REAL(grid_space_x,4) - REAL(dx,4) ) > 1.0E-6 ) THEN
			IF (masterproc) PRINT*,'LPT ERROR: grid_space_x is not same as dx.'
			flag = .FALSE.
		ENDIF
		IF (RUN3D) THEN
			IF ( ABS( REAL(grid_space_y,4) - REAL(dy,4) ) > 1.0E-6 ) THEN
				IF (masterproc) PRINT*,'LPT ERROR: grid_space_y is not same as dy.'
				flag = .FALSE.
			ENDIF
		ELSE
			IF ( ABS( grid_space_y - grid_space_x ) > 1.0E-6 ) THEN
				IF (masterproc) &
				PRINT*,'LPT ERROR: grid_space_y have to be same as dx for 2D run.'
				flag = .FALSE.
			ENDIF
		ENDIF
		IF ( uniform_z .AND. ( ABS( REAL(grid_space_z,4) - REAL(dz,4) ) > 1.0E-6 ) ) THEN
			IF (masterproc) PRINT*,'LPT ERROR: grid_space_z is not same as dz.'
			flag = .FALSE.
		ENDIF
		
		
		! 1. Set MPI_datatype = parcel_array
		IF ( nsubdomains > 1 ) CALL parcel_array2mpi_datatype
		
		
		! 2. Get grid position information
		! SAM uses Arakawa C grid
		!
		! SAM grid index orientation (s=scalar)
		! - Horizontal:
		!         ---------------v(i=1,j=2)----------------
		!        |                                         |
		!   u(i=1,j=1)     w(i=1,j=1), s(i=1,jstepin=1)     u(i=2,j=1)
		!        |                                         |
		!         ---------------v(i=1,j=1)----------------
		!
		! - Vertical:
		!   w(k=2), zi(2) = dz
		!        |
		!   u(k=1), v(k=1), s(k=1), zc(1) = 0.5 * dz
		!        |
		!   w(k=1), zi(1) = 0
		nx_sb = MOD( rank, nsubdomains_x ) * nx
		ny_sb = ( rank / nsubdomains_x ) * ny
		! Side
		DO i = 0, nx+1
			gpxs(i) = REAL(i-1+nx_sb,rlpt) * grid_space_x
		ENDDO
		DO j = 0, ny+1
			gpys(j) = REAL(j-1+ny_sb,rlpt) * grid_space_y
		ENDDO
		DO k = 1, nz
			gpzs(k) = REAL(zi(k),rlpt)
		ENDDO
		! Center
		DO i = -1, nx+1
			gpxc(i) = ( REAL(i+nx_sb,rlpt) - 0.5_rlpt ) * grid_space_x
		ENDDO
		DO j = -1, ny+1
			gpyc(j) = ( REAL(j+ny_sb,rlpt) - 0.5_rlpt ) * grid_space_y
		ENDDO
		DO k = 1, nzm
			gpzc(k) = REAL(z(k),rlpt)
		ENDDO
		
	!	DO i = 0, nsubdomains
	!		IF (i == rank) THEN
	!			PRINT*,'LPT: rank:',rank
	!			PRINT*,'LPT: gpxs',gpxs
	!			PRINT*,'LPT: gpys',gpys
	!			PRINT*,'LPT: gpzs',gpzs
	!			PRINT*,'LPT: gpxc',gpxc
	!			PRINT*,'LPT: gpyc',gpyc
	!			PRINT*,'LPT: gpzc',gpzc
	!			PRINT*,'LPT: nx_sb, ny_sb',nx_sb,ny_sb
	!		ENDIF
	!		CALL task_barrier
	!	ENDDO
	!	CALL task_abort
		
		
		! 3. Find ktop and kbottom, count nzlpt
		!   - SAM Vertical grid structure:
		!   w(k=2), zi(2) = dz
		!        |
		!   u(k=1), v(k=1), s(k=1), zc(1) = 0.5 * dz
		!        |
		!   w(k=1), zi(1) = 0
		!
		! Additional 3 levels below and above for
		!   - vertical 5th-order Lagrange polynomial interpolation
		!   - vertical gradient, e.g., for Richardson number
		! Inerpolation will be degraded if the sufficient number of layer is unavailable.
		DO k = 1, nzm
			IF ( z(k) > zbottom ) THEN
				kbottom = k - 3   ! k-1 is the index just below zbottom.
				EXIT              ! kbottom may be one level lower than 3 levels for zi.
			ENDIF
		ENDDO
		DO k = nzm, 1, -1
			IF ( zi(k) <= ztop ) THEN
				ktop = k + 3   ! k is the index at or just below ztop
				EXIT           ! ktop may be one level higher than 3 levels for z.
			ENDIF
		ENDDO
		! Adjust kbottom and ktop
		kbottom = MAX( kbottom, 1 )
		ktop    = MIN( ktop, nzm )
		! Number of levels necessary for interpolation
		nzlpt = 0
		DO k = kbottom, ktop
			nzlpt = nzlpt + 1
		ENDDO
		IF ( uniform_z ) THEN
			DO k = kbottom, ktop-1
				IF ( ( ABS( z(k+1)-z(k)-dz ) > 1.0E-6 ) .OR. ( ABS(zi(k+1)-zi(k)-dz) > 1.0E-6 ) ) THEN
					IF (masterproc) PRINT*,'LPT ERROR: vertical grid is non-unifrom.'
					flag = .FALSE.
					EXIT
				ENDIF
			ENDDO
		ENDIF
		
		! 4. Allocate arrays
		! 4.1 Default arrays
		ALLOCATE( pres1D( nzlpt ), &
		          ures( x1u:x2u, y1u:y2u, nzlpt ), &
		          vres( x1v:x2v, y1v:y2v, nzlpt ), &
		          wres( x1w:x2w, y1w:y2w, nzlpt ), &
		          t3D ( x1s:x2s, y1s:y2s, nzlpt), &
		          qv3D( x1s:x2s, y1s:y2s, nzlpt ), &
		          qcl3D( x1s:x2s, y1s:y2s, nzlpt ), &
		          qci3D( x1s:x2s, y1s:y2s, nzlpt ), &
		          qpl3D( x1s:x2s, y1s:y2s, nzlpt ), &
		          qpi3D( x1s:x2s, y1s:y2s, nzlpt ), &
		          STAT=ierr )
		IF ( ierr /= 0 ) flag_allocation = .FALSE.
		
		! 4.2 for SGS velocity (SGS TKE, etc.)
		! additional one point with index -1 for gradient calculation
		IF ( flag_lpt_sgs ) THEN
			ALLOCATE( tke3D ( x1s:x2s, y1s:y2s, nzlpt ), &
			          tkes3D( x1s:x2s, y1s:y2s, nzlpt, 2 ), &
			          diss3D( x1s:x2s, y1s:y2s, nzlpt ), &
			          coef1( x1s:x2s, y1s:y2s, nzlpt ), &
			          coef2( x1s:x2s, y1s:y2s, nzlpt ), &
			          coefs_computed( x1s:x2s, y1s:y2s, nzlpt ), &
			          STAT=ierr )
			IF ( ierr /= 0 ) flag_allocation = .FALSE.
		ELSE
			IF ( flag_lpt_tke ) ALLOCATE( tke3D( x1s:x2s, y1s:y2s, nzlpt ), STAT=ierr )
			IF ( ierr /= 0 ) flag_allocation = .FALSE.
			IF ( flag_lpt_tkes ) ALLOCATE(tkes3D(x1s:x2s,y1s:y2s,nzlpt,2), STAT=ierr)
			IF ( ierr /= 0 ) flag_allocation = .FALSE.
		ENDIF
		
		! 4.3 Output variables depending on flags
		! 4.3.1 pressure parturbation
		IF ( flag_lpt_pprime ) THEN
			ALLOCATE( p3D( x1s:x2s, y1s:y2s, nzlpt ), STAT=ierr )
			IF ( ierr /= 0 ) flag_allocation = .FALSE.
		ENDIF
		
		! 4.3.2 micro_field
		IF ( SIZE(flag_lpt_micro) /= nmicro_fields ) THEN
			IF (masterproc) THEN
				print*,'LPT ERROR: SIZE(flag_lpt_micro) /= nmicro_fields'
				flag = .FALSE.
			ENDIF
		ENDIF
		IF ( SUM(flag_lpt_micro) /= nmicroout ) THEN
			IF (masterproc) THEN
				print*,'LPT ERROR: SUM(flag_lpt_micro) /= nmicroout'
				flag = .FALSE.
			ENDIF
		ELSE
			IF ( nmicroout > 0 ) THEN
				ALLOCATE( micro3D(x1s:x2s,y1s:y2s,nzlpt,nmicro_fields), STAT=ierr )
				IF ( ierr /= 0 ) flag_allocation = .FALSE.
			ENDIF
		ENDIF
		
		! 4.3.3 taracer
		IF ( dotracers ) THEN
			IF ( flag_lpt_tracers ) THEN
				ALLOCATE( tracer3D( x1s:x2s, y1s:y2s, nzlpt, ntracers ), STAT=ierr )
				IF ( ierr /= 0 ) flag_allocation = .FALSE.
			ENDIF
		ELSE
			IF ( flag_lpt_tracers ) THEN
				IF (masterproc) THEN
					print*,'LPT ERROR: Set flag_tracers=false for dotracers=false'
					flag = .FALSE.
				ENDIF
			ENDIF
		ENDIF
		
		! 4.4 output arrays. either collected somewhere or computed in collect_output
		! 4.4.1 Collected somewhere or diagnosed in collect_output
		ALLOCATE( qt3D( x1s:x2s, y1s:y2s, nzlpt ), &
		          qp3D( x1s:x2s, y1s:y2s, nzlpt ), &
		          out_computed( x1s:x2s, y1s:y2s, nzlpt ), &
		          STAT=ierr )
		IF ( ierr /= 0 ) flag_allocation = .FALSE.
		
		! 4.4.2 gradiant Richardson number
		IF ( flag_lpt_gradri ) THEN
			ALLOCATE( rig3D( x1s:x2s, y1s:y2s, nzlpt ), STAT=ierr )
			IF ( ierr /= 0 ) flag_allocation = .FALSE.
		ENDIF
		
		! 4.5 Terminate if allocations are failed
		IF ( .NOT.flag_allocation ) THEN
			PRINT*,'LPT ERROR: Failed to allocate arrays in lpt_set on proc:', rank
			CALL lpt_task_abort
		ENDIF
		
		! 5. Terminate if flag=.FALSE.
		IF ( .NOT.flag ) THEN
			IF (masterproc) PRINT*,'LPT detects at least one error. Abort.'
			CALL lpt_task_abort
		ENDIF
		
		! 6. Setup output variable name list. Initialize history_length.
		CALL set_output_list
		history_length = 0
		
		! 7. Set the initialization flag false or read restart data
		! At this point, nstep is either 0 or last nstop
		IF ( nstep < nstep_lpt_init ) THEN
			flag_lpt_init = .FALSE.
			flag_lpt_3Dget = .FALSE.
			flag_lpt_output = .FALSE.
		ELSE IF ( nstep >= nstep_lpt_init ) THEN
			! read restart data
			CALL lpt_restart_read
			
			! Flags have to be true
			flag_lpt_init = .TRUE.
			flag_lpt_3Dget = .TRUE.
		endif
		
		IF (masterproc) THEN
			PRINT*,''
			PRINT*,'The subroutine lpt_set did not detect inconsistency.'
			PRINT*,'Number of total lagrangin parcels:', np1
			PRINT*,'==============================================='
			PRINT*,''
		ENDIF
if(masterproc) then
print*,"!!!!!!!!!!!! lpt_set2"
print*,var_lpt_shrt
print*,"!!!!!!!!!!!!"
endif
		
	END SUBROUTINE lpt_set
	
	!==========================================================================================
	
	SUBROUTINE lpt
	
	! This subroutine has to be called in main.f90 right before "enddo ! icycle", when dolpt=.TRUE.
	!
	! At this point, SAM prognostic variables are at t+1 time step, most of diagnostic variables
	! are at t+1 time step.
	!
	! When leave this subroutine, check if output is saved in the next time step with
	! flag_lpt_output. This flag is used in the other source code to judge if 3D variables should
	! be collected or not.
	
		IMPLICIT NONE
		
		! Following variables are defined in grid.f90;
		! nstep: current number of performed time steps
		! icycle: current subcycle
		! ncycle: number of subcycles over the dynamical timestep
		! nstat: interval in time steps to compute statistics
if(masterproc) then
print*,"!!!!!!!!!!!! lpt1"
print*,var_lpt_shrt
print*,"!!!!!!!!!!!!"
endif
		! Main LPT process
		IF ( flag_lpt_init ) THEN
		
			CALL t_startf ('lpt')
			
			! Boundary exchange for collected 3D variables
			CALL lpt_stepin
			
			! Update velocity
			IF ( .NOT.flag_fixed_uv ) THEN
				! Find resolved velocity at t=t+1/2
				CALL lpt_uvw
				
				! Update SGS u, v, w
				IF ( flag_lpt_sgs ) CALL lpt_sgs_uvw
			ENDIF
			
			! Update x, y, z position. Parcel exchange at boundary
			CALL lpt_xyz
			
			! Update resolved scale velocity at t=t+1
			! Update parameterized ensemble-mean resolved scale TKE at t=t+1
			CALL lpt_stepout
			
			IF ( icycle == ncycle ) THEN
			
				! Diagnose output variables then save data
				! Output variables have to be synchronized at time with SAM variables:
				! e.g., tkes and tkes3D at t=t+1, etc.
				IF ( flag_lpt_output ) CALL lpt_output
				
				! Save output in the next step?
				flag_lpt_output = MOD( nstep+1-nstep_lpt_init, nsave_lpt ) == 0
			
				! Save restart data
				IF ( MOD(nstep,nstat*(1+nrestart_skip)) == 0 .OR. nstep == nstop .OR. nelapse == 0 ) &
					CALL lpt_restart_write
				
			ENDIF
			
			CALL t_stopf ('lpt')
			
		ELSE IF ( .NOT.flag_lpt_init ) THEN

			! Initialization of LPT
			IF ( nstep >= nstep_lpt_init-1 ) CALL lpt_init

		ENDIF
if(masterproc) then
print*,"!!!!!!!!!!!! lpt2"
print*,var_lpt_shrt
print*,"!!!!!!!!!!!!"
endif
	END SUBROUTINE lpt
	
	!==============================================================================================
	!                         SUBROUTINES BELOW ARE NOT CALLED FROM SAM
	!==============================================================================================
	
	!========================================= SET UP =========================================
	
	SUBROUTINE set_output_list
	
		IMPLICIT NONE
		INTEGER :: n, m
		CHARACTER (LEN= 2) :: mchar
		
		! 1. Set scalar_name and scalar_unit
		! The order of flag has to be the same as the flag list. See line 54
		var_lpt_name(0) = 'Number of vertical initialization'
		var_lpt_shrt(0) = 'NVI'
		var_lpt_unit(0) = '[number]'
		var_lpt_name(1) = 'X position'
		var_lpt_shrt(1) = 'X'
		var_lpt_unit(1) = '[m]'
		var_lpt_name(2) = 'Y position'
		var_lpt_shrt(2) = 'Y'
		var_lpt_unit(2) = '[m]'
		var_lpt_name(3) = 'Z position'
		var_lpt_shrt(3) = 'Z'
		var_lpt_unit(3) = '[m]'
		var_lpt_name(4) = 'U wind'
		var_lpt_shrt(4) = 'U'
		var_lpt_unit(4) = '[m/s]'
		var_lpt_name(5) = 'V wind'
		var_lpt_shrt(5) = 'V'
		var_lpt_unit(5) = '[m/s]'
		var_lpt_name(6) = 'W wind'
		var_lpt_shrt(6) = 'W'
		var_lpt_unit(6) = '[m/s]'
if(masterproc) then
print*,"!!!!!!!!!!!! lpt_output_list"
print*,var_lpt_shrt
print*,"!!!!!!!!!!!!"
endif
		IF ( flag_lpt_sgs ) THEN
			var_lpt_name(7) = 'SGS u wind'
			var_lpt_shrt(7) = 'USGS'
			var_lpt_unit(7) = '[m/s]'
			var_lpt_name(8) = 'SGS v wind'
			var_lpt_shrt(8) = 'VSGS'
			var_lpt_unit(8) = '[m/s]'
			var_lpt_name(9) = 'SGS w wind'
			var_lpt_shrt(9) = 'WSGS'
			var_lpt_unit(9) = '[m/s]'
		ENDIF

		n = nv00

		! - 10
		IF ( flag_lpt_pres ) THEN
			n = n + 1
			var_lpt_name(n) = 'Pressure'
			var_lpt_shrt(n) = 'PRES'
			var_lpt_unit(n) = '[Pa]'
		ENDIF
		! - 11
		IF ( flag_lpt_rho ) THEN
			n = n + 1
			var_lpt_name(n) = 'Density'
			var_lpt_shrt(n) = 'RHO'
			var_lpt_unit(n) = '[kg/m3]'
		ENDIF
		! - 12
		IF ( flag_lpt_pprime ) THEN
			n = n + 1
			var_lpt_name(n) = 'Pressure perturbation'
			var_lpt_shrt(n) = 'PPRIME'
			var_lpt_unit(n) = '[Pa]'
		ENDIF
		! - 13
		IF ( flag_lpt_lwse ) THEN
			n = n + 1
			var_lpt_name(n) = 'Liquid water static energy'
			var_lpt_shrt(n) = 'LWSE'
			var_lpt_unit(n) = '[K]'
		ENDIF

		! - 14
		IF ( flag_lpt_mse ) THEN
			n = n + 1
			var_lpt_name(n) = 'Moist static energy'
			var_lpt_shrt(n) = 'MSE'
			var_lpt_unit(n) = '[K]'
		ENDIF
		! - 15
		IF ( flag_lpt_dse ) THEN
			n = n + 1
			var_lpt_name(n) = 'Dry static energy'
			var_lpt_shrt(n) = 'DSE'
			var_lpt_unit(n) = '[K]'
		ENDIF
		! - 16
		IF ( flag_lpt_vdse ) THEN
			n = n + 1
			var_lpt_name(n) = 'Virtual dry static energy'
			var_lpt_shrt(n) = 'VDSE'
			var_lpt_unit(n) = '[K]'
		ENDIF
		! - 17
		IF ( flag_lpt_smse ) THEN
			n = n + 1
			var_lpt_name(n) = 'Saturation moist static energy'
			var_lpt_shrt(n) = 'SMSE'
			var_lpt_unit(n) = '[K]'
		ENDIF
		! - 18
		IF ( flag_lpt_tabs ) THEN
			n = n + 1
			var_lpt_name(n) = 'Absolute temperature'
			var_lpt_shrt(n) = 'TABS'
			var_lpt_unit(n) = '[K]'
		ENDIF
		! - 19
		IF ( flag_lpt_theta ) THEN
			n = n + 1
			var_lpt_name(n) = 'Potential temperature'
			var_lpt_shrt(n) = 'THETA'
			var_lpt_unit(n) = '[K]'
		ENDIF
		! - 20
		IF ( flag_lpt_thetal ) THEN
			n = n + 1
			var_lpt_name(n) = 'Liquid water potential temperature'
			var_lpt_shrt(n) = 'THETAL'
			var_lpt_unit(n) = '[K]'
		ENDIF
		! - 21
		IF ( flag_lpt_thetav ) THEN
			n = n + 1
			var_lpt_name(n) = 'Virtual potential temperature'
			var_lpt_shrt(n) = 'THETAV'
			var_lpt_unit(n) = '[K]'
		ENDIF
		! - 22
		IF ( flag_lpt_thetae ) THEN
			n = n + 1
			var_lpt_name(n) = 'Equivalent potential temperature'
			var_lpt_shrt(n) = 'THETAE'
			var_lpt_unit(n) = '[K]'
		ENDIF
		! - 23
		IF ( flag_lpt_qt ) THEN
			n = n + 1
			var_lpt_name(n) = 'Total nonprecipitating water'
			var_lpt_shrt(n) = 'QT'
			var_lpt_unit(n) = '[g/kg]'
		ENDIF
		! - 24
		IF ( flag_lpt_qv ) THEN
			n = n + 1
			var_lpt_name(n) = 'Water vapor'
			var_lpt_shrt(n) = 'QV'
			var_lpt_unit(n) = '[g/kg]'
		ENDIF
		! - 25
		IF ( flag_lpt_qcl ) THEN
			n = n + 1
			var_lpt_name(n) = 'Cloud water'
			var_lpt_shrt(n) = 'QCL'
			var_lpt_unit(n) = '[g/kg]'
		ENDIF
		! - 26
		IF ( flag_lpt_qci ) THEN
			n = n + 1
			var_lpt_name(n) = 'Cloud ice'
			var_lpt_shrt(n) = 'QCI'
			var_lpt_unit(n) = '[g/kg]'
		ENDIF
		! - 27
		IF ( flag_lpt_qn ) THEN
			n = n + 1
			var_lpt_name(n) = 'Cloud water + ice'
			var_lpt_shrt(n) = 'QN'
			var_lpt_unit(n) = '[g/kg]'
		ENDIF
		! - 28
		IF ( flag_lpt_qpl ) THEN
			n = n + 1
			var_lpt_name(n) = 'Rain water'
			var_lpt_shrt(n) = 'QPL'
			var_lpt_unit(n) = '[g/kg]'
		ENDIF
		! - 29
		IF ( flag_lpt_qpi ) THEN
			n = n + 1
			var_lpt_name(n) = 'Snow + graupel'
			var_lpt_shrt(n) = 'QPI'
			var_lpt_unit(n) = '[g/kg]'
		ENDIF
		! - 30
		IF ( flag_lpt_qp ) THEN
			n = n + 1
			var_lpt_name(n) = 'Rain + snow + graupel'
			var_lpt_shrt(n) = 'QP'
			var_lpt_unit(n) = '[g/kg]'
		ENDIF
		! - 31
		IF ( flag_lpt_qsat ) THEN
			n = n + 1
			var_lpt_name(n) = 'Saturation mixing ratio'
			var_lpt_shrt(n) = 'QSAT'
			var_lpt_unit(n) = '[g/kg]'
		ENDIF
		! - 32
		IF ( flag_lpt_relh ) THEN
			n = n + 1
			var_lpt_name(n) = 'Relative humidity'
			var_lpt_shrt(n) = 'RELH'
			var_lpt_unit(n) = '[unit]'
		ENDIF
		! - 33
		IF ( flag_lpt_tke ) THEN
			n = n + 1
			var_lpt_name(n) = 'Parameterized ensemble-mean resolved scale TKE'
			var_lpt_shrt(n) = 'TKE'
			var_lpt_unit(n) = '[m2/s2]'
		ENDIF
		! - 34
		IF ( flag_lpt_tkes ) THEN
			n = n + 1
			var_lpt_name(n) = 'SGS TKE'
			var_lpt_shrt(n) = 'TKES'
			var_lpt_unit(n) = '[m2/s2]'
		ENDIF
		! - 35
		IF ( flag_lpt_gradri ) THEN
			n = n + 1
			var_lpt_name(n) = 'Gradient Richardson number'
			var_lpt_shrt(n) = 'Rig'
			var_lpt_unit(n) = '[unit]'
		ENDIF
		! - 36
		IF ( nmicroout > 0 ) THEN
			DO m = 1, nmicro_fields
				IF ( flag_lpt_micro(m) == 1 ) THEN
					n = n + 1
					var_lpt_name(n) = TRIM(mklongname(m))
					var_lpt_shrt(n) = TRIM(mkname(m))
					var_lpt_unit(n) = '['//TRIM(mkunits(m))//']'
				ENDIF
			ENDDO
		ENDIF
		! - 37
		IF ( flag_lpt_tracers ) THEN
			DO m = 1, ntracers
				n = n + 1
				WRITE(mchar, '(I2.2)') m
				var_lpt_name(n) = 'Tracer '//TRIM(mchar)
				var_lpt_shrt(n) = 'TR'//TRIM(mchar)
				var_lpt_unit(n) = '[unit]'
			ENDDO
		ENDIF
		
		! Check output list
		IF ( n /= nv ) THEN
			IF (masterproc) PRINT*,''
			IF (masterproc) PRINT*,&
			'LPT ERROR: nv01 and number of scalar output flags are different. Abort.'
			CALL task_abort
		ELSE
			IF (masterproc) PRINT*,''
			IF (masterproc) PRINT*,'LPT: variables to be saved'
			IF (masterproc) THEN
				DO m = 0, nv
					PRINT*, m, var_lpt_shrt(m), var_lpt_unit(m), TRIM(var_lpt_name(m))
				ENDDO
			ENDIF
		ENDIF
if(masterproc) then
print*,"!!!!!!!!!!!! lpt_output_list"
print*,var_lpt_shrt
print*,"!!!!!!!!!!!!"
endif
	END SUBROUTINE set_output_list
	
	!===================================== INITIALIZATION =====================================
	
	SUBROUTINE lpt_init
	
	! This subroutine has to be called when
	!   - flag_lpt_init = .FALSE.
	!   - nstep >= nstep_lpt_init-1
	
		IMPLICIT NONE
		
		IF ( nstep == nstep_lpt_init-1 .AND. icycle == ncycle ) THEN
			! flag_lpt_3Dget has to be true, now
			flag_lpt_3Dget = .TRUE.
			! flag_lpt_output should be true to collect initial values in the next step
			flag_lpt_output = .TRUE.
			
			!!IF (masterproc) PRINT*,'LPT: start initialization'
			
		ELSE IF ( nstep == nstep_lpt_init .AND. icycle == ncycle ) THEN
			
			! Set t1lpt & t2lpt
			t1lpt = 1
			t2lpt = 2
			
			! Update velocity, output scalar, SGS TKE, etc.
			CALL lpt_stepin
			
			! Set initial parcel position
			CALL lpt_place_parcel
			
			! Note flag_lpt_output = .TRUE.
			! Do not move parcels. Just prepare for next time step
			CALL lpt_stepout   ! update velocity, etc.

			CALL lpt_output    ! save initial condition
			
			! Save output in the next step?
			flag_lpt_output = nsave_lpt == 1
			
			! Save restart data
			IF ( MOD( nstep, nstat*(1+nrestart_skip) ) == 0 .OR. nstep == nstop .OR. nelapse == 0 ) &
				CALL lpt_restart_write
			
			! flag_lpt_init has to be true, now
			flag_lpt_init = .TRUE.
			
			IF (masterproc) PRINT*,'LPT: initialization completed'
		ENDIF
		
	END SUBROUTINE lpt_init
	
	!==========================================================================================
	
	SUBROUTINE lpt_place_parcel
	
		IMPLICIT NONE
		
		INTEGER :: i, j, k, m, n, ierr
		REAL(KIND=rlpt) :: dxp, dyp   ! horizontal parcel distance
		REAL(KIND=rlpt) :: xp5 = 0, yp5 = 0   ! displacement for nx=np_x or ny=np_y case
		!!INTEGER :: i0, j0, k0, km2, kp3         ! comment if interpolation function is used
		!!REAL(KIND=rlpt) :: x, y, z
		!!REAL(KIND=rlpt), DIMENSION(-2:3,-2:3,-2:3) :: f216
		
		IF ( homogeneous ) THEN
			! Pacels are homogeneously placed (i.e., equally spaced) in horizontally and vertically
			! between zl_top and nzl*dzl
			
			! Allocate lagrangian parcel
			np2 = np   ! = np_x * np_y * np_xy * nzl, # of parcel for initial time
			ALLOCATE( parcel(0:np2), STAT=ierr )
			IF ( ierr /= 0 ) THEN
				PRINT*,'LPT ERROR: Failed to allocate arrays in lpt_place_parcel on proc:', rank
				CALL lpt_task_abort
			ENDIF
			
			! Horizontal parcel distance
			dxp = ( gpxs(nx+1) - gpxs(1) ) / REAL(np_x, rlpt)
			dyp = ( gpys(ny+1) - gpys(1) ) / REAL(np_y, rlpt)
			
			! Set point 5 factor for nx=np_x or ny=np_y case ==> place at scalar x or y point
			IF ( nx == np_x ) xp5 = 0.5_rlpt
			IF ( ny == np_y ) yp5 = 0.5_rlpt
			
			! Place parcels
			! tag & initial position
			n = 1
			DO i = 1, np_x   ! x-direction
				DO j = 1, np_y   ! y-direction
					DO k = 1, nzl   ! vertical layer
						DO m = 1, np_xy   ! multiple parcel on the same grid
					parcel(n)%tag    = n + rank * np
					parcel(n)%var(1) = gpxs(1) + ( REAL(i-1,rlpt) + xp5 ) * dxp
					parcel(n)%var(2) = ( gpys(1) + ( REAL(j-1,rlpt) + yp5 ) * dyp ) * REAL(YES3D,rlpt)
					parcel(n)%var(3) = zl_top - REAL(k-1,rlpt) * dzl
					n = n + 1
						ENDDO
					ENDDO
				ENDDO
			ENDDO
		ELSE
			! SPECIAL INITIALIZATION
			! NOTE:
			! - TOTAL NUMBER OF PARCELS FOR WHOLE DOMAIN HAS TO BE NP1.
			! - NP1 = NP * NSUBDOMAINS = ( NP_X * NP_Y * NP_Z * NP_XYZ ) * NSUBDOMAINS
			! - THE TAG FOR EACH PARCEL HAS TO BE UNIQUE.
			! - INCLUDE 0 INDEX WHEN ALLOCATE THE PARCEL ARRAY
			
			! BELOW IS AN EXAMPLE CODE
			! Example:
			! - 4 columns in the domain & 256 parcels per column ==> np1 = 4 * 256 = 1024
			! - Number of CPUs: 4 <= nsubdomains=2*n <= 1024
			! - CPU:   4,   8, 16, 32, 64, 128, 256, 512, 1024
			!    np: 256, 128, 64, 32, 16,   8,   4,   2,    1.
			! ==> Set either np_x, np_y, or np_z to np, then set others as well as np_xy to 1
			i = nsubdomains / 4
			DO m = 0, nsubdomains-1
				IF ( rank == m ) THEN
					IF ( MOD(m,i) == 0 ) THEN
						np2 = 256
						ALLOCATE( parcel(0:np2), STAT=ierr )
						IF ( ierr /= 0 ) THEN
							PRINT*,'LPT ERROR: Failed to allocate arrays in lpt_place_parcel on proc:', &
							        rank
							CALL lpt_task_abort
						ENDIF
						
						! Place a column at the center of the subdomain
						dxp = 0.5_rlpt * ( gpxs(nx+1) - gpxs(1) )
						dyp = 0.5_rlpt * ( gpys(ny+1) - gpys(1) )
						DO n = 1, np2
							parcel(n)%tag = n + rank / i * np2   ! tag from 1-1024
							parcel(n)%var(1) = gpxs(1) + dxp
							parcel(n)%var(2) = gpys(1) + dyp
							parcel(n)%var(3) = zl_top - REAL( n-1, rlpt ) * dzl
						ENDDO
					ELSE
						np2 = 0
						ALLOCATE( parcel(0:np2), STAT=ierr )
						IF ( ierr /= 0 ) THEN
							PRINT*,'LPT ERROR: Failed to allocate arrays in lpt_place_parcel on proc:', &
							        rank
							CALL lpt_task_abort
						ENDIF
					ENDIF
				ENDIF
			ENDDO
		ENDIF   ! homogeneous
		
		! Set initial parcel values
		! - parcel%nvi = 0
		! - parcel%var(4:nv0) = 0.0_rlpt <= resolved scale velocity will be filled at lpt_stepout
		! - parcel%var(nv0+1:nv0+nv1) = 0.0_rlpt <= SGS velocity
		! - parcel%var(nv0+nv1+1:nv0+nv1+nv2) = set here <= other variables carried to next step
		DO n = 1, np2
			! Number of vertical initialization
			parcel(n)%nvi = 0
			! U, V, W, USGS, VSGS, WSGS
			parcel(n)%var(4:nv0+nv1) = 0.0_rlpt
			! Initial height
			parcel(n)%var(nv0+nv1+1) = parcel(n)%var(3)
		ENDDO
			
	END SUBROUTINE lpt_place_parcel
	
	!==========================================================================================
	
	SUBROUTINE lpt_stepin
	
	! Boundary exchange
	! flag_lpt_output = .TRUE. means data will be saved in this time step.
	! Swap two time indices for tkes3D ( t1lpt <==> t2lpt ) before update
	
		IMPLICIT NONE
		
		INTEGER :: n
		
		CALL t_startf ('lpt_stepin')
		!!IF (masterproc) PRINT*,'lpt_stepin in'
		
		! Swap time levels
		n = t1lpt
		t1lpt = t2lpt
		t2lpt = n
		
		! Resolved velocity field
		ures(1:nx,1:ny,1:nzlpt) = u(1:nx,1:ny,kbottom:ktop)
		vres(1:nx,1:ny,1:nzlpt) = v(1:nx,1:ny,kbottom:ktop)
		wres(1:nx,1:ny,1:nzlpt) = w(1:nx,1:ny,kbottom:ktop)
		CALL task_exchange_lpt( ures, x1u, x2u, y1u, y2u, nzlpt, ux1, ux2, uy1, uy2, 1 )
		CALL task_exchange_lpt( vres, x1v, x2v, y1v, y2v, nzlpt, vx1, vx2, vy1, vy2, 2 )
		CALL task_exchange_lpt( wres, x1w, x2w, y1w, y2w, nzlpt, wx1, wx2, wy1, wy2, 3 )
		
		
		! SGS TKE and dissipation rate. SGS TKE has two time levels.
		! Here, t2lpt represents t+1 time step, and t1lpt for previous time step.
		IF ( flag_lpt_sgs .OR. ( flag_lpt_output .AND. flag_lpt_tkes ) ) THEN
			tkes3D(1:nx,1:ny,1:nzlpt,t2lpt) = tke(1:nx,1:ny,kbottom:ktop)
			CALL task_exchange_lpt(tkes3D(:,:,:,t2lpt),x1w,x2w,y1w,y2w,nzlpt,wx1,wx2,wy1,wy2,4)
		ENDIF
		IF ( flag_lpt_sgs ) CALL task_exchange_lpt(diss3D,x1s,x2s,y1s,y2s,nzlpt,sx1,sx2,sy1,sy2,5)
		
		
		! Output variables
		IF ( flag_lpt_output ) THEN
			! Store pres, p, tl, micro_field into arrays
			pres1D(1:nzlpt) = pres(kbottom:ktop)
			IF ( flag_lpt_pprime) p3D(1:nx,1:ny,1:nzlpt) = p(1:nx,1:ny,kbottom:ktop)
			t3D(1:nx,1:ny,1:nzlpt)   = t(1:nx,1:ny,kbottom:ktop)
			qv3D(1:nx,1:ny,1:nzlpt)  = qv(1:nx,1:ny,kbottom:ktop)
			qcl3D(1:nx,1:ny,1:nzlpt) = qcl(1:nx,1:ny,kbottom:ktop)
			qci3D(1:nx,1:ny,1:nzlpt) = qci(1:nx,1:ny,kbottom:ktop)
			qpl3D(1:nx,1:ny,1:nzlpt) = qpl(1:nx,1:ny,kbottom:ktop)
			qpi3D(1:nx,1:ny,1:nzlpt) = qpi(1:nx,1:ny,kbottom:ktop)
			IF ( flag_lpt_pprime ) CALL task_exchange_lpt(p3D,x1s,x2s,y1s,y2s,nzlpt,sx1,sx2,sy1,sy2,11)
			CALL task_exchange_lpt( t3D,   x1s, x2s, y1s, y2s, nzlpt, sx1, sx2, sy1, sy2, 12 )
			CALL task_exchange_lpt( qv3D,  x1s, x2s, y1s, y2s, nzlpt, sx1, sx2, sy1, sy2, 13 )
			CALL task_exchange_lpt( qcl3D, x1s, x2s, y1s, y2s, nzlpt, sx1, sx2, sy1, sy2, 14 )
			CALL task_exchange_lpt( qci3D, x1s, x2s, y1s, y2s, nzlpt, sx1, sx2, sy1, sy2, 15 )
			CALL task_exchange_lpt( qpl3D, x1s, x2s, y1s, y2s, nzlpt, sx1, sx2, sy1, sy2, 16 )
			CALL task_exchange_lpt( qpi3D, x1s, x2s, y1s, y2s, nzlpt, sx1, sx2, sy1, sy2, 17 )
		ENDIF
		
		! micro_field
		IF ( flag_lpt_output .AND. nmicroout > 0 ) THEN
			DO n = 1, nmicro_fields
				IF ( flag_lpt_micro(n) == 1 ) THEN
					micro3D(1:nx,1:ny,1:nzlpt,n) = micro_field(1:nx,1:ny,kbottom:ktop,n)
					CALL task_exchange_lpt( micro3D(:,:,:,n), x1s, x2s, y1s, y2s, nzlpt, sx1, sx2, sy1, &
					                        sy2, field_id_micro1+n-1 )
				ENDIF
			ENDDO
		ENDIF
		
		! Tracer (& tracer physics)
		IF ( flag_lpt_output .AND. flag_lpt_tracers ) THEN
			tracer3D(1:nx,1:ny,1:nzlpt,1:ntracers) = tracer(1:nx,1:ny,kbottom:ktop,1:ntracers)
			DO n = 1, ntracers
				CALL task_exchange_lpt( tracer3D(:,:,:,n), x1s, x2s, y1s, y2s, nzlpt, sx1, sx2, sy1, &
				                        sy2, field_id_tracer1+n-1 )
			ENDDO
		ENDIF
		
		!!IF (masterproc) PRINT*,'lpt_stepin out'
		CALL t_stopf ('lpt_stepin')
		
	END SUBROUTINE lpt_stepin
	
	!==========================================================================================
	
	SUBROUTINE lpt_uvw
	
	! This subroutine finds resolved velocity at t=t+1/2 with iterative method.
	
		IMPLICIT NONE
		
		REAL(KIND=rlpt) :: unow, vnow, wnow, xp, yp, zp
		INTEGER, DIMENSION(3) :: i0, j0, k0, km2, kp3
		REAL(KIND=rlpt), DIMENSION(3) :: x, y, z
		REAL(KIND=rlpt), DIMENSION(-2:3,-2:3,-2:3) :: f216
		INTEGER :: i, n
		
		CALL t_startf ('lpt_uvw')
		!!IF (masterproc) PRINT*,'lpt_uvw in'
		
		! Find u, v, w at t=t+1/2 by iterative method.
		! If n_uvw_iteration = 0, then just through this process, so that parcel position will be
		! updated with the forward scheme, i.e., x(t+1) = x(t) + u(t) * dt
		IF ( n_uvw_iteration >= 1 ) THEN
			! Iteration
			DO n = 1, np2
				unow = parcel(n)%var(4)
				vnow = parcel(n)%var(5)
				wnow = parcel(n)%var(6)
				
				DO i = 1, n_uvw_iteration
					! Guess new position
					xp = parcel(n)%var(1) + parcel(n)%var(4) * REAL(dtn,rlpt) 
					yp = parcel(n)%var(2) + parcel(n)%var(5) * REAL(dtn,rlpt) * REAL(YES3D,rlpt)
					zp = parcel(n)%var(3) + parcel(n)%var(6) * REAL(dtn,rlpt)
					
					IF ( subsidence ) THEN
						zp = zp - parcel(n)%var(3) * divergence * REAL(dtn,rlpt)
					ENDIF
				
					! Get necessary information for interpolation
					CALL uvw_def_points( xp, yp, zp, i0, j0, k0, km2, kp3, x, y, z )
					
					! u @ t=t+1/2
					f216( :, -jm2:jp3, -km2(1):kp3(1) ) &
						= ures( i0(1)-2:i0(1)+3, j0(1)-jm2:j0(1)+jp3, k0(1)-km2(1):k0(1)+kp3(1) )
					parcel(n)%var(4) = 0.5_rlpt * ( unow &
						+ interpolation( f216, x(1), y(1), z(1), k0(1), km2(1), kp3(1), 1, .FALSE. ) )
					
					! v @ t=t+1/2
					f216( :, -jm2:jp3, -km2(2):kp3(2) ) &
						= vres(i0(2)-2:i0(2)+3, j0(2)-jm2:j0(2)+jp3, k0(2)-km2(2):k0(2)+kp3(2) )
					parcel(n)%var(5) = 0.5_rlpt * ( vnow &
						+ interpolation( f216, x(2), y(2), z(2), k0(2), km2(2), kp3(2), 1, .FALSE. ) )
					
					! w @ t=t+1/2
					f216( :, -jm2:jp3, -km2(3):kp3(3) ) &
						= wres(i0(3)-2:i0(3)+3, j0(3)-jm2:j0(3)+jp3, k0(3)-km2(3):k0(3)+kp3(3) )
					parcel(n)%var(6) = 0.5_rlpt * ( wnow &
						+ interpolation( f216, x(3), y(3), z(3), k0(3), km2(3), kp3(3), 0, .FALSE. ) )
				ENDDO
			ENDDO
		ENDIF
		
		!!IF (masterproc) PRINT*,'lpt_uvw out'
		CALL t_stopf ('lpt_uvw')
		
	end SUBROUTINE lpt_uvw
	
	!============================================================================================
	
	SUBROUTINE lpt_sgs_uvw
	
	! This subroutine is called if flag_lpt_init = .TRUE. and flag_lpt_sgs = .TRUE.
	! This subroutine updates SGS u, v, w based on Weil et al. (2004, JAS)
	!
	! tkes3D(:,:,:,t1lpt) at t=t, tkes3D(:,:,:,t2lpt) at t=t+1
	! tke3D, diss3D at t=t
	
		IMPLICIT NONE
		
		INTEGER :: i0, j0, k0, km2, kp3
		REAL(KIND=rlpt) :: x, y, z, c1, c2, xp, yp, zp, tkes_now, tkes_new, dtkesdx, dtkesdy, dtkesdz
		REAL(KIND=rlpt), PARAMETER :: c0 = 3
		INTEGER :: n, i, j, k
		REAL(KIND=rlpt), DIMENSION(-2:3,-2:3,-2:3) :: f216
		REAL(KIND=rlpt), DIMENSION(0:1,0:1,0:1) :: f8
		
		CALL t_startf ('lpt_sgs_uvw')
		
		coefs_computed = .FALSE.
		DO n = 1, np2
		
			! Check if flow is turbulent to avoid divide by zero in the parameterization formula.
			! Turbulent if tkes > 1.e-2 (velocity scale > 10 cm/s) for the closest 8 grid points
			CALL scalar_def_point( parcel(n)%var(1), parcel(n)%var(2), parcel(n)%var(3), &
			                       i0, j0, k0, km2, kp3, x, y, z )
			IF(MINVAL(tkes3D(i0:i0+1,j0:j0+YES3D,k0-MIN(km2,1):k0+1,t1lpt)) < 1E-2_rlpt)THEN
				parcel(n)%var(i7) = 0.0_rlpt   ! SGS u wind
				parcel(n)%var(i8) = 0.0_rlpt   ! SGS v wind
				parcel(n)%var(i9) = 0.0_rlpt   ! SGS w wind
			ELSE
				! Parcel in turbulent flow
				
				! Get coef1, coef2 @ t=t
				!   coef1 = fs * diss3D / tkes3D
				!   coef2 = sqrt(fs * diss3D)
				!   where fs = tkes3D / ( tke3D + tkes3D )
				DO k = k0-km2, k0+kp3
					DO j = j0-jm2, j0+jp3
						DO i = i0-2, i0+3
							IF ( .NOT.coefs_computed(i,j,k) ) THEN
								coef1(i,j,k) = diss3D(i,j,k) / ( tke3D(i,j,k) + tkes3D(i,j,k,t1lpt) )
								coef2(i,j,k) = SQRT( c0 * coef1(i,j,k) * tkes3D(i,j,k,t1lpt) )
								coefs_computed(i,j,k) = .TRUE.
							ENDIF
						ENDDO
					ENDDO
				ENDDO
				
				! Interpolate coef1
				f216(:,-jm2:jp3,-km2:kp3) = coef1(i0-2:i0+3,j0-jm2:j0+jp3,k0-km2:k0+kp3)
				c1 = interpolation( f216, x, y, z, k0, km2, kp3, 1, .TRUE. )
				
				! Interpolate coef2
				f216(:,-jm2:jp3,-km2:kp3) = coef2(i0-2:i0+3,j0-jm2:j0+jp3,k0-km2:k0+kp3)
				c2 = interpolation( f216, x, y, z, k0, km2, kp3, 1, .TRUE. )
				
				! SGS TKE and Partial derivative of SGS TKE at parcel point
				f216(:,-jm2:jp3,-km2:kp3) = tkes3D(i0-2:i0+3,j0-jm2:j0+jp3,k0-km2:k0+kp3,t1lpt)
				f8(:,0:1*YES3D,:) = f216(0:1,0:1*YES3D,0:1)
				tkes_now = interpolation( f216, x, y, z, k0, km2, kp3, 1, .TRUE. )
				k0 = k0 + kbottom - 1 ! conversion from 1<=k0<=nzlpt to 1<=k0<=nzm
				dtkesdx  = trilinear_interpolation( f8, x, y, z, k0, 1 )
				dtkesdy  = trilinear_interpolation( f8, x, y, z, k0, 2 )
				dtkesdz  = trilinear_interpolation( f8, x, y, z, k0, 3 )
				
				! SGS TKE @ t+1
				! New location guessed with resolved velocity
				xp = parcel(n)%var(1) + parcel(n)%var(4) * REAL(dtn,rlpt)
				yp = parcel(n)%var(2) + parcel(n)%var(5) * REAL(dtn,rlpt) * REAL(YES3D,rlpt)
				zp = parcel(n)%var(3) + parcel(n)%var(6) * REAL(dtn,rlpt)
				
				IF ( subsidence ) THEN
					zp = zp - parcel(n)%var(3) * divergence * REAL(dtn,rlpt)
				ENDIF
					
				CALL scalar_def_point( xp, yp, zp, i0, j0, k0, km2, kp3, x, y, z )
				f216(:,-jm2:jp3,-km2:kp3) = tkes3D(i0-2:i0+3,j0-jm2:j0+jp3,k0-km2:k0+kp3,t2lpt)
				tkes_new = interpolation( f216, x, y, z, k0, km2, kp3, 1, .TRUE. )
				
				! Update SGS velocity
				! Backward implicit for the first term = [-0.75*c0*c1*parcel(n)%var(7)*dtn] <== sink
				! SGS u wind
				parcel(n)%var(i7) &
					= 1.0_rlpt / ( 1.0_rlpt + 0.75_rlpt * c0 * c1 * REAL(dtn,rlpt) ) &
					* ( 0.5_rlpt * ( tkes_new / tkes_now + 1.0_rlpt ) * parcel(n)%var(i7) &
					    + (1.0_rlpt/3.0_rlpt) * dtkesdx * REAL(dtn,rlpt) &
					    + c2 * num_gauss( sqrt(REAL(dtn,rlpt)) ) )
				
				! SGS v wind
				parcel(n)%var(i8) &
					= 1.0_rlpt / ( 1.0_rlpt + 0.75_rlpt * c0 * c1 * REAL(dtn,rlpt) ) &
					* ( 0.5_rlpt * ( tkes_new / tkes_now + 1.0_rlpt ) * parcel(n)%var(i8) &
					    + (1.0_rlpt/3.0_rlpt) * dtkesdy * REAL(dtn,rlpt) &
					    + c2 * num_gauss( sqrt(REAL(dtn,rlpt)) ) )
				
				! SGS w wind
				parcel(n)%var(i9) &
					= 1.0_rlpt / ( 1.0_rlpt + 0.75_rlpt * c0 * c1 * REAL(dtn,rlpt) ) &
					* ( 0.5_rlpt * ( tkes_new / tkes_now + 1.0_rlpt ) * parcel(n)%var(i9) &
					    + (1.0_rlpt/3.0_rlpt) * dtkesdz * REAL(dtn,rlpt) &
					    + c2 * num_gauss( sqrt(REAL(dtn,rlpt)) ) )
			ENDIF
		ENDDO   ! n, np2
		
	!	xp = minval(parcel%var(i7))
	!	yp = minval(parcel%var(i8))
	!	zp = minval(parcel%var(i9))
	!	x = maxval(parcel%var(i7))
	!	y = maxval(parcel%var(i8))
	!	z = maxval(parcel%var(i9))
	!	PRINT*,'SGS uvw', min(xp, yp), max(x, y), zp, z 
	!	CALL task_barrier
		
		CALL t_stopf ('lpt_sgs_uvw')
		
	END SUBROUTINE lpt_sgs_uvw
	
	!==========================================================================================
	
	SUBROUTINE lpt_xyz
	
	! This subroutine has to be called if flag_lpt_init = .true.
	! This subroutine updates the position of each parcel.
	! 1. Update new each parcel's position by updated u, v, w, and usgs, vsgs, wsgs
	!  1.1 Compute new position
	!   x(t+1) = x(t) + u(t) * dtn + usgs(t) * dtn
	!   y(t+1) = y(t) + v(t) * dtn + vsgs(t) * dtn
	!   z(t+1) = z(t) + w(t) * dtn + wsgs(t) * dtn
	!  t is time step
	!  1.2 Check if each parcel remains in the same subdomain or moves next subdomain.
	!  1.3 Make linked list with pointer for all directions including staying in the current rank.
	! 2. MPI exchange (see task_exchange_parcel)
	!  2.1 Store outgoing parcel into array and send.
	!  2.2 Receive incoming parcels
	! 3. Update parcel pointer-array
	!  3.1 Add incomping parcels to the linked list
	!  3.2 Store all parcels into new parcel
	
		IMPLICIT NONE
		
		INTEGER :: n, ierr
		TYPE(parcel_array), ALLOCATABLE, DIMENSION(:) :: parcel_temp
		!!INTEGER :: i0, j0, k0   ! comment if interpolation function is called.
		!!REAL(KIND=rlpt) :: x, y, z
		!!REAL(KIND=rlpt), DIMENSION(-2:3,-2:3,-2:3) :: f216
		
		CALL t_startf ('lpt_xyz')
		!!IF (masterproc) PRINT*,'lpt_xyz in'
		
		! Initialize the empty list
		NULLIFY( parcel_head, parcel_tail, &
				parcel_out_nn_head, parcel_out_nn_tail, parcel_out_ne_head, parcel_out_ne_tail, &
				parcel_out_ee_head, parcel_out_ee_tail, parcel_out_se_head, parcel_out_se_tail, &
				parcel_out_ss_head, parcel_out_ss_tail, parcel_out_sw_head, parcel_out_sw_tail, &
				parcel_out_ww_head, parcel_out_ww_tail, parcel_out_nw_head, parcel_out_nw_tail )
		
		! Prepare for loop
		n_out(:) = 0
		
		! 1. Update position
		DO n = 1, np2
			! 1.1 New position
			IF ( .NOT.flag_fixed_uv ) THEN
				parcel(n)%var(1) = parcel(n)%var(1) + parcel(n)%var(4) * REAL(dtn,rlpt)
				parcel(n)%var(2) = parcel(n)%var(2) + parcel(n)%var(5) * REAL(dtn,rlpt)*REAL(YES3D,rlpt)
				parcel(n)%var(3) = parcel(n)%var(3) + parcel(n)%var(6) * REAL(dtn,rlpt)
				IF ( subsidence ) THEN
					parcel(n)%var(3) = parcel(n)%var(3) - parcel(n)%var(3) * divergence * REAL(dtn,rlpt)
				ENDIF
				IF ( flag_lpt_sgs ) THEN
				parcel(n)%var(1) = parcel(n)%var(1) + parcel(n)%var(i7) * REAL(dtn,rlpt)
				parcel(n)%var(2) = parcel(n)%var(2) + parcel(n)%var(i8) *REAL(dtn,rlpt)*REAL(YES3D,rlpt)
				parcel(n)%var(3) = parcel(n)%var(3) + parcel(n)%var(i9) * REAL(dtn,rlpt)
				ENDIF
			ELSE
				! Parcel velocity is fixed.
				parcel(n)%var(1) = parcel(n)%var(1) + fixed_u * REAL(dtn,rlpt)
				parcel(n)%var(2) = parcel(n)%var(2) + fixed_v * REAL(dtn,rlpt) * REAL(YES3D,rlpt)
			ENDIF
			
			! Locate parcels to the initial vertical position if they are above ztop or below zbottom
			IF ( (parcel(n)%var(3) >= ztop) .OR. (parcel(n)%var(3) <= zbottom) ) THEN
				! Add 1 for the vertical re-initialization counter
				parcel(n)%nvi = parcel(n)%nvi + 1
				! Reset vertical position to the initial position
				parcel(n)%var(3) = parcel(n)%var(nv0+nv1+1)
				! Reinitialize parcel values
				parcel(n)%var(4:6) = 0.0_rlpt   ! u, v, w
				IF ( flag_lpt_sgs ) parcel(n)%var(i7:i9) = 0.0_rlpt   ! sgs u, v, w
			!!	IF (nv2 > 0) parcel(n)%var(nv0+nv1+2:nv0+nv1+nv2) =   ! other, do not over write init z
			ENDIF
			
			! 1.2 & 1.3 Add parcel to the list, parcel_head or parcel_out_##_head. Count n_out
			! Determin if each parcel is going out or not, then make linked lists
			!
			! The south and west boundaries belong the current subdomain.
			! The following diagram shows the indices for gpxs and gpys for the current rank to judge
			! if a parcel moves to a neighbor rank.
			!    _              _              _
			!    | north-west   | north        | north-east
			!    | ips <        | ips <=       |
			!    |              | ipe+1 >      | ipe+1 >=
			!    |              |              | jpe+1 >=
			!    | jpe+1 >=     | jpe+1 >=     |
			!    *------------| *------------| *------------| <=== jpe+1
			!    _              _              _
			!    | west         |              | east
			!    | ips <        |              |
			!    |              |    rank      | ipe+1 >=
			!    | jps >=       |              | jps >=
			!    | jpe+1 <      |              | jpe+1 <
			!    *------------| *------------| *------------| <=== jps
			!    _              _              _
			!    | south-west   | south        | south-east
			!    | ips <        | ips <=       |
			!    |              | ipe+1 >      | ipe+1 >=
			!    |              | jps <        | jps <
			!    | jps <        |              |
			!    *------------| *------------| *------------|
			!                   ^              ^
			!                   ^              ^
			!                  ips           ipe+1
			IF (RUN3D) THEN
				! North
				IF ( (parcel(n)%var(1) >= gpxs(1)).AND.(parcel(n)%var(1) < gpxs(nx+1)) &
					.AND.(parcel(n)%var(2) >= gpys(ny+1)) ) THEN
					
					! Horizontal boundary position adjustment
					IF ( parcel(n)%var(2) >= ywidth ) parcel(n)%var(2) = parcel(n)%var(2) - ywidth
					
					! Add a new parcel to the end of the list
					IF ( ASSOCIATED(parcel_out_nn_head) ) THEN
						! List is not empty
						parcel_out_nn_tail%next => parcel(n)   ! attach new parcel at end of list
						NULLIFY(parcel(n)%next)
						parcel_out_nn_tail => parcel(n)   ! reset parcel_tail pointer
					ELSE
						! List is empty
						parcel_out_nn_head => parcel(n)   ! start up list with parcel_new
						parcel_out_nn_tail => parcel(n)
						NULLIFY(parcel_out_nn_tail%next)   ! no successor
					ENDIF
					! Count # of parcel going to Northern subdomain
					n_out(6) = n_out(6) + 1
					
				! North East
				ELSE IF ( (parcel(n)%var(1) >= gpxs(nx+1)).AND.(parcel(n)%var(2) >= gpys(ny+1)) ) THEN
				
					! Horizontal boundary position adjustment
					IF ( parcel(n)%var(1) >= xwidth ) parcel(n)%var(1) = parcel(n)%var(1) - xwidth
					IF ( parcel(n)%var(2) >= ywidth ) parcel(n)%var(2) = parcel(n)%var(2) - ywidth
					
					! Add a new parcel to the end of the list
					IF ( ASSOCIATED(parcel_out_ne_head) ) THEN
						parcel_out_ne_tail%next => parcel(n)
						NULLIFY(parcel(n)%next)
						parcel_out_ne_tail => parcel(n)
					ELSE
						parcel_out_ne_head => parcel(n)
						parcel_out_ne_tail => parcel(n)
						NULLIFY(parcel_out_ne_tail%next)
					ENDIF
					n_out(8) = n_out(8) + 1
					
				! East
				ELSE IF ( (parcel(n)%var(1) >= gpxs(nx+1)) &
					.AND.(parcel(n)%var(2) >= gpys(1)).AND.(parcel(n)%var(2) < gpys(ny+1)) ) THEN
					
					! Horizontal boundary position adjustment
					IF ( parcel(n)%var(1) >= xwidth ) parcel(n)%var(1) = parcel(n)%var(1) - xwidth
					
					! Add a new parcel to the end of the list
					IF ( ASSOCIATED(parcel_out_ee_head) ) THEN
						parcel_out_ee_tail%next => parcel(n)
						NULLIFY(parcel(n)%next)
						parcel_out_ee_tail => parcel(n)
					ELSE
						parcel_out_ee_head => parcel(n)
						parcel_out_ee_tail => parcel(n)
						NULLIFY(parcel_out_ee_tail%next)
					ENDIF
					n_out(1) = n_out(1) + 1
					
				! South east
				ELSE IF ( (parcel(n)%var(1) >= gpxs(nx+1)).AND.(parcel(n)%var(2) < gpys(1)) ) THEN
				
					! Horizontal boundary position adjustment
					IF ( parcel(n)%var(1) >= xwidth ) parcel(n)%var(1) = parcel(n)%var(1) - xwidth
					IF ( parcel(n)%var(2) < 0.0_rlpt ) parcel(n)%var(2) = ywidth + parcel(n)%var(2)
					
					! Add a new parcel to the end of the list
					IF ( ASSOCIATED(parcel_out_se_head) ) THEN
						parcel_out_se_tail%next => parcel(n)
						NULLIFY(parcel(n)%next)
						parcel_out_se_tail => parcel(n)
					ELSE
						! List is empty
						parcel_out_se_head => parcel(n)
						parcel_out_se_tail => parcel(n)
						NULLIFY(parcel_out_se_tail%next)
					ENDIF
					n_out(3) = n_out(3) + 1
					
				! South
				ELSE IF ( (parcel(n)%var(1) >= gpxs(1)).AND.(parcel(n)%var(1) < gpxs(nx+1)) &
					.AND.(parcel(n)%var(2) < gpys(1)) ) THEN
					
					! Horizontal boundary position adjustment
					IF ( parcel(n)%var(2) < 0.0_rlpt ) parcel(n)%var(2) = ywidth + parcel(n)%var(2)
					
					! Add a new parcel to the end of the list	
					IF ( ASSOCIATED(parcel_out_ss_head) ) THEN
						parcel_out_ss_tail%next => parcel(n)
						NULLIFY(parcel(n)%next)
						parcel_out_ss_tail => parcel(n)
					ELSE
						parcel_out_ss_head => parcel(n)
						parcel_out_ss_tail => parcel(n)
						NULLIFY(parcel_out_ss_tail%next)
					ENDIF
					n_out(5) = n_out(5) + 1
					
				! South west
				ELSE IF ( (parcel(n)%var(1) < gpxs(1)).AND.(parcel(n)%var(2) < gpys(1)) ) THEN
				
					! Horizontal boundary position adjustment
					IF ( parcel(n)%var(1) < 0.0_rlpt ) parcel(n)%var(1) = xwidth + parcel(n)%var(1)
					IF ( parcel(n)%var(2) < 0.0_rlpt ) parcel(n)%var(2) = ywidth + parcel(n)%var(2)
					
					! Add a new parcel to the end of the list	
					IF ( ASSOCIATED(parcel_out_sw_head) ) THEN
						parcel_out_sw_tail%next => parcel(n)
						NULLIFY(parcel(n)%next)
						parcel_out_sw_tail => parcel(n)
					ELSE
						parcel_out_sw_head => parcel(n)
						parcel_out_sw_tail => parcel(n)
						NULLIFY(parcel_out_sw_tail%next)
					ENDIF
					n_out(7) = n_out(7) + 1
					
				! West
				ELSE IF ( (parcel(n)%var(1) < gpxs(1) ) &
					.AND.(parcel(n)%var(2) >= gpys(1)).AND.(parcel(n)%var(2) < gpys(ny+1)) ) THEN
					
					! Horizontal boundary position adjustment
					IF ( parcel(n)%var(1) < 0.0_rlpt ) parcel(n)%var(1) = xwidth + parcel(n)%var(1)
					
					! Add a new parcel to the end of the list
					IF ( ASSOCIATED(parcel_out_ww_head) ) THEN
						parcel_out_ww_tail%next => parcel(n)
						NULLIFY(parcel(n)%next)
						parcel_out_ww_tail => parcel(n)
					ELSE
						parcel_out_ww_head => parcel(n)
						parcel_out_ww_tail => parcel(n)
						NULLIFY(parcel_out_ww_tail%next)
					ENDIF
					n_out(2) = n_out(2) + 1
					
				! North west
				ELSE IF ( (parcel(n)%var(1) < gpxs(1)).AND.(parcel(n)%var(2) >= gpys(ny+1)) ) THEN
				
					! Horizontal boundary position adjustment
					IF ( parcel(n)%var(1) < 0.0_rlpt ) parcel(n)%var(1) = xwidth + parcel(n)%var(1)
					IF ( parcel(n)%var(2) >= ywidth ) parcel(n)%var(2) = parcel(n)%var(2) - ywidth
					
					! Add a new parcel to the end of the list
					IF ( ASSOCIATED(parcel_out_nw_head) ) THEN
						parcel_out_nw_tail%next => parcel(n)
						NULLIFY(parcel(n)%next)
						parcel_out_nw_tail => parcel(n)
					ELSE
						parcel_out_nw_head => parcel(n)
						parcel_out_nw_tail => parcel(n)
						NULLIFY(parcel_out_nw_tail%next)
					ENDIF
					n_out(4) = n_out(4) + 1
					
				! Stay in this subdomain
				ELSE
					! Add a new parcel to the end of the list
					IF ( ASSOCIATED(parcel_head) ) THEN
						! List is not empty
						parcel_tail%next => parcel(n)
						NULLIFY(parcel(n)%next)
						parcel_tail => parcel(n)
					ELSE
						! List is empty
						parcel_head => parcel(n)
						parcel_tail => parcel(n)
						NULLIFY(parcel_tail%next)
					ENDIF
				ENDIF
				
			ELSE
				! 2D run
				! East
				IF ( parcel(n)%var(1) >= gpxs(nx+1) ) THEN
				
					! Horizontal boundary position adjustment
					IF ( parcel(n)%var(1) >= xwidth ) parcel(n)%var(1) = parcel(n)%var(1) - xwidth
					
					! Add a new parcel to the end of the list
					IF ( ASSOCIATED(parcel_out_ee_head) ) THEN
						parcel_out_ee_tail%next => parcel(n)
						NULLIFY(parcel(n)%next)
						parcel_out_ee_tail => parcel(n)
					ELSE
						parcel_out_ee_head => parcel(n)
						parcel_out_ee_tail => parcel(n)
						NULLIFY(parcel_out_ee_tail%next)
					ENDIF
					n_out(1) = n_out(1) + 1
					
				! West
				ELSE IF ( parcel(n)%var(1) < gpxs(1) ) THEN
				
					! Horizontal boundary position adjustment
					IF ( parcel(n)%var(1) < 0.0_rlpt ) parcel(n)%var(1) = xwidth + parcel(n)%var(1)
					
					! Add a new parcel to the end of the list
					IF ( ASSOCIATED(parcel_out_ww_head) ) THEN
						parcel_out_ww_tail%next => parcel(n)
						NULLIFY(parcel(n)%next)
						parcel_out_ww_tail => parcel(n)
					ELSE
						parcel_out_ww_head => parcel(n)
						parcel_out_ww_tail => parcel(n)
						NULLIFY(parcel_out_ww_tail%next)
					ENDIF
					n_out(2) = n_out(2) + 1
					
				! Stay in this subdomain
				ELSE
					! Add a new parcel to the end of the list
					IF ( ASSOCIATED(parcel_head) ) THEN
						! List is not empty
						parcel_tail%next => parcel(n)
						NULLIFY(parcel(n)%next)
						parcel_tail => parcel(n)
					ELSE
						! List is empty
						parcel_head => parcel(n)
						parcel_tail => parcel(n)
						NULLIFY(parcel_tail%next)
					ENDIF
				ENDIF
				
			ENDIF   ! RUN3D
		ENDDO   ! n, np2
		
		! 2. MPI exchange
		np_out = n_out(1)+n_out(2)+n_out(3)+n_out(4)+n_out(5)+n_out(6)+n_out(7)+n_out(8)
		CALL task_exchange_parcel
		
		! 3. Update parcel pointer-array
		! 3.1 Add incoming parcels to the linked list
		IF (np_in > 0) THEN
			DO n = 1, np_in
				IF ( ASSOCIATED(parcel_head) ) THEN
					parcel_tail%next => parcel_in(n)
					NULLIFY(parcel_in(n)%next)
					parcel_tail => parcel_in(n)
				ELSE
					parcel_head => parcel_in(n)
					parcel_tail => parcel_in(n)
					NULLIFY(parcel_tail%next)
				ENDIF
			ENDDO
		ENDIF
		
		! 3.2 Store parcel_head (linked list data) into new parcel (pointer-array) through
		!     parcel_temp (array) for next time step. Do clean deallocation
		np2 = np2 - np_out + np_in
		ALLOCATE( parcel_temp(0:np2), STAT=ierr )
		IF ( ierr /= 0 ) THEN
			PRINT*,'LPT ERROR: Failed to allocate arrays in lpt_xyz on proc:', rank
			CALL lpt_task_abort
		ENDIF
		IF ( np2 > 0 .AND. .NOT.ASSOCIATED(parcel_head) ) THEN
			PRINT*,'LPT: parcel_head failed. Abort. rank:', rank
			CALL lpt_task_abort
		ELSE
			DO n = 1, np2
				parcel_temp(n)%tag = parcel_head%tag
				parcel_temp(n)%nvi = parcel_head%nvi
				parcel_temp(n)%var(:) = parcel_head%var(:)
				parcel_head => parcel_head%next
			ENDDO
		ENDIF
		NULLIFY( parcel_head, parcel_tail, &
				parcel_out_nn_head, parcel_out_nn_tail, parcel_out_ne_head, parcel_out_ne_tail, &
				parcel_out_ee_head, parcel_out_ee_tail, parcel_out_se_head, parcel_out_se_tail, &
				parcel_out_ss_head, parcel_out_ss_tail, parcel_out_sw_head, parcel_out_sw_tail, &
				parcel_out_ww_head, parcel_out_ww_tail, parcel_out_nw_head, parcel_out_nw_tail )
		DEALLOCATE( parcel, parcel_in, STAT=ierr )
		IF ( ierr /= 0 ) THEN
			PRINT*,'LPT ERROR: Failed to deallocate arrays in lpt_xyz on proc:', rank
			CALL lpt_task_abort
		ENDIF
		ALLOCATE( parcel(0:np2), STAT=ierr )
		IF ( ierr /= 0 ) THEN
			PRINT*,'LPT ERROR: Failed to allocate arrays in lpt_xyz on proc:', rank
			CALL lpt_task_abort
		ENDIF
		DO n = 1, np2
			parcel(n)%tag = parcel_temp(n)%tag
			parcel(n)%nvi = parcel_temp(n)%nvi
			parcel(n)%var(:) = parcel_temp(n)%var(:)
		ENDDO
		DEALLOCATE( parcel_temp, STAT=ierr )
		IF ( ierr /= 0 ) THEN
			PRINT*,'LPT ERROR: Failed to deallocate arrays in lpt_xyz on proc:', rank
			CALL lpt_task_abort
		ENDIF
		
		!!IF (masterproc) PRINT*,'lpt_xyz out'
		CALL t_stopf ('lpt_xyz')
		
	END SUBROUTINE lpt_xyz
	
	!==========================================================================================
	
	SUBROUTINE lpt_stepout
	
	! Update resolved scale velocity at t=t+1.
	! Update parameterized ensemble-mean resolved scale TKE, boundary exchange
	
		IMPLICIT NONE
		
		INTEGER :: i, j, k, n, n1
		INTEGER, DIMENSION(3) :: i0, j0, k0, km2, kp3
		REAL(KIND=rlpt), DIMENSION(3) :: x, y, z
		REAL(KIND=rlpt), DIMENSION(-2:3,-2:3,-2:3) :: f216
		
		CALL t_startf ('lpt_stepout')
		!!IF (masterproc) PRINT*,'lpt_stepout in'
		
		! Update resolved scale velocity at t=t+1.
		DO n = 1, np2
			CALL uvw_def_points( parcel(n)%var(1), parcel(n)%var(2), parcel(n)%var(3), i0, j0, k0, &
			                     km2, kp3, x, y, z )
			! u
			f216( :, -jm2:jp3, -km2(1):kp3(1) ) &
				= ures( i0(1)-2:i0(1)+3, j0(1)-jm2:j0(1)+jp3, k0(1)-km2(1):k0(1)+kp3(1) )
			parcel(n)%var(4) = interpolation(f216, x(1), y(1), z(1), k0(1), km2(1), kp3(1), 1, .FALSE.)
			! v
			f216( :, -jm2:jp3, -km2(2):kp3(2) ) &
				= vres( i0(2)-2:i0(2)+3, j0(2)-jm2:j0(2)+jp3, k0(2)-km2(2):k0(2)+kp3(2) )
			parcel(n)%var(5) = interpolation(f216, x(2), y(2), z(2), k0(2), km2(2), kp3(2), 1, .FALSE.)
			! w
			f216( :, -jm2:jp3, -km2(3):kp3(3) ) &
				= wres( i0(3)-2:i0(3)+3, j0(3)-jm2:j0(3)+jp3, k0(3)-km2(3):k0(3)+kp3(3) )
			parcel(n)%var(6) = interpolation(f216, x(3), y(3), z(3), k0(3), km2(3), kp3(3), 0, .FALSE.)
		ENDDO
		
		
		! Update parameterized ensemble-mean resolved scale TKE at t=t+1
		IF ( flag_lpt_sgs.OR.(flag_lpt_output.AND.flag_lpt_tke) ) THEN
			n = 1
			DO k = kbottom, ktop
				n1 = MIN( n+1, nzlpt )
				DO j = 1, ny
					DO i = 1, nx
						tke3D(i,j,n) = 0.5_rlpt * &
							( 0.5_rlpt * ( ( ures(i+1,j,n) - REAL(u0(k),rlpt) ) ** 2 &
							             + ( ures(i,j,n) - REAL(u0(k),rlpt) ) ** 2 ) &
							+ 0.5_rlpt * ( ( vres(i+1-YES3D,j+YES3D,n) - REAL(v0(k),rlpt) ) ** 2 &
							             + ( vres(i,j,n) - REAL(v0(k),rlpt) ) ** 2 ) &
							+ 0.5_rlpt * ( wres(i,j,n1) ** 2 + wres(i,j,n) ** 2 ) )
					ENDDO
				ENDDO
				n = n + 1
			ENDDO
			CALL task_exchange_lpt(tke3D, x1s, x2s, y1s, y2s, nzlpt, sx1, sx2, sy1, sy2, 6)
		ENDIF
		
		!!IF (masterproc) PRINT*,'lpt_stepout out'
		CALL t_stopf ('lpt_stepout')
		
	END SUBROUTINE lpt_stepout
	
	!=============================== PARCEL LOCATION IN DOMAIN ======================================
	
	SUBROUTINE uvw_def_points( xp, yp, zp, i, j, k, km2, kp3, x, y, z )
	
	! This subroutine finds necessary information for spatial interpolation for u, v, & w points
	
		IMPLICIT NONE
		
		REAL(KIND=rlpt), INTENT(IN) :: xp, yp, zp ! parcel position
		INTEGER, DIMENSION(3), INTENT(OUT) :: i, j, k ! index in the subdomain, 1 <= k <= nzlpt
		INTEGER, DIMENSION(3), INTENT(OUT) :: km2, kp3 ! extra levels for interpolation
		REAL(KIND=rlpt), DIMENSION(3), INTENT(OUT) :: x, y, z ! scaled distance by dx, dy, dz
		INTEGER :: m(1), n
		
		! Output
		! u: i(1), j(1), k(1), km2(1), kp3(1), x(1), y(1), z(1)
		! v: i(2), j(2), k(2), km2(2), kp3(2), x(2), y(2), z(2)
		! w: i(3), j(3), k(3), km2(3), kp3(3), x(3), y(3), z(3)
		
		! x direction
		i(1) = FLOOR( xp * idx ) + 1 - nx_sb   ! x wall
		i(3) = FLOOR( xp * idx + 0.5_rlpt ) - nx_sb   ! x center
		IF (i(1) > nx+1) THEN 
			PRINT*,'rank, xp', rank, xp
		ENDIF
		
		x(1) = ( xp - gpxs(i(1)) ) * idx
		x(3) = ( xp - gpxc(i(3)) ) * idx
		IF (RUN3D) THEN
			i(2) = i(3)
			x(2) = x(3)
		ELSE
			i(2) = i(1)
			x(2) = x(1)
		ENDIF
		
		! y direction
		IF (RUN3D) THEN
			j(1) = FLOOR( yp * idy + 0.5_rlpt ) - ny_sb   ! y center
			j(2) = FLOOR( yp * idy ) + 1 - ny_sb   ! y wall
			j(3) = j(1)
			y(1) = ( yp - gpyc(j(1)) ) * idy
			y(2) = ( yp - gpys(j(2)) ) * idy
			y(3) = y(1)
		ELSE
			j(1:3) = 1
			y(1:3) = 0.0_rlpt
		ENDIF
		
		! z direction (below/on zp)
		! - In this part, k ranges between kbottom and ktop, not between 1 and nzlpt. Adjustment is
		!   made later
		! - k(1) (and k(2)) have possibility to become 0 but this can be avoided by specifying
		!   zbottom at least higher than the first scalar level from the surface. However this is not
		!   enough since this routine is called durging iterative Huen scheme. zp for the iteration
		!   may become below zbottom.
		! - k(3) has possibility to become nzm but this can be avoided by specifiying ztop at least
		!   a few levels lower than the domain top. THIS IS A REQUIREMENT.
		IF ( uniform_z ) THEN
			! z center: may become 0 below gpzc(1)
			k(1) = FLOOR( zp * idz + 0.5_rlpt )
			! Adjust k(1) if zp < gpzc(1)
			IF ( k(1) == 0 ) k(1) = 1
			z(1) = ( zp - gpzc(k(1)) ) * idz
			! z wall
			k(3) = FLOOR( zp * idz ) + 1
			z(3) = ( zp - gpzs(k(3)) ) * idz
		ELSE
			! Specify index at scalar point ( = u & v )
			! MINLOC returns value between 1 and nzlpt
			! Convert 1 <= m <= nzlpt to kbottom <= m <= ktop by adding kbottom-1
			m = MINLOC( ABS( gpzc(kbottom:ktop) - zp ) ) + kbottom - 1
			! MINLOC outputs an array with the one less dimension of the input array (i.e., gpzc),
			! store the output, m(1), to integer, n.
			n = m(1)
			IF ( gpzc(n) > zp ) n = n - 1   ! n may become 0 below gpzc(1)
			! Adjust k(1) if n = 0.
			IF ( n >= 1 ) THEN
				k(1) = n
			ELSE
				k(1) = 1
			ENDIF
			z(1) = ( zp - gpzc(k(1)) ) / ( gpzc(k(1)+1) - gpzc(k(1)) )
			! Specify index for w
			IF ( zp >= gpzs(n+1) ) n = n + 1   ! n is never below 1 because gpzs(1) = 0.
			k(3) = n
			z(3) = ( zp - gpzs(n) ) / ( gpzs(n+1) - gpzs(n) )
		ENDIF
		k(2) = k(1)
		z(2) = z(1)
		
		! Extra levels depending on interpolation scheme
		! At this point, 1 <= k(1:3) < nzm
		SELECT CASE(poly_order)
		CASE(2)   ! Trilinear interpolation (2-level interpolation)
			km2(:) = 0
			kp3(:) = 1
		CASE(3)   ! 3rd-order Lagrange polynomial interpolation (4-level interpolation)
			km2(:) = 1
			kp3(:) = 2
			! Adjustment to use linear interpolation around bottom and top boundary
			SELECT CASE(k(1))
			CASE(1)
				km2(1:2) = 0
			CASE(nzm-2:nzm)
				kp3(1:2) = 1
			END SELECT
			SELECT CASE(k(3))
			CASE(1)
				km2(3) = 0
			CASE(nzm-2:nzm)
				kp3(3) = 1
			END SELECT
		CASE(5)  ! 5th-order Lagrange polynomial interpolation (6-level interpolation)
			km2(:) = 2
			kp3(:) = 3
			! Adjustment to use either linear interpolation or 3rd-order Lagrange interpolation
			! around bottom and top boundary
			SELECT CASE(k(1))
			CASE(1)
				km2(1:2) = 0
			CASE(2)
				km2(1:2) = 1
			CASE(nzm-3)
				kp3(1:2) = 2
			CASE(nzm-2:nzm)
				kp3(1:2) = 1
			END SELECT
			SELECT CASE(k(3))
			CASE(1)
				km2(3) = 0
			CASE(2)
				km2(3) = 1
			CASE(nzm-3)
				kp3(3) = 2
			CASE(nzm-2:nzm)
				kp3(3) = 1
			END SELECT
		END SELECT   ! poly_order
		
		! Adjust k by kbottom so that 1 <= k < nzlpt
		k(:) = k(:) - kbottom + 1
		
	END SUBROUTINE uvw_def_points
	
	!------------------------------------------------------------------------------------------
	
	SUBROUTINE scalar_def_point( xp, yp, zp, i, j, k, km2, kp3, x, y, z )
	
	! This subroutine finds necessary information for spatial interpolation for scalar
	
		IMPLICIT NONE
		
		REAL(KIND=rlpt), INTENT(IN) :: xp, yp, zp
		INTEGER, INTENT(OUT) :: i, j, k
		INTEGER, INTENT(OUT) :: km2, kp3
		REAL(KIND=rlpt), INTENT(OUT) :: x, y, z
		INTEGER :: m(1), n
		
		! x direction
		i = FLOOR( xp * idx + 0.5_rlpt ) - nx_sb   ! x center
		x = ( xp - gpxc(i) ) * idx
		
		! y direction
		IF (RUN3D) THEN
			j = FLOOR( yp * idy + 0.5_rlpt ) - ny_sb   ! y center
			y = ( yp - gpyc(j) ) * idy
		ELSE
			j = 1
			y = 0.0_rlpt
		ENDIF
		
		! z direction
		! Bottom boundary adjustment is required like uvw_def_points, since this routine is called in
		! the SGS velocity caluculation and this routine is used to estimates the interpolation
		! infromation for future position based only on resolved velocity.
		IF ( uniform_z ) THEN
			k = FLOOR( zp * idz + 0.5_rlpt )   ! z center
			IF ( k == 0 ) k = 1
			z = ( zp - gpzc(k) ) * idz
		ELSE
			! z center
			m = MINLOC( ABS( gpzc(kbottom:ktop) - zp ) ) + kbottom - 1
			n = m(1)
			IF ( gpzc(n) > zp ) n = n - 1
			IF ( n >= 1 ) THEN
				k = n
			ELSE
				k = 1
			ENDIF
			z = ( zp - gpzc(k) ) / ( gpzc(k+1) - gpzc(k) )
		ENDIF
		
		! Extra levels depending on interpolation scheme
		! At this point, 1 <= k < nzm
		SELECT CASE(poly_order)
		CASE(2)
			km2 = 0
			kp3 = 1
		CASE(3)
			km2 = 1
			kp3 = 2
			SELECT CASE(k)
			CASE(1)
				km2 = 0
			CASE(nzm-2:nzm)
				kp3 = 1
			END SELECT
		CASE(5)
			km2 = 2
			kp3 = 3
			SELECT CASE(k)
			CASE(1)
				km2 = 0
			CASE(2)
				km2 = 1
			CASE(nzm-3)
				kp3 = 2
			CASE(nzm-2:nzm)
				kp3 = 1
			END SELECT
		END SELECT   ! poly_order
		
		! Adjust k by kbottom
		k = k - kbottom + 1
		
	END SUBROUTINE scalar_def_point
	
	!------------------------------------------------------------------------------------------
	
	SUBROUTINE sw_def_points( xp, yp, zp, i, j, k, km2, kp3, x, y, z )
	
	! This subroutine finds necessary information for spatial interpolation for u, v, w & s points
	
		IMPLICIT NONE
		
		REAL(KIND=rlpt), INTENT(IN) :: xp, yp, zp
		INTEGER, DIMENSION(2), INTENT(OUT) :: i, j, k
		INTEGER, DIMENSION(2), INTENT(OUT) :: km2, kp3
		REAL(KIND=rlpt), DIMENSION(2), INTENT(OUT) :: x, y, z
		INTEGER :: m(1), n
		
		! x direction
		i(1) = FLOOR( xp * idx + 0.5_rlpt ) - nx_sb   ! x center
		i(2) = i(1)
		x(1) = ( xp - gpxc(i(1)) ) * idx
		x(2) = x(1)
		
		! y direction
		IF (RUN3D) THEN
			j(1) = FLOOR( yp * idy + 0.5_rlpt ) - ny_sb   ! y center
			j(2) = j(1)
			y(1) = ( yp - gpyc(j(1)) ) * idy
			y(2) = y(1)
		ELSE
			j(1:2) = 1
			y(1:2) = 0.0_rlpt
		ENDIF
		
		! z direction
		IF ( uniform_z ) THEN
			! z center
			k(1) = FLOOR( zp * idz + 0.5_rlpt )
			IF ( k(1) == 0 ) k(1) = 1
			z(1) = ( zp - gpzc(k(1)) ) * idz
			! z wall
			k(2) = FLOOR( zp * idz ) + 1
			z(2) = ( zp - gpzs(k(2)) ) * idz
		ELSE
			! z center
			m = MINLOC( ABS( gpzc(kbottom:ktop) - zp ) ) + kbottom - 1
			n = m(1)
			IF ( gpzc(n) > zp ) n = n - 1
			IF ( n >= 1 ) THEN
				k(1) = n
			ELSE
				k(1) = 1
			ENDIF
			z(1) = ( zp - gpzc(k(1)) ) / ( gpzc(k(1)+1) - gpzc(k(1)) )
			! z wall
			IF ( zp >= gpzs(n+1) ) n = n + 1
			k(2) = n
			z(2) = ( zp - gpzs(n) ) / ( gpzs(n+1) - gpzs(n) )
		ENDIF
		
		! Extra levels
		SELECT CASE(poly_order)
		CASE(2)
			km2(:) = 0
			kp3(:) = 1
		CASE(3)
			km2(:) = 1
			kp3(:) = 2
			SELECT CASE(k(1))
			CASE(1)
				km2(1) = 0
			CASE(nzm-2:nzm)
				kp3(1) = 1
			END SELECT
			SELECT CASE(k(2))
			CASE(1)
				km2(2) = 0
			CASE(nzm-2:nzm)
				kp3(2) = 1
			END SELECT
		CASE(5)
			km2(:) = 2
			kp3(:) = 3
			SELECT CASE(k(1))
			CASE(1)
				km2(1) = 0
			CASE(2)
				km2(1) = 1
			CASE(nzm-3)
				kp3(1) = 2
			CASE(nzm-2:nzm)
				kp3(1) = 1
			END SELECT
			SELECT CASE(k(2))
			CASE(1)
				km2(2) = 0
			CASE(2)
				km2(2) = 1
			CASE(nzm-3)
				kp3(2) = 2
			CASE(nzm-2:nzm)
				kp3(2) = 1
			END SELECT
		END SELECT   ! poly_order
		
		! Adjust k by kbottom
		k(:) = k(:) - kbottom + 1
		
	END SUBROUTINE sw_def_points
	
	!===================================== INTERPOLATION ======================================
	
	FUNCTION interpolation( f_in, x, y, z, k0, km2, kp3, ztype, positive ) RESULT( f_out )
	
	! This is wrapper function to pass necessary variable to each interpolation scheme.
	
		IMPLICIT NONE
		
		REAL(KIND=rlpt), DIMENSION(-2:3,-2:3,-2:3), INTENT(IN) :: f_in ! contains values only
		                                                               ! between -km2 and kp3
		REAL(KIND=rlpt), INTENT(IN) :: x, y, z ! z < 0 if parcel is below gpzc(1)
		INTEGER, INTENT(IN) :: k0 ! level on or just below parcel, 1 <= k0 <= nzlpt.
		                          ! k0 = 1 if pracel is below gpzc(1).
		INTEGER, INTENT(IN) :: km2, kp3 ! extra levels below and above the level k0 for interpolation
		                                ! km2=[2,1,0], kp3=[3,2,1]
		INTEGER, INTENT(IN) :: ztype ! 1 for zc (center level), 0 for zs (interface level)
		LOGICAL, INTENT(IN) :: positive
		REAL(KIND=rlpt) :: f_out
		
		! local
		REAL(KIND=rlpt), DIMENSION(0:1,0:1,0:1) :: f8
		INTEGER :: k00
		
		! k00 is index in terms of the SAM level (1<= k00 <= nzm).
		k00 = k0 + kbottom - 1
		
		SELECT CASE (poly_order)
		CASE(2) ! Trilinear interpolation
			f8(:,0:1*YES3D,:) = f_in(0:1,0:1*YES3D,0:1)
			f_out = trilinear_interpolation( f8, x, y, z, k00, 0 )
		CASE(3) ! 3rd-order Lagrang interpolation
			f_out = lagrange_interpolation( f_in, x, y, z, k00, km2, kp3, ztype, positive )
		CASE(5) ! 5th-order Lagrang interpolation
			f_out = lagrange_interpolation( f_in, x, y, z, k00, km2, kp3, ztype, positive )
		END SELECT
	
	END FUNCTION interpolation
	
	!------------------------------------------------------------------------------------------
	
	FUNCTION trilinear_interpolation( f, x, y, z, k0, derivative ) RESULT( f1 )
	
	!      f(0,1,1) ---------- f(1,1,1)
	!        / |                /  |
	!     f(0,0,1) ---------- f(1,0,1)
	!      |   |              |    |
	!      |   |         + <==|=== |======= parcel
	!      | f(0,1,0) --------|- f(1,1,0)
	!      | /                |  /
	!     f(0,0,0) ---------- f(1,0,0)
	!
	! F(x,y,z) = c0 + c1*x + c2*y + c3*z + c4*x*y + c5*y*z + c6*z*x + c7*x*y*z
	!
	! Input:
	! - f: closest 2x2x2 grid value
	! - x, y, z: relative positon from f(0,0,0) and scaled with grid space
	! - k0: vertical position index. Significant for non-uniform z
	! Option:
	! - derivative: 0 for function value, 1 for df/dx, 2 for df/dy, 3 for df/dz for variable defined
	!   on z, 4 for df/dz for vriable defined on zi
	
		IMPLICIT NONE
		
		! input
		REAL(KIND=rlpt), DIMENSION(0:1,0:1,0:1), INTENT(IN) :: f
		REAL(KIND=rlpt), INTENT(IN) :: x, y, z
		INTEGER, INTENT(IN) :: k0, derivative
		
		! output
		REAL(KIND=rlpt) :: f1
		
		! local
		REAL(KIND=rlpt) :: c0, c1, c2, c3, c4, c5, c6, c7
		
		c0 = f(0,0,0)
		c1 = - f(0,0,0) + f(1,0,0)
		c3 = - f(0,0,0) + f(0,0,1)
		c6 = f(0,0,0) - f(0,0,1) - f(1,0,0) + f(1,0,1)
		IF (RUN3D) THEN
			c2 = - f(0,0,0) + f(0,1*YES3D,0)
			c4 = f(0,0,0)  - f(0,1*YES3D,0) - f(1,0,0) + f(1,1*YES3D,0)
			c5 = f(0,0,0) - f(0,0,1) - f(0,1*YES3D,0) + f(0,1*YES3D,1)
			c7 = - f(0,0,0) + f(0,0,1) + f(0,1*YES3D,0) - f(0,1*YES3D,1) + f(1,0,0) - f(1,0,1) &
			     - f(1,1*YES3D,0) + f(1,1*YES3D,1)
		ELSE   ! bilinear interpolation for 2D run
			c2 = 0.0_rlpt
			c4 = 0.0_rlpt
			c5 = 0.0_rlpt
			c7 = 0.0_rlpt
		ENDIF
		
		SELECT CASE(derivative)
		CASE(0)
			f1 = c0 + c1 * x + c2 * y + c3 * z + c4 * x * y + c5 * y * z + c6 * z * x + c7 * x * y * z
		CASE(1)   ! return partial df/dx
			f1 = ( c1 + c4 * y + c6 * z + c7 * y * z ) * idx
		CASE(2)   ! return partial df/dy
			f1 = ( c2 + c4 * x + c5 * z + c7 * x * z ) * idy
		CASE(3:4)   ! return partial df/dz
			f1 = c3 + c5 * y + c6 * x + c7 * x * y
			IF ( uniform_z ) THEN
				f1 = f1 * idz
			ELSE
				IF ( derivative == 3 ) f1 = f1 / ( gpzc(k0+1) - gpzc(k0) )
				IF ( derivative == 4 ) f1 = f1 / ( gpzs(k0+1) - gpzs(k0) )
			ENDIF
		END SELECT
		
	END FUNCTION trilinear_interpolation
	
	!------------------------------------------------------------------------------------------
	
	FUNCTION lagrange_interpolation( f, x, y, z, k0, km2, kp3, ztype, positive ) RESULT( f3 )
	
	! Third- or fifth-order Lagrange interpolation
	!
	! Input: 
	! - f: closest 6x6x6 grid value
	! - x, y, and z: relative position from f(0,0,0) and scaled with dx, dy, dz
	! - k0: vertical position index. Significant for non-uniform z
	! - km2, kp3: extra levels required for interpolation. If these values are not enough, then
	!   vertical interpolation is degraded
	! - ztype: 1 for zc (center level), 0 for zs (interface level), This is used for non-uniform z.
	! Option:
	! - positive: if true, trilinear interpolation is used if interpolated value is less than zero.
	!
	!      f(0,1,1) ---------- f(1,1,1)
	!        / |                /  |
	!    f(0,0,1) ---------- f(1,0,1)
	!      |   |              |    |
	!      |   |         + <==|=== |======= parcel
	!      | f(0,1,0) --------|- f(1,1,0)
	!      | /                |  /
	!    f(0,0,0) ---------- f(1,0,0)
	!
	! 1. Interpolation for x direction
	! 2. Interpolation for y direction for 3D run
	! 3. Interpolation for z direction
	! 4. positive option
	
		IMPLICIT NONE
	
		! input
		REAL(KIND=rlpt), DIMENSION(-2:3,-2:3,-2:3), INTENT(IN) :: f
		REAL(KIND=rlpt), INTENT(IN) :: x, y, z
		INTEGER, INTENT(IN) :: k0, km2, kp3
		INTEGER, INTENT(IN) :: ztype
		LOGICAL, INTENT(IN) :: positive
	
		! output
		REAL(KIND=rlpt) :: f3
		
		! local
		REAL(KIND=rlpt), DIMENSION(-2:3,-2:3) :: f1
		REAL(KIND=rlpt), DIMENSION(-2:3) :: f2
		INTEGER :: j, k
		REAL(KIND=rlpt), DIMENSION(0:1,0:1,0:1) :: f8
		
		SELECT CASE( poly_order)
		CASE(3)   ! 3rd-order Lagrange interpolation
			! Interpolation in x direction
			DO k = -km2, kp3
				DO j = -jm1, jp2
					f1(j,k) = lagrange_interpolation_3rd( f(-1,j,k), f(0,j,k), f(1,j,k), f(2,j,k), x )
				ENDDO
			ENDDO
			
			!Interpolation in y direction
			IF (RUN3D) THEN
				DO k = -km2, kp3
					f2(k) = lagrange_interpolation_3rd( f1(-1,k), f1(0,k), f1(1,k), f1(2,k), y )
				ENDDO
			ELSE
				f2(:) = f1(0,:)
			ENDIF
			
			! Interpolation in z direction
			IF ( km2 == 1 .AND. kp3 == 2 ) THEN
				IF ( uniform_z ) THEN
					f3 = lagrange_interpolation_3rd( f2(-1), f2(0), f2(1), f2(2), z )
				ELSE
					f3 = lagrange_interpolation_3rd_nuz( f2(-1), f2(0), f2(1), f2(2), z, k0, ztype )
				ENDIF
			ELSE
				! Linear interpolation
				f3 = f2(0) * ( 1.0_rlpt - z ) + f2(1) * z
			ENDIF
			
		CASE(5)   ! 5th-order Lagrange interpolation
			! Interpolation in x direction
			DO k = -km2, kp3
				DO j = -jm2, jp3
					f1(j,k) = lagrange_interpolation_5th( f(-2,j,k), f(-1,j,k), f(0,j,k), f(1,j,k), &
					                                      f(2,j,k), f(3,j,k), x )
				ENDDO
			ENDDO
			
			! Interpolation in y direction
			IF (RUN3D) THEN
				DO k = -km2, kp3
					f2(k) = lagrange_interpolation_5th( f1(-2,k), f1(-1,k), f1(0,k), f1(1,k), f1(2,k), &
					                                    f1(3,k), y )
				ENDDO
			ELSE
				f2(:) = f1(0,:)
			ENDIF
			
			! Interpolation for z direction
			IF ( km2 == 2 .AND. kp3 == 3 ) THEN
				IF ( uniform_z ) THEN
					f3 = lagrange_interpolation_5th( f2(-2), f2(-1), f2(0), f2(1), f2(2), f2(3), z )
				ELSE
					f3 = lagrange_interpolation_5th_nuz( f2(-2), f2(-1), f2(0), f2(1), f2(2), f2(3), &
					                                     z, k0, ztype )
				ENDIF
			ELSE IF ( km2 == 1 .AND. kp3 == 2 ) THEN
				! 3rd-order Lagrange interpolation
				IF ( uniform_z ) THEN
					f3 = lagrange_interpolation_3rd( f2(-1), f2(0), f2(1), f2(2), z )
				ELSE
					f3 = lagrange_interpolation_3rd_nuz( f2(-1), f2(0), f2(1), f2(2), z, k0, ztype )
				ENDIF
			ELSE
				! Linear interpolation
				f3 = f2(0) * ( 1.0_rlpt - z ) + f2(1) * z
			ENDIF
		
		CASE DEFAULT
			! NO OTHER ORDER IS IMPLEMENTED
		END SELECT
		
		! Positive option
		IF ( positive .AND. f3 < 0.0_rlpt ) THEN
			f8(:,0:1*YES3D,:) = f(0:1,0:1*YES3D,0:1)
			f3 = trilinear_interpolation( f8, x, y, z, k0, 0 )
		ENDIF
		
	END function lagrange_interpolation
	
	!------------------------------------------------------------------------------------------
	
	FUNCTION lagrange_interpolation_3rd( fm1, f0, fp1, fp2, x ) RESULT( f )
	
	! Returns interpolated value with 3rd-order Lagrange polynomial function for uniform grid
	
		IMPLICIT NONE
		
		REAL(KIND=rlpt), INTENT(IN) :: fm1, f0, fp1, fp2, x
		REAL(KIND=rlpt) :: f
		
		f = f0 &
		  + x * ( - 2.0_rlpt * fm1 - 3.0_rlpt * f0 + 6.0_rlpt * fp1 - fp2 ) / 6.0_rlpt &
		  + ( x**2 ) * ( fm1 - 2.0_rlpt * f0 + fp1 ) * 0.5_rlpt &
		  + ( x**3 ) * ( - fm1 + 3.0_rlpt * f0 - 3.0_rlpt * fp1 + fp2 ) / 6.0_rlpt
		
	END FUNCTION lagrange_interpolation_3rd
	
	!------------------------------------------------------------------------------------------
	
	FUNCTION lagrange_interpolation_5th( fm2, fm1, f0, fp1, fp2, fp3, x ) RESULT( f )
	
	! Returns interpolated value with 5th-order Lagrange polynomial function for uniform grid
	
		IMPLICIT NONE
		
		REAL(KIND=rlpt), INTENT(IN) :: fm2, fm1, f0, fp1, fp2, fp3, x
		REAL(KIND=rlpt) :: f
		
		f = f0 &
		  + x * ( 3.0_rlpt * fm2 - 30.0_rlpt * fm1 - 20.0_rlpt * f0 + 60.0_rlpt * fp1 &
		    - 15.0_rlpt * fp2 + 2.0_rlpt * fp3 ) / 60.0_rlpt &
		  + ( x**2 ) * ( - fm2 + 16.0_rlpt * fm1 - 30.0_rlpt * f0 + 16.0_rlpt * fp1 - fp2 ) &
		    / 24.0_rlpt&
		  + ( x**3 ) * ( - fm2 - fm1 + 10.0_rlpt * f0 - 14.0_rlpt * fp1 + 7.0_rlpt * fp2 - fp3 ) &
		    / 24.0_rlpt  &
		  + ( x**4 ) * ( fm2 - 4.0_rlpt * fm1 + 6.0_rlpt * f0 - 4.0_rlpt * fp1 + fp2 ) / 24.0_rlpt &
		  + ( x**5 ) * ( - fm2 + 5.0_rlpt * fm1 - 10.0_rlpt * f0 + 10.0_rlpt * fp1 - 5.0_rlpt * fp2 &
		    + fp3 ) / 120.0_rlpt
		
	END FUNCTION lagrange_interpolation_5th
	
	!------------------------------------------------------------------------------------------
	
	FUNCTION lagrange_interpolation_3rd_nuz( fm1, f0, fp1, fp2, z, k0, ztype ) RESULT( f )
	
	! Based on polint in "Numerical Recipes in Fortran 90: the Art of Parallel Scientific Computing"
	! Modified from subroutine to function. Given function values fm1, f0, fp1, fp2, and normalized
	! parcel vertical position, z, this routine returns a value f with 3rd-order Lagrange polynomial
	! interpolation.
	
		IMPLICIT NONE
		
		REAL(KIND=rlpt), INTENT(IN) :: fm1, f0, fp1, fp2, z
		INTEGER, INTENT(IN) :: k0
		INTEGER, INTENT(IN) :: ztype
		REAL(KIND=rlpt) :: f
		
		! Local
		INTEGER, PARAMETER :: n = 4, km = 1, kp = 2
		REAL(KIND=rlpt), DIMENSION(n) :: c, d, den, ho
		REAL(KIND=rlpt) :: df
		INTEGER :: m, ns
		
		! Initialize the tableau of c's and d's
		c(:) = (/ fm1, f0, fp1, fp2 /)
		d(:) = c(:)
		
		! Normalized levels
		SELECT CASE( ztype )
		CASE(0) ! w level
			ho(:) = ( gpzs(k0-km:k0+kp) - gpzs(k0) ) / ( gpzs(k0+1) - gpzs(k0) ) - z
		CASE(1) ! s level
			ho(:) = ( gpzc(k0-km:k0+kp) - gpzc(k0) ) / ( gpzc(k0+1) - gpzc(k0) ) - z
		END SELECT
		
		! Find index ns of closest table entry. ns is either 2 (at k0) or 3 (at k0+1)
		! Initial approximation to f
		IF ( z <= 0.5 ) THEN   ! 0 <= z <= 1 due to normalization
			ns = 2
			f = f0
		ELSE
			ns = 3
			f = fp1
		ENDIF
		
		! For each column of the tableau, loop over the current c's and d's and update them.
		ns = ns - 1
		DO m = 1, n-1
			den(1:n-m) = ho(1:n-m) - ho(1+m:n)
			den(1:n-m) = ( c(2:n-m+1) - d(1:n-m) ) / den(1:n-m)
			
			! Here the c’s and d’s are updated.
			d(1:n-m) = ho(1+m:n) * den(1:n-m)
			c(1:n-m) = ho(1:n-m) * den(1:n-m)
			
			! After each column in the tableau is completed, we decide which correction, c or d, we
			! want to add to our accumulating value of y, i.e., which path to take through the
			! tableau—forking up or down. We do this in such a way as to take the most "straight line"
			! route through the tableau to its apex, updating ns accordingly to keep track of where we
			! are. This route keeps the partial approxima- tions centered (insofar as possible) on the
			! target x. The last dy added is thus the error indication.
			IF ( 2*ns < n-m ) THEN
				df = c(ns+1)
			ELSE
				df = d(ns)
				ns = ns - 1
			ENDIF
			f = f + df
		ENDDO
		
	END FUNCTION lagrange_interpolation_3rd_nuz
	
	!------------------------------------------------------------------------------------------
	
	FUNCTION lagrange_interpolation_5th_nuz( fm2, fm1, f0, fp1, fp2, fp3, z, k0, ztype ) RESULT( f )
	
	! Based on polint in "Numerical Recipes in Fortran 90: the Art of Parallel Scientific Computing"
	! Modified from subroutine to function. Given function values fm1, f0, fp1, fp2, and normalized
	! parcel vertical position, z, this routine returns a value f with 5th-order Lagrange polynomial
	! interpolation.
	
		IMPLICIT NONE
		
		REAL(KIND=rlpt), INTENT(IN) :: fm2, fm1, f0, fp1, fp2, fp3, z
		INTEGER, INTENT(IN) :: k0
		INTEGER, INTENT(IN) :: ztype
		REAL(KIND=rlpt) :: f
		
		INTEGER, PARAMETER :: n = 6, km = 2, kp = 3
		REAL(KIND=rlpt), DIMENSION(n) :: c, d, den, ho
		REAL(KIND=rlpt) :: df
		INTEGER :: m, ns
		
		! Initialize the tableau of c's and d's
		c(:) = (/ fm2, fm1, f0, fp1, fp2, fp3 /)
		d(:) = c(:)
		
		! Normalized levels
		SELECT CASE( ztype )
		CASE(0) ! w level
			ho(:) = ( gpzs(k0-km:k0+kp) - gpzs(k0) ) / ( gpzs(k0+1) - gpzs(k0) ) - z
		CASE(1) ! s level
			ho(:) = ( gpzc(k0-km:k0+kp) - gpzc(k0) ) / ( gpzc(k0+1) - gpzc(k0) ) - z
		END SELECT
		
		! Find index ns of closest table entry. ns is either 3 (at k0) or 4 (at k0+1)
		! Initial approximation to f
		IF ( z <= 0.5 ) THEN   ! 0 <= z <= 1 due to normalization
			ns = 3
			f = f0
		ELSE
			ns = 4
			f = fp1
		ENDIF
		
		! For each column of the tableau, loop over the current c's and d's and update them.
		ns = ns - 1
		DO m = 1, n-1
			den(1:n-m) = ho(1:n-m) - ho(1+m:n)
			den(1:n-m) = ( c(2:n-m+1) - d(1:n-m) ) / den(1:n-m)
			
			! Here the c’s and d’s are updated.
			d(1:n-m) = ho(1+m:n) * den(1:n-m)
			c(1:n-m) = ho(1:n-m) * den(1:n-m)
			
			! After each column in the tableau is completed, we decide which correction, c or d, we
			! want to add to our accumulating value of y, i.e., which path to take through the
			! tableau—forking up or down. We do this in such a way as to take the most "straight line"
			! route through the tableau to its apex, updating ns accordingly to keep track of where we
			! are. This route keeps the partial approxima- tions centered (insofar as possible) on the
			! target x. The last dy added is thus the error indication.
			IF ( 2*ns < n-m ) THEN
				df = c(ns+1)
			ELSE
				df = d(ns)
				ns = ns - 1
			ENDIF
			f = f + df
		ENDDO
		
	END FUNCTION lagrange_interpolation_5th_nuz
	
	!========================= RANDOM NUMBER OF THE GAUSS DISTRIBUTION ========================
	
	FUNCTION num_gauss( sigma ) RESULT( ng )
	
	! Returns random number of the gauss distribution with zero mean and variance of sigma^2.
	! Number within two standard deviation (2*sigma) is returned.
	
		IMPLICIT NONE
		! input
		REAL(KIND=rlpt), INTENT(IN) :: sigma
		! output
		REAL(KIND=rlpt) :: ng
		! local
		REAL(KIND=rlpt), PARAMETER :: nsigma = 2
		
		DO
			ng = gasdev( rank+1, sigma )
				! manually convert rlpt real of sigma to 4 byte real to pass gasdev
				! manually convert 4 byte real of gasdev to rlpt real
			IF ( abs(ng) <= nsigma * sigma ) EXIT
		ENDDO
		
	END FUNCTION num_gauss
	
	!------------------------------------------------------------------------------------------
	
	REAL(rlpt) FUNCTION gasdev(idum, sigma)
	
	! gasdev_s in "Numerical Recipes in Fortran 90: the Art of Parallel Scientific Computing"
	! gasdev in "Numerical Recipes in Fortran 77: the Art of Scientific Computing"
	! Modified from subroutine to function. Returns a normally distributed deviate with zero
	! mean and variance of sigma, using ran(idum) as the source of unit deviates.
	! Input idum has to be positive. For LPT, idum = rank+1
	
		IMPLICIT NONE
		INTEGER, PARAMETER :: K4B=selected_int_kind(9)
		INTEGER(K4B), INTENT(IN) :: idum
		REAL(KIND=rlpt), INTENT(IN) :: sigma   ! mean and standard deviation
		REAL(KIND=rlpt) :: rsq, v1, v2
		REAL(KIND=rlpt), SAVE :: gnext
		LOGICAL, SAVE :: gaus_stored = .FALSE.
		
		IF (gaus_stored) THEN   ! We have an extra deviate handy, so return it, and unset the flag.
			gasdev = gnext
			gaus_stored = .FALSE.
		ELSE   ! We don't have an extra deviate handy, so pick two uniform numbers
			DO	
				v1 = 2.0_rlpt * randomu(idum) - 1.0_rlpt
				v2 = 2.0_rlpt * randomu(idum) - 1.0_rlpt
				rsq = v1**2 + v2**2
				IF (rsq > 0.0_rlpt .AND. rsq < 1.0_rlpt) EXIT   ! See if they are in the unit circle,
			ENDDO                                              ! otherwise try again
			rsq = SQRT(-2.0_rlpt * log(rsq) / rsq)   ! Now make the Box-Muller transformation to get
			gasdev = v1 * rsq * sigma                ! two normal deviates. Return one and save the
			gnext  = v2 * rsq * sigma                ! other for next time.
			gaus_stored = .TRUE.   ! Set flag
		ENDIF
		
	END FUNCTION gasdev
	
	!------------------------------------------------------------------------------------------
	
	REAL(rlpt) FUNCTION randomu(idum)
	
	! "ram" in "Numerical Recipes in Fortran 90: the Art of Parallel Scientific Computing"
	! "Minimal" random number generator of Park and Miller combined with a Marsaglia shift sequence.
	! Returns a uniform random deviate between 0.0 and 1.0 (exclusive of the endpoint values).
	! This fully portable, scalar generator has the "traditional" (not F90) calling sequence with a
	! random deviate as the returned function value: call with idum a nagative integer to initialize;
	! thereafter, do not alter idum except to reinitialize. The period of this generator is about 
	! 3.1x10^18
	
		IMPLICIT NONE
		INTEGER, PARAMETER :: K4B=SELECTED_INT_KIND(9)
		!!INTEGER(K4B), INTENT(INOUT) :: idum
		INTEGER(K4B), INTENT(IN) :: idum
		INTEGER(K4B), PARAMETER :: IA=16870, IM=2147483647, IQ=127773, IR=2836
		REAL(KIND=rlpt), SAVE :: am
		INTEGER(K4B), SAVE :: ix=-1, iy=-1, k
		
		IF (idum <= 0 .OR. iy < 0) THEN   ! initialize
			am = nearest(1.0,-1.0) / IM
			iy = ior(ieor(888889999,abs(idum)),1)
			ix = ieor(777755555,abs(idum))
			!!idum = abs(idum) + 1   ! set idum positive
		ENDIF
		ix = ieor(ix,ishft(ix,13))   ! Marsaglia shift sequence with period 2^23 - 1
		ix = ieor(ix,ishft(ix,-17))
		ix = ieor(ix,ishft(ix,5))
		k = iy / IQ   ! Park-Miller sequence by Schrage's method, period 2^31 - 2
		iy = IA * (iy - k * IQ) - IR * k
		IF (iy < 0) iy = iy + IM
		randomu = am * ior(iand(IM,ieor(ix,iy)),1)   ! Combine the two generators with masking to
		                                             ! ensure nonzero value
	END FUNCTION randomu
	
	!========================================= OUTPUT =========================================
	
	SUBROUTINE lpt_output
	
	! Obtain scaler information by interpolation.
	! This subroutine has to be called when flag_lpt_output = .TRUE.
	
		IMPLICIT NONE
		
		INTEGER :: i, n, ierr
		
		CALL t_startf ('lpt_output')
		
		IF (masterproc) PRINT*,'LPT: Collecting lagrangian parcel data...'
		
		IF ( np2 > 0 ) THEN
			ALLOCATE( parcel2(np2), STAT=ierr )
		ELSE
			ALLOCATE( parcel2(0:0), STAT=ierr )
		ENDIF
		IF ( ierr /= 0 ) THEN
			PRINT*,'LPT ERROR: Failed to allocate arrays in lpt_output on proc:', rank
			CALL lpt_task_abort
		ENDIF
		
		! 1. Compute new scalar variables
		IF ( flag_lpt_gradri ) CALL gradient_richardson_number
		
		! 2. Loop through each parcel, interpolate scalar, then diagnose other scalars
		DO n = 1, np2
			! Store tag, position and velocity variables
			! Add ug and vg for horizontal flow velocity
			parcel2(n)%tag = parcel(n)%tag
			parcel2(n)%nvi = parcel(n)%nvi
			DO i = 1, nv00
				SELECT CASE(i)
				CASE(4) ! u velocity
					parcel2(n)%var(i) = parcel(n)%var(i) + REAL(ug,rlpt)
				CASE(5) ! v velocity
					parcel2(n)%var(i) = parcel(n)%var(i) + REAL(vg,rlpt)
				CASE DEFAULT
					parcel2(n)%var(i) = parcel(n)%var(i)
				END SELECT
			ENDDO
		ENDDO
		! Interpolate & diagnose scalar output, store in parcel2	
		CALL collect_output
		
		! 3. Collect all parcel data to rannk 0 and save and show info
		!    Deallocate parcel2 for next call
		CALL lpt_write
		
		CALL t_stopf ('lpt_output')
		
	END SUBROUTINE lpt_output
	
	!------------------------------------------------------------------------------------------
	
	SUBROUTINE gradient_richardson_number
	
	! Compute the gradient Richardson number
	! Richardson number will be located on w point.
	! Boundary exchanges for ures, vres have to be finished at this point.
	! The lowest level (surface/kbottom) will contains the value at the second level.
	
		IMPLICIT NONE
		
		REAL(KIND=rlpt), DIMENSION(nx,ny,2) :: thetav
		REAL(KIND=rlpt) :: rdz, dudz2, dvdz2, dtvdz, tv_at_zi
		INTEGER :: i, j, k
		
		! 1. Compute thetav at k=1 at scalar point.
		k = 1
		DO j = 1, ny
			DO i = 1, nx
				thetav(i,j,k) = ( t3D(i,j,k) - REAL(gamaz(kbottom-1+k),rlpt) &
					+ REAL(fac_cond,rlpt) * ( qcl3D(i,j,k) + qpl3D(i,j,k) ) &
					+ REAL(fac_sub,rlpt)  * ( qci3D(i,j,k) + qpi3D(i,j,k) ) ) &
					* ( 1.0_rlpt + REAL(epsv,rlpt) * qv3D(i,j,k) - qcl3D(i,j,k) - qci3D(i,j,k) &
					- qpl3D(i,j,k) - qpi3D(i,j,k) ) &
					* ( ( 1E3_rlpt / pres1D(k) ) ** ( REAL(rgas,rlpt) / REAL(cp,rlpt) ) )
			ENDDO
		ENDDO
		
		! 2. Compute gradient Richardson number, k starts from 2.
		IF ( uniform_z ) THEN
			DO k = 2, nzlpt
				DO j = 1, ny
					DO i = 1, nx
						dudz2 = 0.5_rlpt * &
							( ( ( ures(i,j,k) - ures(i,j,k-1) ) * idz ) ** 2 &
							+ ( ( ures(i+1,j,k) - ures(i+1,j,k-1) ) * idz ) ** 2 )
						
						dvdz2 = 0.5_rlpt * &
							( ( ( vres(i,j,k) - vres(i,j,k-1) ) * idz ) ** 2 &
							+ ( ( vres(i+1-YES3D,j+YES3D,k) - vres(i+1-YES3D,j+YES3D,k-1) ) * idz )**2 )
						
						thetav(i,j,2) = ( t3D(i,j,k) - REAL(gamaz(kbottom-1+k),rlpt) &
							+ REAL(fac_cond,rlpt) * ( qcl3D(i,j,k) + qpl3D(i,j,k) ) &
							+ REAL(fac_sub,rlpt)  * ( qci3D(i,j,k) + qpi3D(i,j,k) ) ) &
							* ( 1.0_rlpt + REAL(epsv,rlpt) * qv3D(i,j,k) - qcl3D(i,j,k) &
							- qci3D(i,j,k) - qpl3D(i,j,k) - qpi3D(i,j,k) ) &
							* (( 1E3_rlpt / pres1D(k) ) ** ( REAL(rgas,rlpt) / REAL(cp,rlpt) ))
						
						dtvdz = ( thetav(i,j,2) - thetav(i,j,1) ) * idz
						
						tv_at_zi = 0.5_rlpt * ( thetav(i,j,2) + thetav(i,j,1) )
						
						rig3D(i,j,k) = REAL(ggr,rlpt)*dtvdz / ( tv_at_zi*(dudz2+dvdz2+1.0E-10_rlpt) )
					ENDDO
				ENDDO
				thetav(:,:,1) = thetav(:,:,2)
			ENDDO
		ELSE
			DO k = 2, nzlpt
				rdz = 1.0_rlpt / ( gpzc(kbottom+k-1) - gpzc(kbottom+k-2) )
				DO j = 1, ny
					DO i = 1, nx
						dudz2 = 0.5_rlpt * &
							( ( ( ures(i,j,k) - ures(i,j,k-1) ) * rdz ) ** 2 &
							+ ( ( ures(i+1,j,k) - ures(i+1,j,k-1) ) * rdz ) ** 2 )
						
						dvdz2 = 0.5_rlpt * &
							( ( ( vres(i,j,k) - vres(i,j,k-1) ) * rdz ) ** 2 &
							+ ( ( vres(i+1-YES3D,j+YES3D,k) - vres(i+1-YES3D,j+YES3D,k-1) ) * rdz )**2 )
						
						thetav(i,j,2) = ( t3D(i,j,k) - REAL(gamaz(kbottom-1+k),rlpt) &
							+ REAL(fac_cond,rlpt) * ( qcl3D(i,j,k) + qpl3D(i,j,k) ) &
							+ REAL(fac_sub,rlpt)  * ( qci3D(i,j,k) + qpi3D(i,j,k) ) ) &
							* ( 1.0_rlpt + REAL(epsv,rlpt) * qv3D(i,j,k) - qcl3D(i,j,k) &
							- qci3D(i,j,k) - qpl3D(i,j,k) - qpi3D(i,j,k) ) &
							* (( 1E3_rlpt / pres1D(k) ) ** ( REAL(rgas,rlpt) / REAL(cp,rlpt) ))
						
						dtvdz = ( thetav(i,j,2) - thetav(i,j,1) ) * rdz
						
						tv_at_zi = 0.5_rlpt * ( thetav(i,j,2) + thetav(i,j,1) )
						
						rig3D(i,j,k) = REAL(ggr,rlpt)*dtvdz / ( tv_at_zi*(dudz2+dvdz2+1.0E-10_rlpt) )
					ENDDO
				ENDDO
				thetav(:,:,1) = thetav(:,:,2)
			ENDDO
		ENDIF
		
		! Set the lowest level value same as the second level
		rig3D(:,:,1) = rig3D(:,:,2)
		
		! 3. Boundary exchange
		CALL task_exchange_lpt( rig3D, x1s, x2s, y1s, y2s, nzlpt, sx1, sx2, sy1, sy2, &
		                        field_id_newout1 )
		
	END SUBROUTINE gradient_richardson_number
	
	!------------------------------------------------------------------------------------------
	
	SUBROUTINE collect_output
	
	! With the logical flag diag_micro_sam1mom = .TRUE., thermodynamic variable arediagnosed based
	! on MICRO_SAM1MOM and statistics.f90
	! - possible negative value around the cloud edge by interpolation is avoided.
	! - output holds the grid point value if parcel is on scalar definition point.
	! - conversion between variables will work without error.
	! Radiation and tendency variables are interpolated.
	
		IMPLICIT NONE
		
		INTEGER, DIMENSION(2) :: i0, j0, k0, km2, kp3
		REAL(KIND=rlpt), DIMENSION(2) :: x, y, z
		REAL(KIND=rlpt), DIMENSION(-2:3,-2:3,-2:3) :: f216
		INTEGER :: i, j, k, m, n, nv4
		
		REAL(KIND=rlpt) :: presD, gamazD, tlD, qtD, qpD, tabsD, qnD, qvD, qclD, qciD, qplD, qpiD, &
		                   thetaD, qsatD
		
		! Initialize flag
		out_computed = .FALSE.
		
		! Loop through each parcel to collect output
		DO n = 1, np2
		
			! Find i0, j0, k0, x, y, z
			! Use parcel pointer-array for input position, since parcel2 may be a different type.
			CALL sw_def_points( parcel(n)%var(1), parcel(n)%var(2), parcel(n)%var(3), i0, j0, k0, &
			                    km2, kp3, x, y, z )
			
			! Store qt, qp in array
			DO k = k0(1)-km2(1), k0(1)+kp3(1)
				DO j = j0(1)-jm2, j0(1)+jp3
					DO i = i0(1)-2, i0(1)+3
						IF ( .NOT.out_computed(i,j,k) ) THEN
							qt3D(i,j,k) = qv3D(i,j,k) + qcl3D(i,j,k) + qci3D(i,j,k)
							qp3D(i,j,k) = qpl3D(i,j,k) + qpi3D(i,j,k)
							out_computed(i,j,k) = .TRUE.
						ENDIF
					ENDDO
				ENDDO
			ENDDO
			
			! Diagnose microphysical variable based on MICRO_SAM1MOM
			! - Pressure
			DO j = -jm2, jp3
				DO i = -2, 3
					f216( i, j, -km2(1):kp3(1) ) = pres1D( k0(1)-km2(1):k0(1)+kp3(1) )
				ENDDO
			ENDDO
			presD = interpolation( f216, x(1), y(1), z(1), k0(1), km2(1), kp3(1), 1, .TRUE. )
			
			! - gamaz = g / cp * z
			gamazD = ggr / cp * parcel(n)%var(3)
			
			! - tl, qt, qp
			f216( :, -jm2:jp3, -km2(1):kp3(1) ) &
				= t3D( i0(1)-2:i0(1)+3, j0(1)-jm2:j0(1)+jp3, k0(1)-km2(1):k0(1)+kp3(1) )
			tlD = interpolation( f216, x(1), y(1), z(1), k0(1), km2(1), kp3(1), 1, .TRUE. )
			f216( :, -jm2:jp3, -km2(1):kp3(1) ) &
				= qt3D( i0(1)-2:i0(1)+3, j0(1)-jm2:j0(1)+jp3, k0(1)-km2(1):k0(1)+kp3(1) )
			qtD = interpolation( f216, x(1), y(1), z(1), k0(1), km2(1), kp3(1), 1, .TRUE. )
			f216( :, -jm2:jp3, -km2(1):kp3(1) ) &
				= qp3D( i0(1)-2:i0(1)+3, j0(1)-jm2:j0(1)+jp3, k0(1)-km2(1):k0(1)+kp3(1) )
			qpD = interpolation( f216, x(1), y(1), z(1), k0(1), km2(1), kp3(1), 1, .TRUE. )
			
			! - tabs, qn, qv, qcl, qci, qpl, qpi
			IF ( diag_micro_sam1mom ) THEN
				! Diagnose with MICRO_SAM1MOM
				CALL micro_sam1mom( presD, gamazD, tlD, qtD, qpD, tabsD, qnD, qvD, qclD, qciD, qplD, &
				                    qpiD )
			ELSE
				! Interpolate qv, qcl, qci, qpl, qpi, then diagnose qn, tabs
				f216( :, -jm2:jp3, -km2(1):kp3(1) ) &
					= qv3D( i0(1)-2:i0(1)+3, j0(1)-jm2:j0(1)+jp3, k0(1)-km2(1):k0(1)+kp3(1) )
				qvD = interpolation( f216, x(1), y(1), z(1), k0(1), km2(1), kp3(1), 1, .TRUE. )
				f216( :, -jm2:jp3, -km2(1):kp3(1) ) &
					= qcl3D( i0(1)-2:i0(1)+3, j0(1)-jm2:j0(1)+jp3, k0(1)-km2(1):k0(1)+kp3(1) )
				qclD = interpolation( f216, x(1), y(1), z(1), k0(1), km2(1), kp3(1), 1, .TRUE. )
				f216( :, -jm2:jp3, -km2(1):kp3(1) ) &
					= qci3D( i0(1)-2:i0(1)+3, j0(1)-jm2:j0(1)+jp3, k0(1)-km2(1):k0(1)+kp3(1) )
				qciD = interpolation( f216, x(1), y(1), z(1), k0(1), km2(1), kp3(1), 1, .TRUE. )
				f216( :, -jm2:jp3, -km2(1):kp3(1) ) &
					= qpl3D( i0(1)-2:i0(1)+3, j0(1)-jm2:j0(1)+jp3, k0(1)-km2(1):k0(1)+kp3(1) )
				qplD = interpolation( f216, x(1), y(1), z(1), k0(1), km2(1), kp3(1), 1, .TRUE. )
				f216( :, -jm2:jp3, -km2(1):kp3(1) ) &
					= qpi3D( i0(1)-2:i0(1)+3, j0(1)-jm2:j0(1)+jp3, k0(1)-km2(1):k0(1)+kp3(1) )
				qpiD = interpolation( f216, x(1), y(1), z(1), k0(1), km2(1), kp3(1), 1, .TRUE. )
				qnD = qclD + qciD
				tabsD = tlD - gamazD + REAL(fac_cond,rlpt) * qnD
			ENDIF
			
			! - theta, qsat
			thetaD = tabsD * ( ( 1E3_rlpt / presD ) ** ( REAL(rgas,rlpt) / REAL(cp,rlpt) ) )
			qsatD = qsatw( REAL(tabsD,rsam), REAL(presD,rsam) )
			
			
			! Store, compute then store, or interpolate then store variables into parcel2.
			! Do not forget to add one to nv4.
			! The order of variables has to be macth with the list in set_output_list.
			nv4 = 0
			
			! - 10 pressure
			IF ( flag_lpt_pres ) THEN
				nv4 = nv4 + 1
				parcel2(n)%var(nv00+nv4) = presD
			ENDIF
			
			! - 11 density
			IF ( flag_lpt_rho ) THEN
				nv4 = nv4 + 1
				DO j = -jm2, jp3
					DO i = -2, 3
						f216( i, j, -km2(1):kp3(1) ) &
							= REAL( rho( k0(1)-km2(1)+kbottom-1:k0(1)+kp3(1)+kbottom-1 ), rlpt )
							! Above "rho" is a SAM variable.
					ENDDO
				ENDDO
				parcel2(n)%var(nv00+nv4) &
					= interpolation( f216, x(1), y(1), z(1), k0(1), km2(1), kp3(1), 1, .TRUE. )
			ENDIF
			
			! - 12. pressure perturbation
			IF ( flag_lpt_pprime ) THEN
				nv4 = nv4 + 1
				f216( :, -jm2:jp3, -km2(1):kp3(1) ) &
					= p3D( i0(1)-2:i0(1)+3, j0(1)-jm2:j0(1)+jp3, k0(1)-km2(1):k0(1)+kp3(1) )
				parcel2(n)%var(nv00+nv4) &
					= interpolation( f216, x(1), y(1), z(1), k0(1), km2(1), kp3(1), 1, .FALSE. )
			ENDIF
			
			! - 13. l.w.s.e.
			IF ( flag_lpt_lwse ) THEN
				nv4 = nv4 + 1
				parcel2(n)%var(nv00+nv4) = tlD
			ENDIF
			
			! - 14. moist static energy
			IF ( flag_lpt_mse ) THEN
				nv4 = nv4 + 1
				parcel2(n)%var(nv00+nv4) = tabsD + gamazD + REAL(fac_cond,rlpt) * qvD
			ENDIF
			
			! - 15. dry static energy
			IF ( flag_lpt_dse ) THEN
				nv4 = nv4 + 1
				parcel2(n)%var(nv00+nv4) = tabsD + gamazD
			ENDIF
			
			! - 16. virtual dse
			IF ( flag_lpt_vdse ) THEN
				nv4 = nv4 + 1
				parcel2(n)%var(nv00+nv4) = tabsD * (1.0_rlpt + REAL(epsv,rlpt)*qvD-qnD-qpD) + gamazD
			ENDIF
			
			! - 17. saturation moist static energy
			IF ( flag_lpt_smse ) THEN
				nv4 = nv4 + 1
				parcel2(n)%var(nv00+nv4) = tabsD + gamazD + REAL(fac_cond,rlpt) * qsatD
			ENDIF
			
			! - 18. absolute temperature
			IF ( flag_lpt_tabs ) THEN
				nv4 = nv4 + 1
				parcel2(n)%var(nv00+nv4) = tabsD
			ENDIF
			
			! - 19. potential T
			IF ( flag_lpt_theta ) THEN
				nv4 = nv4 + 1
				parcel2(n)%var(nv00+nv4) = thetaD
			ENDIF
			
			! - 20. liquid water potential T
			IF ( flag_lpt_thetal ) THEN
				nv4 = nv4 + 1
				parcel2(n)%var(nv00+nv4) = thetaD * ( 1.0_rlpt - REAL(fac_cond,rlpt) * qnD / tabsD )
			ENDIF
			
			! - 21. virtual potential T
			IF ( flag_lpt_thetav ) THEN
				nv4 = nv4 + 1
				parcel2(n)%var(nv00+nv4) = thetaD * ( 1.0_rlpt + REAL(epsv,rlpt) * qvD - qnD - qpD )
			ENDIF
			
			! - 22. equivalent potential T
			IF ( flag_lpt_thetae ) THEN
				nv4 = nv4 + 1
				parcel2(n)%var(nv00+nv4) = thetaD * ( 1.0_rlpt + REAL(fac_cond,rlpt) * qvD / tabsD )
			ENDIF
			
			! - 23. total non-precipitating water mixing ratio, qt
			IF ( flag_lpt_qt ) THEN
				nv4 = nv4 + 1
				parcel2(n)%var(nv00+nv4) = qtD * 1E3_rlpt
			ENDIF
			
			! - 24. water vapor
			IF ( flag_lpt_qv ) THEN
				nv4 = nv4 + 1
				parcel2(n)%var(nv00+nv4) = qvD * 1E3_rlpt
			ENDIF
			
			! - 25. cloud water
			IF ( flag_lpt_qcl ) THEN
				nv4 = nv4 + 1
				parcel2(n)%var(nv00+nv4) = qclD * 1E3_rlpt
			ENDIF
			
			! - 26. cloud ice
			IF ( flag_lpt_qci ) THEN
				nv4 = nv4 + 1
				parcel2(n)%var(nv00+nv4) = qciD * 1E3_rlpt
			ENDIF
			
			! - 27. cloud water + ice, qn
			IF ( flag_lpt_qn ) THEN
				nv4 = nv4 + 1
				parcel2(n)%var(nv00+nv4) = qnD * 1E3_rlpt
			ENDIF
			
			! - 28. rain water
			IF ( flag_lpt_qpl ) THEN
				nv4 = nv4 + 1
				parcel2(n)%var(nv00+nv4) = qplD * 1E3_rlpt
			ENDIF
			
			! - 29. snow + graupel
			IF ( flag_lpt_qpi ) THEN
				nv4 = nv4 + 1
				parcel2(n)%var(nv00+nv4) = qpiD * 1E3_rlpt
			ENDIF
			
			! - 30. rain + snow + graupel
			IF ( flag_lpt_qp ) THEN
				nv4 = nv4 + 1
				parcel2(n)%var(nv00+nv4) = qpD * 1E3_rlpt
			ENDIF
			
			! - 31. saturation mixing ratio
			IF ( flag_lpt_qsat ) THEN
				nv4 = nv4 + 1
				parcel2(n)%var(nv00+nv4) = qsatD * 1E3_rlpt
			ENDIF
			
			! - 32 relative humidity
			IF ( flag_lpt_relh ) THEN
				nv4 = nv4 + 1
				parcel2(n)%var(nv00+nv4) = qvD / qsatD
			ENDIF
			
			! - 33. Parameterized ensemble-mean resolved scale TKE
			IF ( flag_lpt_tke ) THEN
				nv4 = nv4 + 1
				f216( : ,-jm2:jp3, -km2(1):kp3(1) ) &
					= tke3D( i0(1)-2:i0(1)+3, j0(1)-jm2:j0(1)+jp3, k0(1)-km2(1):k0(1)+kp3(1) )
				parcel2(n)%var(nv00+nv4) &
					= interpolation( f216, x(1), y(1), z(1), k0(1), km2(1), kp3(1), 1, .TRUE. )
			ENDIF
			
			! - 34. SGS TKE
			IF ( flag_lpt_tkes ) THEN
				nv4 = nv4 + 1
				f216( :, -jm2:jp3, -km2(1):kp3(1) ) &
					= tkes3D( i0(1)-2:i0(1)+3, j0(1)-jm2:j0(1)+jp3, k0(1)-km2(1):k0(1)+kp3(1), t2lpt )
				parcel2(n)%var(nv00+nv4) &
					= interpolation( f216, x(1), y(1), z(1), k0(1), km2(1), kp3(1), 1, .TRUE. )
			ENDIF
			
			! - 35. Gradient Richardson number, w point
			IF ( flag_lpt_gradri ) THEN
				nv4 = nv4 + 1
				f216( :, -jm2:jp3, -km2(2):kp3(2) ) &
					= rig3D( i0(2)-2:i0(2)+3, j0(2)-jm2:j0(2)+jp3, k0(2)-km2(2):k0(2)+kp3(2) )
				parcel2(n)%var(nv00+nv4) &
					= interpolation( f216, x(2), y(2), z(2), k0(2), km2(2), kp3(2), 0, .FALSE. )
			ENDIF
			
			! - 36. micro_field
			IF ( nmicroout > 0 ) THEN
				DO m = 1, nmicro_fields
					IF ( flag_lpt_micro(m) == 1 ) THEN
						nv4 = nv4 + 1
						f216( :, -jm2:jp3, -km2(1):kp3(1) ) &
							= micro3D( i0(1)-2:i0(1)+3, j0(1)-jm2:j0(1)+jp3, k0(1)-km2(1):k0(1)+kp3(1), m )
						parcel2(n)%var(nv00+nv4) &
							= interpolation( f216, x(1), y(1), z(1), k0(1), km2(1), kp3(1), 1, .TRUE. ) &
							* mkoutputscale(m)
					ENDIF
				ENDDO
			ENDIF
			
			! - 37. Tracer
			IF ( flag_lpt_tracers ) THEN
				DO m = 1, ntracers
					nv4 = nv4 + 1
					f216( :, -jm2:jp3, -km2(1):kp3(1) ) &
						= tracer3D( i0(1)-2:i0(1)+3, j0(1)-jm2:j0(1)+jp3, k0(1)-km2(1):k0(1)+kp3(1), m )
					parcel2(n)%var(nv00+nv4) &
						= interpolation( f216, x(1), y(1), z(1), k0(1), km2(1), kp3(1), 1, .TRUE. )
				ENDDO
			ENDIF
			
		ENDDO   ! n, np2
		
	END SUBROUTINE collect_output
	
	!------------------------------------------------------------------------------------------
	
	SUBROUTINE micro_sam1mom( presD, gamazD, tlD, qtD, qpD, tabsD, qnD, qvD, qclD, qciD, qplD, qpiD )
	
		IMPLICIT NONE
		
		REAL(KIND=rlpt), INTENT(IN) :: presD, gamazD, tlD, qtD
		REAL(KIND=rlpt), INTENT(INOUT) :: qpD
		REAL(KIND=rlpt), INTENT(OUT) :: tabsD, qnD, qvD, qclD, qciD, qplD, qpiD
		
		! Local
		REAL(KIND=rlpt), PARAMETER :: tbgmin = 253.16    ! Minimum temperature for cloud water., K
		REAL(KIND=rlpt), PARAMETER :: tbgmax = 273.16    ! Maximum temperature for cloud ice, K
		REAL(KIND=rlpt), PARAMETER :: tprmin = 268.16    ! Minimum temperature for rain, K
		REAL(KIND=rlpt), PARAMETER :: tprmax = 283.16    ! Maximum temperature for snow+graupel, K
		REAL(KIND=rlpt), PARAMETER :: tgrmin = 223.16    ! Minimum temperature for snow, K
		REAL(KIND=rlpt), PARAMETER :: tgrmax = 283.16    ! Maximum temperature for graupel, K
		REAL(KIND=rlpt), PARAMETER :: an = 1.0_rlpt / ( tbgmax - tbgmin ), &
		                              bn = tbgmin * an, &
		                              ap = 1.0_rlpt / ( tprmax - tprmin ), &
		                              bp = tprmin * ap, &
		                              fac1 = REAL(fac_cond,rlpt)+(1.0_rlpt+bp)*REAL(fac_fus,rlpt), &
		                              fac2 = REAL(fac_fus,rlpt) * ap, &
		                              ag = 1.0_rlpt / ( tgrmax - tgrmin )
		REAL(KIND=rlpt) :: dtabs, tabs1, om, omp, fff, dfff, qsatt, dqsat, lstarn, dlstarn, lstarp, &
		                   dlstarp
		INTEGER :: niter
		
		! Same procedure in MICRO_SAM1MOM/cloud.f90
		! - Initial guess temperature
		tabsD = tlD - gamazD
		tabs1 = ( tabsD + fac1 * qpD ) / ( 1.0_rlpt + fac2 * qpD )
		
		! - Adjust temperature and then, compute saturation mixing ratio
		IF (tabs1 >= tbgmax) THEN
			! warm cloud
			tabs1 = tabsD + REAL(fac_cond,rlpt) * qpD
			qsatt = qsatw( REAL(tabs1,rsam), REAL(presD,rsam) )
		ELSE IF (tabs1 <= tbgmin) THEN
			! ice cloud
			tabs1 = tabsD + REAL(fac_sub,rlpt) * qpD
			qsatt = qsati( REAL(tabs1,rsam), REAL(presD,rsam) )
		ELSE
			! mixed-phase cloud
			om = an * tabs1 - bn
			qsatt = om * REAL( qsatw( REAL(tabs1,rsam), REAL(presD,rsam) ), rlpt ) &
			      + ( 1.0_rlpt - om ) * REAL( qsati( REAL(tabs1,rsam), REAL(presD,rsam) ), rlpt )
		ENDIF
		
		! - Test if condensation is possible
		IF ( qtD > qsatt ) THEN
			niter = 0
			dtabs = 100.0_rlpt
			DO WHILE ( ABS(dtabs) > 0.01_rlpt .AND. niter < 10 )
				IF ( tabs1 >= tbgmax ) THEN
					om = 1.0_rlpt
					lstarn = REAL(fac_cond,rlpt)
					dlstarn = 0.0_rlpt
					qsatt = qsatw( REAL(tabs1,rsam), REAL(presD,rsam) )
					dqsat = dtqsatw( REAL(tabs1,rsam), REAL(presD,rsam) )
				ELSE IF ( tabs1 <= tbgmin ) then
					om = 0.0_rlpt
					lstarn = REAL(fac_sub,rlpt)
					dlstarn = 0.0_rlpt
					qsatt = qsati( REAL(tabs1,rsam), REAL(presD,rsam) )
					dqsat = dtqsati( REAL(tabs1,rsam), REAL(presD,rsam) )
				ELSE
					om = an * tabs1 - bn
					lstarn = REAL(fac_cond,rlpt) + (1.0_rlpt-om) * REAL(fac_fus,rlpt)
					dlstarn = an * REAL(fac_fus,rlpt)
					qsatt = om * REAL( qsatw(REAL(tabs1,rsam),REAL(presD,rsam)),rlpt ) &
					     + (1.0_rlpt-om) * REAL( qsati(REAL(tabs1,rsam),REAL(presD,rsam)),rlpt )
					dqsat = om * REAL( dtqsatw(REAL(tabs1,rsam),REAL(presD,rsam)),rlpt ) &
					   + (1.0_rlpt-om) * REAL( dtqsati(REAL(tabs1,rsam),REAL(presD,rsam)),rlpt )
				ENDIF
				IF ( tabs1 >= tprmax ) THEN
					omp = 1.0_rlpt
					lstarp = REAL(fac_cond,rlpt)
					dlstarp = 0.0_rlpt
				ELSE IF ( tabs1 <= tprmin ) THEN
					omp = 0.0_rlpt
					lstarp = REAL(fac_sub,rlpt)
					dlstarp = 0.0_rlpt
				ELSE
					omp = ap * tabs1 - bp
					lstarp = REAL(fac_cond,rlpt) + (1.0_rlpt-omp) * REAL(fac_fus,rlpt)
					dlstarp = ap * REAL(fac_fus,rlpt)
				ENDIF
				fff = tabsD - tabs1 + lstarn * ( qtD - qsatt ) + lstarp * qpD
				dfff = dlstarn * ( qtD - qsatt ) + dlstarp * qpD - lstarn * dqsat - 1.0_rlpt
				dtabs = - fff / dfff
				niter = niter + 1
				tabs1 = tabs1 + dtabs
			ENDDO
			qsatt = qsatt + dqsat * dtabs
			qnD = MAX( 0.0_rlpt, qtD-qsatt )   ! qcl+qc
		ELSE
			qnD = 0.0_rlpt
		ENDIF
		tabsD = tabs1
		qpD = max( 0.0_rlpt, qpD )
		
		! Same procedure in MICRO_SAM1MOM/microphysics.f90/micro_diagnse
		qvD = qtD - qnD
		om = MAX( 0.0_rlpt, MIN(1.0_rlpt, (tabsD-REAL(tbgmin,rlpt))*an) )
		qclD = qnD * om
		qciD = qnD * ( 1.0_rlpt - om )
		omp = MAX( 0.0_rlpt, MIN(1.0_rlpt, (tabsD-REAL(tprmin,rlpt))*ap) )
		qplD = qpD * omp
		qpiD = qpD * ( 1.0_rlpt - omp )
		
	END SUBROUTINE micro_sam1mom
	
	!------------------------------------------------------------------------------------------
	
	SUBROUTINE lpt_write
	
		IMPLICIT NONE
		INCLUDE 'mpif.h'
		TYPE(parcel_array_output), ALLOCATABLE, DIMENSION(:) :: parcel3
		INTEGER, DIMENSION(nsubdomains) :: nps, sdispls, rdispls, snp, rnp
		INTEGER, DIMENSION(nsubdomains,nsubdomains) :: snp2D
		INTEGER, DIMENSION(np) :: tag_out
		INTEGER, DIMENSION(np) :: nvi_out
		REAL(KIND=4), DIMENSION(np) :: var_out
		REAL(KIND=4) :: day4
		INTEGER :: i, j, i1, i2, i3, ierr, n, irank
		
		CALL t_startf ('lpt_write')
		CALL t_startf ('lpt_sort')
		
		! 1. Sort by tag order
		IF ( nsubdomains > 1 ) THEN
			! Procedure from 1 to 4 is to avoid possible memory problem for merge sort when parcels
			! are concentrated in certain rank(s).
			
			! 1. Tell size of np2 to all processors from all processors by MPI_ALLGATHER
			CALL MPI_ALLGATHER( np2, 1, MPI_INTEGER, nps, 1, MPI_INTEGER, MPI_COMM_WORLD, ierr )
			
			! 2. Count number of passing parcels if nps > np, also count receiving parcels if nps < np
			snp2D(:,:) = 0 ! # of send pacels to each rank from all ranks
			DO i = 1, nsubdomains
				snp2D(i,i) = MIN( nps(i), np )
			ENDDO
			nps(:) = nps(:) - np
			DO j = 1, nsubdomains
				IF ( nps(j) > 0 ) THEN   ! more than np parcels
					DO i = 1, nsubdomains
						IF ( nps(i) < 0 ) THEN   ! need more parcels
							IF ( nps(i) + nps(j) <= 0 ) THEN   ! pass all excess parcels
								snp2D(i,j) = snp2D(i,j) + nps(j)
								nps(i) = nps(i) + nps(j)
								nps(j) = 0
							ELSE   ! pass just enough parcels so that rank i-1 is now np parcels
								snp2D(i,j) = snp2D(i,j) - nps(i)
								nps(j) = nps(j) + nps(i)
								nps(i) = 0
							ENDIF
						ENDIF
						IF ( nps(j) == 0 ) EXIT
					ENDDO
				ENDIF
			ENDDO
			snp = 0   ! # of send pacels to each rank from current rank
			rnp = 0   ! # of receive parcels from each rank
			DO i = 1, nsubdomains
				snp(i) = snp2D(i,rank+1)
				rnp(i) = snp2D(rank+1,i)
			ENDDO
			
			! 3. Obtain send and receive displacements
			sdispls(:) = 0
			rdispls(:) = 0
			DO i = 2, nsubdomains
				sdispls(i) = sdispls(i-1) + snp(i-1)
				rdispls(i) = rdispls(i-1) + rnp(i-1)
			ENDDO
			
			! 4. Send parcels to each designated rank
			ALLOCATE( parcel3(np), STAT=ierr )
			IF ( ierr /= 0 ) THEN
				PRINT*,'LPT ERROR: Failed to allocate arrays in lpt_write on proc:', rank
				CALL lpt_task_abort
			ENDIF
			CALL MPI_ALLTOALLV( parcel2, snp, sdispls, newtype2, parcel3, rnp, rdispls, newtype2, &
			                    MPI_COMM_WORLD, ierr )
			
			! 5. Sort by tag order
			CALL lpt_sort( np, parcel3 )
			
			! 6. Count number of outgoing parcels based on tag.
			!    Parcels with tag between rank*np+1 and (rank+1)*np are passed to rank
			!    rank starts 0. rank=0 corresponds to subdomain 1.
			!    rank*np+1 = (subdomain-1)*np+1
			!    (rank+1)*np = subdomain*np
			snp(:) = 0   ! # of send parcels to each rank
			DO i = 1, nsubdomains
				DO n = 1, np
					IF ((parcel3(n)%tag >= (i-1)*np+1).AND.(parcel3(n)%tag <= i*np)) snp(i) = snp(i) + 1
				ENDDO
			ENDDO
			
			! 7. Obtain number of incoming parcels from all ranks
			rnp(:) = 0   ! # of receive parcels from each rank
			CALL MPI_ALLTOALL( snp, 1, MPI_INTEGER, rnp, 1, MPI_INTEGER, MPI_COMM_WORLD, ierr )
			
			! 8. Obtain send and receive displacements
			sdispls(:) = 0
			rdispls(:) = 0
			DO i = 2, nsubdomains
				sdispls(i) = sdispls(i-1) + snp(i-1)
				rdispls(i) = rdispls(i-1) + rnp(i-1)
			ENDDO
			
			! 9. Send parcels to each designated rank
			DEALLOCATE( parcel2, STAT=ierr )
			IF ( ierr /= 0 ) THEN
				PRINT*,'LPT ERROR: Failed to deallocate arrays in lpt_write on proc:', rank
				CALL lpt_task_abort
			ENDIF
			ALLOCATE( parcel2(np), STAT=ierr )
			IF ( ierr /= 0 ) THEN
				PRINT*,'LPT ERROR: Failed to allocate arrays in lpt_write on proc:', rank
				CALL lpt_task_abort
			ENDIF
			CALL MPI_ALLTOALLV( parcel3, snp, sdispls, newtype2, parcel2, rnp, rdispls, newtype2, &
			                    MPI_COMM_WORLD, ierr )
			
			! 10. Deallocate parcel3
			DEALLOCATE( parcel3, STAT=ierr )
			IF ( ierr /= 0 ) THEN
				PRINT*,'LPT ERROR: Failed to deallocate arrays in lpt_write on proc:', rank
				CALL lpt_task_abort
			ENDIF
		ENDIF
		
		! 11. Sort by tag order
		CALL lpt_sort( np, parcel2 )
		
		CALL t_stopf ('lpt_sort')
		
		
		! 2. Save data
		! Create output filename, check output file status, then create file if necessary
		CALL lpt_write_init
		
		! Write data
		DO irank = 0, nsubdomains-1
			CALL task_barrier()
			IF ( irank == rank ) THEN
				OPEN(151, FILE=filename_lpt, STATUS='old', FORM='unformatted', POSITION='append')
				
				! append data
				day4 = REAL( day - dt / 86400.0_rsam, 4 ) !-dt/86400: synchronize to SAM
				IF (masterproc) WRITE(151) history_length, day4
				tag_out = parcel2%tag
				WRITE(151) tag_out
				nvi_out = parcel2%nvi
				WRITE(151) nvi_out
				DO n = 1, nv
					var_out = parcel2%var(n)
					WRITE(151) var_out
				ENDDO
				CLOSE(151)
			ENDIF
		ENDDO
		
		! 3. Show info
		IF (masterproc) THEN
			PRINT*,'=========================================================='
			PRINT*,'LPT history length:', history_length
			IF ( flag_fixed_uv ) PRINT('(" Fixed U & V:",2F15.7)'), fixed_u, fixed_v
			i1 = 1
			i2 = MAX( np/2, 1 )
			i3 = np
			PRINT('(" Parcel:",I6,2I15 )'), parcel2(i1)%tag, parcel2(i2)%tag,    parcel2(i3)%tag
			PRINT('(" 1. X :",3F15.7)'), parcel2(i1)%var(1), parcel2(i2)%var(1), parcel2(i3)%var(1)
			PRINT('(" 2. Y :",3F15.7)'), parcel2(i1)%var(2), parcel2(i2)%var(2), parcel2(i3)%var(2)
			PRINT('(" 3. Z :",3F15.7)'), parcel2(i1)%var(3), parcel2(i2)%var(3), parcel2(i3)%var(3)
			PRINT('(" 4. UR:",3F15.7)'), parcel2(i1)%var(4), parcel2(i2)%var(4), parcel2(i3)%var(4)
			PRINT('(" 5. VR:",3F15.7)'), parcel2(i1)%var(5), parcel2(i2)%var(5), parcel2(i3)%var(5)
			PRINT('(" 6. WR:",3F15.7)'), parcel2(i1)%var(6), parcel2(i2)%var(6), parcel2(i3)%var(6)
			IF ( flag_lpt_sgs ) THEN
			PRINT('(" 7. US:",3F15.7)'), parcel2(i1)%var(i7), parcel2(i2)%var(i7), parcel2(i3)%var(i7)
			PRINT('(" 8. VS:",3F15.7)'), parcel2(i1)%var(i8), parcel2(i2)%var(i8), parcel2(i3)%var(i8)
			PRINT('(" 9. WS:",3F15.7)'), parcel2(i1)%var(i9), parcel2(i2)%var(i9), parcel2(i3)%var(i9)
			PRINT('("10. NV:",I7,2I15)'), parcel2(i1)%nvi,   parcel2(i2)%nvi,    parcel2(i3)%nvi
			ELSE
			PRINT('(" 7. NV:",I7,2I15)'), parcel2(i1)%nvi,   parcel2(i2)%nvi,    parcel2(i3)%nvi
			ENDIF
print*,"!"
print*,var_lpt_shrt
print*,"!"
			DO i = nv00+1, nv
print*,var_lpt_shrt(i)
				PRINT('(A10,3F25.15)'), var_lpt_shrt(i), parcel2(i1)%var(i), parcel2(i2)%var(i), &
				                        parcel2(i3)%var(i)
			ENDDO
print*,"!"
			PRINT*,'=========================================================='
			PRINT*,''
		ENDIF
		
		! 4. Deallocate parcel2
		DEALLOCATE( parcel2, STAT=ierr )
		IF ( ierr /= 0 ) THEN
			PRINT*,'LPT ERROR: Failed to deallocate arrays in lpt_write on proc:', rank
			CALL lpt_task_abort
		ENDIF
		
		CALL t_stopf ('lpt_write')
		
	END SUBROUTINE lpt_write
	
	!------------------------------------------------------------------------------------------
	
	SUBROUTINE lpt_write_init
	
		IMPLICIT NONE
		
		CHARACTER (LEN=5) :: history_char
		LOGICAL :: file_exist
		
		! 1. Add one to history length
		history_length = history_length + 1
		
		! 2. Output file extention
		IF ( nhistory < 0 ) THEN
			filename_lpt_ext = 'LPT.bin'
		ELSE
			IF ( MOD(history_length-1,nhistory) == 0 ) THEN
				WRITE(history_char,'(I5.5)') (history_length-1) / nhistory + 1
				filename_lpt_ext = 'LPT'//TRIM(history_char)//'.bin'
			ENDIF
		ENDIF
		
		! 3. Output filename
		filename_lpt = './OUT_LPT/'//TRIM(case)//'_'//TRIM(caseid)//'_'//TRIM(filename_lpt_ext)
		
		! 4. Check output file status
		IF (masterproc) THEN
			! 1. Chekc if the file has already existed
			INQUIRE(FILE=filename_lpt, EXIST=file_exist)
			
			! 2. If the file does not exist then create
			IF (.NOT.file_exist) THEN
				PRINT*,'LPT: New output file is created: '//TRIM(filename_lpt)
				
				! Save header information
				OPEN(151, FILE=filename_lpt, STATUS='new', FORM='unformatted')
				WRITE(151) nsubdomains, np1, nv0+nv1, nv01, nv, nhistory, &
				           REAL(xwidth,4), REAL(ywidth,4), REAL(ztop,4), REAL(zbottom,4)
				WRITE(151) var_lpt_name, var_lpt_shrt, var_lpt_unit
				CLOSE(151)
			ELSE IF (history_length > 1.AND.file_exist) THEN
				PRINT*,'LPT: Output is appended to '//TRIM(filename_lpt)
			ELSE IF (history_length == 1.AND.file_exist) THEN
				! Avoid appending initial condition to exsiting file
				PRINT*,'LPT: A same data file has already existed, Abort.'
				PRINT*, TRIM(filename_lpt)
				CALL lpt_task_abort
			ENDIF
		ENDIF
		
	END SUBROUTINE lpt_write_init
	
	!======================================== SORTING ============================================
	
	SUBROUTINE lpt_sort( n, input )
	
		INTEGER, INTENT(IN) :: n
		TYPE(parcel_array_output), DIMENSION(n), INTENT(INOUT) :: input
		TYPE(parcel_array_output), DIMENSION(n) :: temp
		TYPE(parcel_array_sort), DIMENSION(n) :: tags
		INTEGER :: i, j
		
		! Store input for later use
		temp = input
		
		! Store tag in a temporal array
		tags(:)%tag = input(:)%tag
		DO i = 1, n
			tags(i)%index = i
		ENDDO
		
		! Margesort
		CALL mergesort( tags, n, ceiling(n/2.), n-ceiling(n/2.) )
		
		! Sort input with the sorted tags
		DO i = 1, n
			j = tags(i)%index
			input(i) = temp(j)
		ENDDO
		
	END SUBROUTINE lpt_sort
	
	!------------------------------------------------------------------------------------------
	
	RECURSIVE SUBROUTINE mergesort( input, n, nl, nr )
	
		IMPLICIT NONE
		
		INTEGER, INTENT(IN) :: n, nl, nr
		TYPE(parcel_array_sort), DIMENSION(n), INTENT(INOUT) :: input
		TYPE(parcel_array_sort), DIMENSION(nl) :: left
		TYPE(parcel_array_sort), DIMENSION(nr) :: right
		INTEGER :: i
		
		IF ( n > 1 ) THEN
			DO i = 1, nl
				left(i) = input(i)
			ENDDO
			DO i = 1, nr
				right(i) = input(i+nl)
			ENDDO
			CALL mergesort( left, nl, CEILING(nl/2.), nl-CEILING(nl/2.) )
			CALL mergesort( right, nr, CEILING(nr/2.), nr-CEILING(nr/2.) )
			CALL merge( input, left, right, n, nl, nr )
		ENDIF
		
	END SUBROUTINE mergesort
	
	!------------------------------------------------------------------------------------------
	
	SUBROUTINE merge( input, left, right, n, nl, nr )
	
		IMPLICIT NONE
		
		INTEGER, INTENT(IN) :: n, nl, nr
		TYPE(parcel_array_sort), DIMENSION(n), INTENT(INOUT) :: input
		TYPE(parcel_array_sort), DIMENSION(nl), INTENT(IN) :: left
		TYPE(parcel_array_sort), DIMENSION(nr), INTENT(IN) :: right
		INTEGER :: nl2, nr2, i, j, il, ir
		
		nl2 = nl
		nr2 = nr
		i = 0
		il = 1
		ir = 1
		DO WHILE ( ( nl2 > 0 ) .AND. ( nr2 > 0 ) )
			IF ( left(il)%tag < right(ir)%tag ) THEN
				i = i + 1
				input(i) = left(il)
				nl2 = nl2 - 1
				il = il + 1
			ELSE
				i = i + 1
				input(i) = right(ir)
				nr2 = nr2 - 1
				ir = ir + 1
			ENDIF
		ENDDO
		
		IF ( nl2 > 0 ) THEN
			il = il - 1
			DO j = 1, nl2
				input(i+j) = left(il+j)
			ENDDO
		ENDIF
		
		IF ( nr2 > 0 ) THEN
			ir = ir - 1
			DO j = 1, nr2
				input(i+j) = right(ir+j)
			ENDDO
		ENDIF
		
	END SUBROUTINE merge
	
	!====================================== RESTART I/O =======================================
	
	SUBROUTINE lpt_restart_read
	
		IMPLICIT NONE
		
		TYPE(parcel_array), ALLOCATABLE, DIMENSION(:) :: parcel_temp
		CHARACTER (LEN=4) :: rankchar
		INTEGER :: irank, ii, n, ierr
		INTEGER :: lenstr
		EXTERNAL :: lenstr
		
		IF(masterproc) PRINT*,'LPT: Reading restart file...'
		
		! Read restart data
		! Allocate parcel_temp (array) and parcel (pointer-array)
		IF (restart_sep) THEN
			WRITE(rankchar,'(i4)') rank
			OPEN(64, FILE='./RESTART/'//case(1:LEN_TRIM(case))//'_'//caseid(1:LEN_TRIM(caseid))// &
				'_'//rankchar(5-lenstr(rankchar):4)//'_restart_LPT.bin', STATUS='unknown',&
				FORM='unformatted')
			READ(64) np2, history_length, filename_lpt, flag_lpt_output, t1lpt, t2lpt
			ALLOCATE( parcel_temp(0:np2), parcel(0:np2), STAT=ierr )
			IF ( ierr /= 0 ) THEN
				PRINT*,'LPT ERROR: Failed to allocate arrays in lpt_restart_read on proc:', rank
				CALL lpt_task_abort
			ENDIF
			READ(64) parcel_temp
			IF (flag_lpt_sgs) READ(64) tkes3D, tke3D
			CLOSE(64)
		ELSE
			WRITE(rankchar,'(i4)') nsubdomains
			OPEN(64, FILE='./RESTART/'//case(1:LEN_TRIM(case))//'_'//caseid(1:LEN_TRIM(caseid))// &
				'_'//rankchar(5-lenstr(rankchar):4)//'_restart_LPT.bin', STATUS='unknown', &
				FORM='unformatted')
			DO irank = 0, nsubdomains-1
				CALL task_barrier()
				IF (irank == rank) THEN
					DO ii = 0, irank-1   ! skip records
						READ(64)
						READ(64)
					ENDDO
					READ(64) np2, history_length, filename_lpt, flag_lpt_output, t1lpt, t2lpt
					ALLOCATE( parcel_temp(0:np2), parcel(0:np2), STAT=ierr )
					IF ( ierr /= 0 ) THEN
						PRINT*,'LPT ERROR: Failed to allocate arrays in lpt_restart_read on proc:', rank
						CALL lpt_task_abort
					ENDIF
					READ(64) parcel_temp
					IF (flag_lpt_sgs) READ(64) tkes3D, tke3D
					CLOSE(64)
				ENDIF
			ENDDO
		ENDIF
		
		! Store data from parcel_temp (array) to parcel (pointer-array)
		! Deallocate parcel_temp
		DO n = 1, np2
			parcel(n)%tag = parcel_temp(n)%tag
			parcel(n)%nvi = parcel_temp(n)%nvi
			parcel(n)%var(:) = parcel_temp(n)%var(:)
		ENDDO
		DEALLOCATE( parcel_temp, STAT=ierr )
		IF ( ierr /= 0 ) THEN
			PRINT*,'LPT ERROR: Failed to deallocate arrays in lpt_restart_read on proc:', rank
			CALL lpt_task_abort
		ENDIF
		
		IF (rank == nsubdomains-1) THEN
			PRINT*,'Case:', caseid
			PRINT*,'Restart LPT at step:', nstep
			PRINT*,'Time:', nstep*dt
		ENDIF
		
		CALL task_barrier()
		
	END SUBROUTINE lpt_restart_read
	
	!------------------------------------------------------------------------------------------
	
	SUBROUTINE lpt_restart_write
	
		IMPLICIT NONE
		
		TYPE(parcel_array), ALLOCATABLE, DIMENSION(:) :: parcel_temp
		CHARACTER (LEN=4) :: rankchar
		INTEGER :: irank, n, ierr
		INTEGER :: lenstr
		EXTERNAL :: lenstr
		
		CALL t_startf ('lpt_restart_write')
		
		IF (masterproc) PRINT*,'LPT: Writting restart file...'
		
		! Store data from parcel (pointer-array) to parcel_temp
		ALLOCATE( parcel_temp(0:np2), STAT=ierr )
		IF ( ierr /= 0 ) THEN
			PRINT*,'LPT ERROR: Failed to allocate arrays in lpt_restart_write on proc:', rank
			CALL lpt_task_abort
		ENDIF
		DO n = 1, np2
			parcel_temp(n)%tag = parcel(n)%tag
			parcel_temp(n)%nvi = parcel(n)%nvi
			parcel_temp(n)%var(:) = parcel(n)%var(:)
		ENDDO
		
		! Write restart data
		IF (restart_sep) THEN
			WRITE(rankchar,'(i4)') rank
			OPEN(64, FILE='./RESTART/'//case(1:LEN_TRIM(case))//'_'//caseid(1:LEN_TRIM(caseid))// &
				'_'//rankchar(5-lenstr(rankchar):4)//'_restart_LPT.bin', STATUS='unknown', &
				FORM='unformatted')
			WRITE(64) np2, history_length, filename_lpt, flag_lpt_output, t1lpt, t2lpt
			WRITE(64) parcel_temp
			IF (flag_lpt_sgs) WRITE(64) tkes3D, tke3D
			CLOSE(64)
		ELSE
			WRITE(rankchar,'(i4)') nsubdomains
			DO irank = 0, nsubdomains-1
				CALL task_barrier()
				IF (irank == rank) THEN
					IF (masterproc) THEN
						OPEN(64, FILE='./RESTART/'//case(1:LEN_TRIM(case))//'_'// &
							caseid(1:LEN_TRIM(caseid))//'_'//rankchar(5-lenstr(rankchar):4)// &
							'_restart_LPT.bin', STATUS='unknown', FORM='unformatted')
					ELSE
						OPEN(64, FILE='./RESTART/'//case(1:LEN_TRIM(case))//'_'// & 
							caseid(1:LEN_TRIM(caseid)) //'_'//rankchar(5-lenstr(rankchar):4)// &
							'_restart_LPT.bin', STATUS='unknown', FORM='unformatted', POSITION='append')
					ENDIF
					WRITE(64) np2, history_length, filename_lpt, flag_lpt_output, t1lpt, t2lpt
					WRITE(64) parcel_temp
					IF (flag_lpt_sgs) WRITE(64) tkes3D, tke3D
					CLOSE(64)
				ENDIF
			ENDDO
		ENDIF
		
		! Deallocate parcel_temp
		DEALLOCATE( parcel_temp, STAT=ierr )
		IF ( ierr /= 0 ) THEN
			PRINT*,'LPT ERROR: Failed to allocate arrays in lpt_restart_write on proc:', rank
			CALL lpt_task_abort
		ENDIF
		
		IF (masterproc) PRINT*,'LPT: Saved restart file. nstep=', nstep
		
		CALL task_barrier()
		
		CALL t_stopf ('lpt_restart_write')
		
	END SUBROUTINE lpt_restart_write
	
	!========================== SPECIAL MPI CODE FOR PARCEL EXCHANGE ==========================
	
	SUBROUTINE parcel_array2mpi_datatype
	
		IMPLICIT NONE
		INCLUDE 'mpif.h'
		INTEGER :: buff_type(2), block_length(2), displacements(2), ierr
		
		! parcel_array: (int, int, real, real, real,...)
		! buff_type:    (int, real)
		! block_length: (2 for int, nv0+nv1+nv2 for real)
		! displacement: (0 bite for int, 2*4 bytes for first real)
		!   => (int 4 byte|int 4 byte| real ...)
		buff_type(1) = MPI_INTEGER   ! 4 byte integer = MPI_INTEGER4
		SELECT CASE(rlpt)
		CASE(4)
			buff_type(2) = MPI_REAL4   ! single precision, 4 byte real
		CASE(8)
			buff_type(2) = MPI_REAL8   ! double precision, 8 byte real
		END SELECT
		block_length(1) = 2
		block_length(2) = nv0 + nv1 + nv2
		displacements(1) = 0
		displacements(2) = 2 * 4
		CALL MPI_TYPE_STRUCT(2, block_length, displacements, buff_type, newtype, ierr)
		CALL MPI_TYPE_COMMIT(newtype, ierr)
		
		! parcel_output_array: (int, int, real, real, real,...)
		! buff_type:    (int, real)
		! block_length: (2 for int, nv for real)
		! displacement: (0 bite for int, 2*4 bytes for first real)
		buff_type(2) = MPI_REAL4
		block_length(2) = nv
		CALL MPI_TYPE_STRUCT(2, block_length, displacements, buff_type, newtype2, ierr)
		CALL MPI_TYPE_COMMIT(newtype2, ierr)
		
	END SUBROUTINE parcel_array2mpi_datatype
	
	!------------------------------------------------------------------------------------------
	
	SUBROUTINE task_recv_parcel(buffer, length, tag, request)
	
		IMPLICIT NONE
		INCLUDE 'mpif.h'
		INTEGER, INTENT(IN) :: length   ! buffers' length
		TYPE(parcel_array),DIMENSION(length),INTENT(OUT) :: buffer  ! buffer of data
		INTEGER, INTENT(IN) :: tag
		INTEGER, INTENT(OUT) :: request
		INTEGER :: ierr
		CALL MPI_IRECV(buffer, length, newtype, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, request, ierr)
	
	END SUBROUTINE task_recv_parcel
	
	!------------------------------------------------------------------------------------------
	
	SUBROUTINE task_bsend_parcel(rank_to, buffer, length, tag)
	
		IMPLICIT NONE
		INCLUDE 'mpif.h'
		INTEGER, INTENT(IN) :: rank_to   ! receiving task's rank
		INTEGER, INTENT(IN) :: length    ! buffers' length
		TYPE(parcel_array),DIMENSION(length),INTENT(IN) :: buffer   ! buffer of data
		INTEGER, INTENT(IN) :: tag   ! tag of the message
		INTEGER :: ierr
		CALL MPI_SEND(buffer, length, newtype, rank_to, tag, MPI_COMM_WORLD, ierr)
		
	END SUBROUTINE task_bsend_parcel
	
	!------------------------------------------------------------------------------------------
	
	SUBROUTINE task_exchange_parcel
	
	! Non-blocking receives before blocking sends for efficiency on IBM SP.
	!
	! Procedure:
	!   1. 3D run or 2D run?
	!   2. Set ranks for loop.
	!   3. Obtain n_in, np_in, n_max. <= MPI exchange
	!   4. Receive and send parcels. <= MPI exchange
	!
	!      ----------------- ----------------- -----------------
	!     | i=4             | i=6             | i=8             |
	!     | ranks(4)=ranknw | ranks(6)=ranknn | ranks(8)=rankne |
	!     | tags(4)=3       | tags(6)=5       | tags(8)=7       |
	!      ----------------- ----------------- -----------------
	!     | i=2             |                 | i=1             |
	!     | ranks(2)=rankww |  current rank   | ranks(1)=rankee |
	!     | tags(2)=1       |                 | tags(1)=2       |
	!      ----------------- ----------------- -----------------
	!     | i=7             | i=5             | i=3             |
	!     | ranks(7)=ranksw | ranks(5)=rankss | ranks(3)=rankse |
	!     | tags(7)=8       | tags(5)=6       | tags(3)=4       |
	!      ----------------- ----------------- -----------------
	!     - i is the indices for neighbour ranks.
	!     - ranks are the actual neighbour rank indices.
	!     - tags converts to incoming index from outgoing index. If i=1 for the current rank,
	!       then i=2 for the right next rank.
	
		IMPLICIT NONE
		
		TYPE(parcel_array), ALLOCATABLE ::  buff_send(:,:)   ! buffer to send data
		TYPE(parcel_array), ALLOCATABLE ::  buff_recv(:,:)   ! buffer to receive data
		TYPE(parcel_pointer), pointer :: temp
		LOGICAL, DIMENSION(8) :: flag
		INTEGER, DIMENSION(8) :: ranks, n_in, reqs, tags, buff
		INTEGER :: n_max, tag_in, count, status, i, j, iend, n, ierr
		
		! 1. 3D run or 2D run
		IF ( RUN3D ) THEN
			iend = 8
		ELSE
			iend = 2
		ENDIF
		
		! 2. Prepare for MPI exchange
		ranks(1) = rankee   ! East
		ranks(2) = rankww   ! West
		ranks(3) = rankse   ! South east
		ranks(4) = ranknw   ! North west
		ranks(5) = rankss   ! South
		ranks(6) = ranknn   ! North
		ranks(7) = ranksw   ! South west
		ranks(8) = rankne   ! North East
		
		tags(1) = 2   ! rank tag for incoming parcels
		tags(2) = 1
		tags(3) = 4
		tags(4) = 3
		tags(5) = 6
		tags(6) = 5
		tags(7) = 8
		tags(8) = 7
		
		! 3. Obtain n_in by MPI
		! Non-brocking receives n_in from each neighbors:
		flag(:) = .FALSE.
		DO i = 1, iend
			IF ( rank /= ranks(i) ) THEN
				CALL task_recv_integer(buff(i), reqs(i))
				! i is the incoming rank index for the current rank. However, since number of
				! communication is known (either 2 or 8) and each communication passes/receives only
				! one number, MPI_ANY_TAG is used in task_rec_integer.
			ELSE
				flag(i) = .TRUE.
				! This flag indicates either no MPI communiation or complete MPI communication.
				! flag has to be true to avoid calling task_test for MPI communication.
			ENDIF
		ENDDO
		
		! Blocking sends n_out to each neighbors:
		! Fill the data from the buffers that have the same rank (were not sent):
		count = 0
		DO i = 1, iend
			IF ( rank /= ranks(i) ) THEN
				CALL task_bsend_integer(ranks(i), n_out(i), i)
			ELSE
				n_in(tags(i)) = n_out(i)   ! i is outgoing rank
				count = count + 1   ! tags(i) convers to incoming rank, e.g. i=1, tags(i)=2
			ENDIF
		ENDDO
		
		! Monitor the progress of receiving
		DO WHILE ( count < iend )
			DO i = 1, iend
				IF ( .NOT.flag(i) ) THEN
					CALL task_test(reqs(i), flag(i), status, tag_in)
					! tag_in is the same as i in task_bsend_integer.
					! tags(tag_in) converts to the correct incoming rank index for the current rank.
					IF ( flag(i) ) THEN
						n_in(tags(tag_in)) = buff(i)
						count = count + 1
					ENDIF
				ENDIF
			ENDDO
		ENDDO
		CALL task_barrier
		
		!IF (masterproc) THEN
		!	PRINT*,'LPT: from E ', n_in(1)
		!	PRINT*,'LPT: from W ', n_in(2)
		!	PRINT*,'LPT: from SE', n_in(3)
		!	PRINT*,'LPT: from NW', n_in(4)
		!	PRINT*,'LPT: from S ', n_in(5)
		!	PRINT*,'LPT: from N ', n_in(6)
		!	PRINT*,'LPT: from SW', n_in(7)
		!	PRINT*,'LPT: from NE', n_in(8)
		!ENDIF
		
		
		! 4. Obtain np_in, n_max and allocate arrays
		np_in = 0
		n_max = 0
		DO i = 1, iend
			np_in = np_in + n_in(i)
			IF ( n_in(i) > n_max ) n_max = n_in(i)
			IF ( n_out(i) > n_max ) n_max = n_out(i)
		ENDDO
		IF ( np_in == 0 ) np_in = 1   ! number of element has to be more than / equal to 1
		IF ( n_max == 0 ) n_max = 1   ! number of element has to be more than / equal to 1
		ALLOCATE( buff_recv(n_max,iend), buff_send(n_max,iend), parcel_in(np_in), STAT=ierr )
		IF ( ierr /= 0 ) THEN
			PRINT*,'LPT ERROR: Failed to allocate arrays in task_exchange_parcel on proc:', rank
			CALL lpt_task_abort
		ENDIF
		
		! 5. Receive and send parcels
		! Non-blocking receives first:
		flag(:) = .FALSE.
		DO i = 1, iend
			IF (( rank /= ranks(i) ).AND.( n_in(i) > 0 )) THEN
				CALL task_recv_parcel(buff_recv(1,i), n_in(i), tags(i), reqs(i))
				! i is the incoming rank index for the current rank.
				! tags(i) is the incoming rank, which should be same as i for the destination rank in
				! task_bsend_parcel.
			ELSE
				flag(i) = .TRUE.
				! This flag indicates either no MPI communiation or complete MPI communication.
				! flag has to be true to avoid calling task_test for MPI communication.
			ENDIF
		ENDDO
		
		! Blocking sends:
		NULLIFY(temp)   ! empty
		IF ( n_out(1) > 0 ) THEN
			IF ( .NOT.ASSOCIATED(parcel_out_ee_head) ) THEN
				IF (masterproc) PRINT*,'LPT: parcel_out_ee_head failed. Abort. rank:', rank
				CALL lpt_task_abort
			ELSE
				temp => parcel_out_ee_head	! in order not to mess head, use temporal pointer
				DO n = 1, n_out(1)
					buff_send(n,1)%tag = temp%tag
					buff_send(n,1)%nvi = temp%nvi
					buff_send(n,1)%var(:) = temp%var(:)
					temp => temp%next
				ENDDO
			ENDIF
			NULLIFY(temp)   ! for clean deallocation
		ENDIF
		IF ( n_out(2) > 0 ) THEN
			IF ( .NOT.ASSOCIATED(parcel_out_ww_head) ) THEN
				IF (masterproc) PRINT*,'LPT: parcel_out_ww_head failed. Abort. rank:', rank
				CALL lpt_task_abort
			ELSE
				temp => parcel_out_ww_head
				DO n = 1, n_out(2)
					buff_send(n,2)%tag = temp%tag
					buff_send(n,2)%nvi = temp%nvi
					buff_send(n,2)%var(:) = temp%var(:)
					temp => temp%next
				ENDDO
			ENDIF
			NULLIFY(temp)
		ENDIF
		IF ( RUN3D ) THEN
			IF ( n_out(3) > 0 ) THEN
				IF ( .NOT.ASSOCIATED(parcel_out_se_head) ) THEN
					IF (masterproc) PRINT*,'LPT: parcel_out_se_head failed. Abort. rank:', rank
					CALL lpt_task_abort
				ELSE
					temp => parcel_out_se_head
					DO n = 1, n_out(3)
						buff_send(n,3)%tag = temp%tag
						buff_send(n,3)%nvi = temp%nvi
						buff_send(n,3)%var(:) = temp%var(:)
						temp => temp%next
					ENDDO
				ENDIF
				NULLIFY(temp)
			ENDIF
			IF ( n_out(4) > 0 ) THEN
				IF ( .NOT.ASSOCIATED(parcel_out_nw_head) ) THEN
					IF (masterproc) PRINT*,'LPT: parcel_out_nw_head failed. Abort. rank:', rank
					CALL lpt_task_abort
				ELSE
					temp => parcel_out_nw_head
					DO n = 1, n_out(4)
						buff_send(n,4)%tag = temp%tag
						buff_send(n,4)%nvi = temp%nvi
						buff_send(n,4)%var(:) = temp%var(:)
						temp => temp%next
					ENDDO
				ENDIF
				NULLIFY(temp)
			ENDIF
			IF ( n_out(5) > 0 ) THEN
				IF ( .NOT.ASSOCIATED(parcel_out_ss_head) ) THEN
					IF (masterproc) PRINT*,'LPT: parcel_out_ss_head failed. Abort. rank:', rank
					CALL lpt_task_abort
				ELSE
					temp => parcel_out_ss_head
					DO n = 1, n_out(5)
						buff_send(n,5)%tag = temp%tag
						buff_send(n,5)%nvi = temp%nvi
						buff_send(n,5)%var(:) = temp%var(:)
						temp => temp%next
					ENDDO
				ENDIF
				NULLIFY(temp)
			ENDIF
			IF ( n_out(6) > 0 ) THEN
				IF ( .NOT.ASSOCIATED(parcel_out_nn_head) ) THEN
					IF (masterproc) PRINT*,'LPT: parcel_out_nn_head failed. Abort. rank:', rank
					CALL lpt_task_abort
				ELSE
					temp => parcel_out_nn_head
					DO n = 1, n_out(6)
						buff_send(n,6)%tag = temp%tag
						buff_send(n,6)%nvi = temp%nvi
						buff_send(n,6)%var(:) = temp%var(:)
						temp => temp%next
					ENDDO
				ENDIF
				NULLIFY(temp)
			ENDIF
			IF ( n_out(7) > 0 ) THEN
				IF ( .NOT.ASSOCIATED(parcel_out_sw_head) ) THEN
					IF (masterproc) PRINT*,'LPT: parcel_out_sw_head failed. Abort. rank:', rank
					CALL lpt_task_abort
				ELSE
					temp => parcel_out_sw_head
					DO n = 1, n_out(7)
						buff_send(n,7)%tag = temp%tag
						buff_send(n,7)%nvi = temp%nvi
						buff_send(n,7)%var(:) = temp%var(:)
						temp => temp%next
					ENDDO
				ENDIF
				NULLIFY(temp)
			ENDIF
			IF ( n_out(8) > 0 ) THEN
				IF ( .NOT.ASSOCIATED(parcel_out_ne_head) ) THEN
					IF (masterproc) PRINT*,'LPT: parcel_out_ne_head failed. Abort. rank:', rank
					CALL lpt_task_abort
				ELSE
					temp => parcel_out_ne_head
					DO n = 1, n_out(8)
						buff_send(n,8)%tag = temp%tag
						buff_send(n,8)%nvi = temp%nvi
						buff_send(n,8)%var(:) = temp%var(:)
						temp => temp%next
					ENDDO
				ENDIF
				NULLIFY(temp)
			ENDIF
		ENDIF   ! RUN3D
		
		DO i = 1, iend
			IF ( n_out(i) > 0 ) THEN
				IF ( rank /= ranks(i) ) THEN
					CALL task_bsend_parcel(ranks(i), buff_send(1:n_out(i),i), n_out(i), i)
					! i is the destination rank index from the current rank
				ELSE
					! Fill the buff_recv from buff_send that has the same rank
					DO j = 1, n_out(i)
						buff_recv(j,i) = buff_send(j,i)
					ENDDO
				ENDIF
			ENDIF
		ENDDO
		
		! Fill parcel_in from buff_recv that has the same rank:
		np_in = 0
		count = 0
		DO i = 1, iend
			IF ( flag(i) ) THEN   ! no MPI communication
				count = count + 1
				IF (( rank == ranks(tags(i)) ).AND.( n_in(tags(i)) > 0 )) THEN
					! There are outgoing parcels which will be periodically back to the same rank
					DO j = 1, n_in(tags(i))
						parcel_in(np_in+j)%tag = buff_recv(j,i)%tag
						parcel_in(np_in+j)%nvi = buff_recv(j,i)%nvi
						parcel_in(np_in+j)%var(:) = buff_recv(j,i)%var(:)
					ENDDO
					np_in = np_in + n_in(tags(i))
				ENDIF
			ENDIF
		ENDDO
		
		! Monitor the progress of receiving; fill the data:
		DO WHILE ( count < iend )
			DO i = 1, iend
				IF ( .NOT.flag(i) ) THEN
					CALL task_test(reqs(i), flag(i), status, tag_in)
					! reqs(i) is the same reqs(i) in task_recv_parcel.
					! tag_in is the same as tags(i) in task_recv_parcel.
					IF ( flag(i) ) then
						count = count + 1
						DO j = 1, n_in(i)
							parcel_in(np_in+j)%tag = buff_recv(j,i)%tag
							parcel_in(np_in+j)%nvi = buff_recv(j,i)%nvi
							parcel_in(np_in+j)%var(:) = buff_recv(j,i)%var(:)
						ENDDO
						np_in = np_in + n_in(i)
					ENDIF
				ENDIF
			ENDDO
		ENDDO
		
		CALL task_barrier
		
		DEALLOCATE( buff_recv, buff_send, STAT=ierr )
		IF ( ierr /= 0 ) THEN
			PRINT*,'LPT ERROR: Failed to allocate arrays in task_exchange_parcel on proc:', rank
			CALL lpt_task_abort
		ENDIF
		
	END SUBROUTINE task_exchange_parcel
	
	!============================= MPI CODE FOR GRID VALUE EXCHANGE ===========================
	
	SUBROUTINE task_exchange_lpt(f, dimx1, dimx2, dimy1, dimy2, dimz, i_1, i_2, j_1, j_2, id)
	
	! Non-blocking receives before blocking sends for efficiency on IBM SP.
	! Sends and receives the boundary messages
	! This subroutine is the same subroutine, "task_exchange.f90," but modified for LPT purpose.
	
		IMPLICIT NONE
		
		INTEGER, INTENT(IN) :: dimx1, dimx2, dimy1, dimy2, dimz
		INTEGER, INTENT(IN) :: i_1, i_2, j_1, j_2
		REAL(KIND=rlpt), DIMENSION(dimx1:dimx2, dimy1:dimy2, dimz), INTENT(IN) :: f
		INTEGER, INTENT(IN) :: id   ! id of the sent field
		
		
		INTEGER, PARAMETER :: nghost = MAX(ux1,ux2,vy1,vy2,sx1,sx2,sy1,sy2)
		INTEGER, PARAMETER :: bufflen = MAX(nx,ny) * nghost * nz
		REAL(KIND=rlpt), DIMENSION(bufflen) :: buff_send   ! buff to send data
		REAL(KIND=rlpt), DIMENSION(bufflen,8) :: buff_recv ! buff to receive data
		INTEGER, DIMENSION(8) :: reqs_in, ids  
		INTEGER, DIMENSION(8) :: i_start, i_end, j_start, j_end, ranks
		LOGICAL, DIMENSION(8) :: flag
		
		INTEGER i, j, k, n, rf, tag, m, mend, i1, i2, j1, j2, mask, count
		
		i1 = i_1 - 1
		i2 = i_2 - 1
		j1 = j_1 - 1
		j2 = j_2 - 1
		! East
		i_start(1) = nx-i1
		i_end  (1) = nx
		j_start(1) = 1
		j_end  (1) = ny
		! West
		i_start(2) = 1
		i_end  (2) = 1+i2
		j_start(2) = 1
		j_end  (2) = ny
		! North
		i_start(7) = 1
		i_end  (7) = nx
		j_start(7) = ny-j1
		j_end  (7) = ny
		! North East
		i_start(8) = nx-i1
		i_end  (8) = nx
		j_start(8) = ny-j1
		j_end  (8) = ny
		! South East
		i_start(3) = nx-i1
		i_end  (3) = nx
		j_start(3) = 1
		j_end  (3) = 1+j2
		! South
		i_start(4) = 1
		i_end  (4) = nx
		j_start(4) = 1
		j_end  (4) = 1+j2
		! South West
		i_start(5) = 1
		i_end  (5) = 1+i2
		j_start(5) = 1
		j_end  (5) = 1+j2
		! North West
		i_start(6) = 1
		i_end  (6) = 1+i2
		j_start(6) = ny-j1
		j_end  (6) = ny
		
		ranks(1) = rankee
		ranks(2) = rankww
		ranks(7) = ranknn
		ranks(8) = rankne
		ranks(3) = rankse
		ranks(4) = rankss
		ranks(5) = ranksw
		ranks(6) = ranknw
		
		mask=id*100000+i1*10000+i2*1000+j1*100+j2*10
		
		ids(1) = mask+7
		ids(2) = mask+3
		ids(7) = mask+5
		ids(8) = mask+6
		ids(3) = mask+8
		ids(4) = mask+1
		ids(5) = mask+2
		ids(6) = mask+4
		
		! Send/receive buffs to/from neighbors
		IF ( RUN3D ) THEN
			mend = 8
		ELSE
			mend = 2
		ENDIF
		
		! Non-blocking receives first:
		DO m = 1, mend
			IF ( rank /= ranks(m) ) THEN
				CALL task_receive_lpt(buff_recv(:,m), bufflen, reqs_in(m))
				flag(m) = .FALSE.
			ELSE
				flag(m) = .TRUE.
			ENDIF
		ENDDO
		
		! Blocking sends:
		DO m = 1, mend
			n = 0
			DO k = 1, dimz
				DO j = j_start(m), j_end(m)
					DO i = i_start(m), i_end(m)
						n = n+1
						buff_send(n) = f(i,j,k)
					ENDDO
				ENDDO
			ENDDO
			IF ( rank /= ranks(m) ) THEN
				CALL task_bsend_lpt(ranks(m), buff_send, n, ids(m))
			ELSE
				DO i = 1, n
					buff_recv(i,m) = buff_send(i)
				ENDDO
			ENDIF
		ENDDO
		
		! Fill the data from the buffers that have the same rank (were not sent):
		count = 0
		DO m = 1, mend
			IF ( flag(m) ) THEN
				count = count+1
				tag = ids(m)
				CALL task_dispatch_lpt(bufflen, buff_recv(1:bufflen,m), tag)
			ENDIF
		ENDDO
		
		! Monitor the progress of receiving; fill the data:
		DO WHILE (count < mend)
			DO m = 1, mend
				IF ( .NOT.flag(m) ) THEN
					CALL task_test(reqs_in(m), flag(m), rf, tag)
					IF ( flag(m) ) THEN
						count = count + 1
						CALL task_dispatch_lpt(bufflen, buff_recv(1:bufflen,m), tag)
					ENDIF
				ENDIF
			ENDDO
		ENDDO
		
		CALL task_barrier()
		
	END SUBROUTINE task_exchange_lpt
	
	!------------------------------------------------------------------------------------------
	
	SUBROUTINE task_dispatch_lpt(blen, buff, tag)
	
	! Dispatches the messages according to the field sent
	
		IMPLICIT NONE
		
		INTEGER, INTENT(IN) :: blen
		REAL(KIND=rlpt), DIMENSION(blen), INTENT(IN) :: buff
		INTEGER, INTENT(IN) :: tag
		INTEGER :: field, n
		
		field = tag / 100000
		
		SELECT CASE(field)
		! resolved velocity components, field index: 1-3
		CASE(1)
			CALL task_assign_lpt(ures, x1u, x2u, y1u, y2u, nzlpt, buff, blen, tag)
		CASE(2)
			CALL task_assign_lpt(vres, x1v, x2v, y1v, y2v, nzlpt, buff, blen, tag)
		CASE(3)
			CALL task_assign_lpt(wres, x1w, x2w, y1w, y2w, nzlpt, buff, blen, tag)
		
		! SGS variables for diffusive velocity, field index: 4 for SGS TKE, 5-10 for other
		CASE(4) 
			CALL task_assign_lpt(tkes3D(:,:,:,t2lpt), x1s, x2s, y1s, y2s, nzlpt, buff, blen, tag)
		CASE(5)
			CALL task_assign_lpt(diss3D, x1s, x2s, y1s, y2s, nzlpt, buff, blen, tag)
		CASE(6)
			CALL task_assign_lpt(tke3D, x1s, x2s, y1s, y2s, nzlpt, buff, blen, tag)
		
		! scalars, field index: 11-40
		CASE(11)
			CALL task_assign_lpt(p3D, x1s, x2s, y1s, y2s, nzlpt, buff, blen, tag)
		CASE(12)
			CALL task_assign_lpt(t3D, x1s, x2s, y1s, y2s, nzlpt, buff, blen, tag)
		CASE(13)
			CALL task_assign_lpt(qv3D, x1s, x2s, y1s, y2s, nzlpt, buff, blen, tag)
		CASE(14)
			CALL task_assign_lpt(qcl3D, x1s, x2s, y1s, y2s, nzlpt, buff, blen, tag)
		CASE(15)
			CALL task_assign_lpt(qci3D, x1s, x2s, y1s, y2s, nzlpt, buff, blen, tag)
		CASE(16)
			CALL task_assign_lpt(qpl3D, x1s, x2s, y1s, y2s, nzlpt, buff, blen, tag)
		CASE(17)
			CALL task_assign_lpt(qpi3D, x1s, x2s, y1s, y2s, nzlpt, buff, blen, tag)
		
		! micro_field:
		CASE(field_id_micro1:field_id_micro2)
			n = field - field_id_micro1 + 1
			CALL task_assign_lpt(micro3D(:,:,:,n), x1s, x2s, y1s, y2s, nzlpt, buff, blen, tag)
		
		! tracer:
		CASE(field_id_tracer1:field_id_tracer2)
			n = field - field_id_tracer1 + 1
			CALL task_assign_lpt(tracer3D(:,:,:,n), x1s, x2s, y1s, y2s, nzlpt, buff, blen, tag)
		
		! new scalars, field index: 41-50
		CASE(field_id_newout1:field_id_newout2)
			CALL task_assign_lpt(rig3D, x1s, x2s, y1s, y2s, nzlpt, buff, blen, tag)
		
		END SELECT
		
	END SUBROUTINE task_dispatch_lpt
	
	!------------------------------------------------------------------------------------------
	
	SUBROUTINE task_assign_lpt(f, dimx1, dimx2, dimy1, dimy2, dimz, buff, blen, tag)
	
	! This routine assignes the boundary info after MPI exchange
	
		IMPLICIT NONE
		
		INTEGER, INTENT(IN) :: dimx1, dimx2, dimy1, dimy2, dimz
		REAL(KIND=rlpt), DIMENSION(dimx1:dimx2,dimy1:dimy2,dimz), INTENT(INOUT) :: f
		INTEGER, INTENT(IN) :: blen
		REAL(KIND=rlpt), DIMENSION(blen), INTENT(IN) :: buff
		INTEGER, INTENT(IN) :: tag
		INTEGER :: i, j, k, n, proc, i1, i2, j1, j2
		
		! Decode the tag:
		i  = tag / 100000
		i1 = (tag - i * 100000) / 10000
		i2 = (tag - i * 100000 - i1 * 10000)/1000
		j1 = (tag - i * 100000 - i1 * 10000 - i2 * 1000) / 100
		j2 = (tag - i * 100000 - i1 * 10000 - i2 * 1000 - j1 * 100) / 10
		proc = tag - i * 100000 - i1 * 10000 - i2 * 1000 - j1 * 100 - j2 * 10
		
		SELECT CASE(proc)
		CASE(1)   ! From "North":
			n = 0
			DO k = 1, dimz
				DO j = nyp1, nyp1+j2
					DO i = 1, nx
						n = n + 1
						f(i,j,k) = buff(n)
					ENDDO
				ENDDO
			ENDDO
		CASE(2)   ! From "North-East":
			n = 0
			DO k = 1, dimz
				DO j = nyp1, nyp1+j2
					DO i = nxp1, nxp1+i2
						n = n + 1
						f(i,j,k) = buff(n)
					ENDDO
				ENDDO
			ENDDO
		CASE(3)   ! From "East":
			n = 0
			DO k = 1, dimz
				DO j = 1, ny
					DO i = nxp1, nxp1+i2
						n = n + 1
						f(i,j,k) = buff(n)
					ENDDO
				ENDDO
			ENDDO
		CASE(4)   ! From "South-East":
			n = 0
			DO k = 1, dimz
				DO j = -j1, 0
					DO i = nxp1, nxp1+i2
						n = n + 1
						f(i,j,k) = buff(n)
					ENDDO
				ENDDO
			ENDDO
		CASE(5)   ! From "South":
			n = 0
			DO k = 1, dimz
				DO j = -j1, 0
					DO i = 1, nx
						n = n + 1
						f(i,j,k) = buff(n)
					ENDDO
				ENDDO
			ENDDO
		CASE(6)   ! From "South-West":
			n = 0
			DO k = 1, dimz
				DO j = -j1, 0
					DO i = -i1, 0
						n = n + 1
						f(i,j,k) = buff(n)
					ENDDO
				ENDDO
			ENDDO
		CASE(7)   ! From "West":
			n = 0
			DO k = 1, dimz
				DO j = 1, ny
					DO i = -i1, 0
						n = n + 1
						f(i,j,k) = buff(n)
					ENDDO
				ENDDO
			ENDDO
		CASE(8)   ! From "North-West":
			n = 0
			DO k = 1, dimz
				DO j = nyp1, nyp1+j2
					DO i = -i1, 0
						n = n + 1
						f(i,j,k) = buff(n)
					ENDDO
				ENDDO
			ENDDO
		END SELECT
		
	END SUBROUTINE task_assign_lpt
	
	!====================================== MPI UTILITIY ======================================
	
	SUBROUTINE lpt_task_abort
	
	! Abort process instead of using task_abort in task_util_MPI.f90
	
		USE grid, ONLY: dompi
		INCLUDE 'mpif.h'
		INTEGER ierr, rc
		
		IF (dompi) THEN
			CALL MPI_ABORT(MPI_COMM_WORLD, rc, ierr)
			CALL MPI_FINALIZE(ierr)
		ENDIF
		CALL EXIT(999) ! to avolid resubmission when finished
		
	END SUBROUTINE lpt_task_abort
	
	!------------------------------------------------------------------------------------------
	
	SUBROUTINE task_recv_integer(buffer, request)
	
		IMPLICIT NONE
		INCLUDE 'mpif.h'
		INTEGER, INTENT(OUT) :: buffer   ! buffer of data
		INTEGER, INTENT(OUT) :: request
		INTEGER :: ierr
		CALL MPI_IRECV(buffer,1,MPI_INTEGER,MPI_ANY_SOURCE,MPI_ANY_TAG,MPI_COMM_WORLD,request,ierr)
		
	END SUBROUTINE task_recv_integer
	
	!------------------------------------------------------------------------------------------
	
	SUBROUTINE task_bsend_integer(rank_to, buffer, tag)
	
		IMPLICIT NONE
		INCLUDE 'mpif.h'
		INTEGER, INTENT(IN) :: rank_to   ! receiving task's rank
		INTEGER, INTENT(IN) :: buffer    ! buffer of data
		INTEGER, INTENT(IN) :: tag       ! tag of the message
		INTEGER :: ierr
		CALL MPI_SEND(buffer,1,MPI_INTEGER,rank_to,tag,MPI_COMM_WORLD,ierr)
		
	END SUBROUTINE task_bsend_integer
	
	!------------------------------------------------------------------------------------------
	
	SUBROUTINE task_receive_lpt(buffer, length, request)
	
		IMPLICIT NONE
		INCLUDE 'mpif.h'
		INTEGER, INTENT(IN) :: length   ! buffers' length
		REAL(KIND=rlpt), DIMENSION(length), INTENT(OUT) :: buffer   ! buffer of data
		INTEGER, INTENT(OUT) :: request
		INTEGER :: ierr
		
		SELECT CASE(rlpt)
		CASE(4)
			CALL MPI_IRECV( buffer, length, MPI_REAL4, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &
			                request, ierr )
		CASE(8)
			CALL MPI_IRECV( buffer, length, MPI_REAL8, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &
			                request, ierr )
		END SELECT
		
	END SUBROUTINE task_receive_lpt
	
	!------------------------------------------------------------------------------------------
	
	SUBROUTINE task_bsend_lpt(rank_to, buffer, length, tag)
	
		IMPLICIT NONE
		INCLUDE 'mpif.h'
		INTEGER, INTENT(IN) :: rank_to   ! receiving task's rank
		INTEGER, INTENT(IN) :: length    ! buffers' length
		REAL(KIND=rlpt), DIMENSION(length), INTENT(IN) :: buffer   ! buffer of data
		INTEGER, INTENT(IN) :: tag   ! tag of the message
		INTEGER :: ierr
		
		SELECT CASE(rlpt)
		CASE(4)
			CALL MPI_SEND( buffer, length, MPI_REAL4, rank_to, tag, MPI_COMM_WORLD, ierr )
		CASE(8)
			CALL MPI_SEND( buffer, length, MPI_REAL8, rank_to, tag, MPI_COMM_WORLD, ierr )
		END SELECT
		
	END SUBROUTINE task_bsend_lpt
	
	!==========================================================================================
	
END MODULE lagrangian_parcel_tracker
