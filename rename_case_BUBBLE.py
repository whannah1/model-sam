#!/usr/bin/env python
#========================================================================================================================
# 	This script renames files of an ensemble of SAM runs 
#
#    Jul, 2013	Walter Hannah 		Colorado State University
#========================================================================================================================
import datetime
import sys
import os
import numpy as np
import glob
#========================================================================================================================
#========================================================================================================================

debug = False

skip_3D = False

shear_ideal = True

umax = "04"	# max u-wind value for idealized shear profile

qsig = 0.0	# factor to increase moisture (in std. deviations)
tsig = 0.0	# factor to increase temperature (in std. deviations)

nx = 144	# number of points in the x & y directions
nz =  64	# number of vertical levels

dx = 500	# Horizontal resolution [m]
dt =   5	# timestep		[s]

SAM_DIR	= "/maloney-scratch/whannah/SAM/"
SAM_EXE = "SAM_ADV_MPDATA_RAD_CAM_MICRO_SAM1MOM_"+str(nx)+"x"+str(nz)

#BUB_DX	= [4,6,8,10,12]
BUB_DX	= [8,12]

BUB_DT	= [1.0,2.0]
BUB_DQ	= [3,6]

#========================================================================================================================
#========================================================================================================================
ndx = len(BUB_DX)
ndt = len(BUB_DT)
ndq = len(BUB_DQ)

with open(SAM_DIR+"CaseName", "w") as CaseName_file:	# make sure CaseName indicates
    CaseName_file.write("BUBBLE\n")			# we are running the BUBBLE case
#========================================================================================================================
#========================================================================================================================
print
print
print "Running SAM BUBBLE ensemble  (",datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),")"
print
print
for x in xrange(ndx):
	for t in range(ndt):
		for q in range(ndq):
			#-----------------------------------------------------------------------------------
			# Set the old case name
			#-----------------------------------------------------------------------------------
			CDX=format(BUB_DX[x],"02.0f")
			CDT=str(BUB_DT[t])
			CDQ=str(BUB_DQ[q])
			qsigstr = format(qsig,"01.1f")
			tsigstr = format(tsig,"01.1f")
			old_CASE_TAIL = "LP_"+str(dx)+"_"+str(nx)+"x"+str(nz)
			if shear_ideal == True:
				old_CASE_TAIL = old_CASE_TAIL+"_shear"+umax
			old_CASE_TAIL = old_CASE_TAIL#+"_qsig"+qsigstr+"_tsig"+tsigstr
			old_CASE_TAIL = old_CASE_TAIL+"_"+CDX+"km_"+CDT+"k_"+CDQ+"g"
			old_CASE = "BUBBLE_"+old_CASE_TAIL
			print "  old CASE = "+old_CASE
			#-----------------------------------------------------------------------------------
			# Set the old new name
			#-----------------------------------------------------------------------------------
			CDX=format(BUB_DX[x],"02.0f")
			CDT=str(BUB_DT[t])
			CDQ=str(BUB_DQ[q])
			qsigstr = format(qsig,"01.1f")
			tsigstr = format(tsig,"01.1f")
			new_CASE_TAIL = "LP_"+str(dx)+"_"+str(nx)+"x"+str(nz)
			if shear_ideal == True:
				new_CASE_TAIL = new_CASE_TAIL+"_shear"+umax
			new_CASE_TAIL = new_CASE_TAIL+"_q"+qsigstr+"_t"+tsigstr
			new_CASE_TAIL = new_CASE_TAIL+"_"+CDX+"km_"+CDT+"k_"+CDQ+"g"
			new_CASE = "BUBBLE_"+new_CASE_TAIL
			print "  new CASE = "+new_CASE
			print
			#-----------------------------------------------------------------------------------
			# Edit the file and folder names
			#-----------------------------------------------------------------------------------
			idir = SAM_DIR+old_CASE+"/"
			odir = SAM_DIR+new_CASE+"/"
			CMD = "mv "+idir+" "+odir
			print "    "+CMD
			if debug!=True and os.path.isdir(idir) :  os.system(CMD)
			#-----------------------------------------------------------------------------------
			print
			idir = SAM_DIR+new_CASE+"/OUT_2D/"+old_CASE+"*"
			print("  "+idir+":")
			for f in sorted(glob.glob(idir)):
				ifile = f
				ofile = f.replace(old_CASE,new_CASE)
				CMD = "mv "+ifile+" "+ofile
				#print "  "+CMD
				print "    "+f.replace(SAM_DIR+new_CASE+"/OUT_2D/","").ljust(55)+"  >  "\
				            +f.replace(SAM_DIR+new_CASE+"/OUT_2D/","").replace(old_CASE,new_CASE)
				if debug!=True and os.path.isfile(f) : os.system(CMD)
			#-----------------------------------------------------------------------------------
			print
			idir = SAM_DIR+new_CASE+"/OUT_STAT/"+old_CASE+"*"
			print("  "+idir+":")
			for f in sorted(glob.glob(idir)):
				ifile = f
				ofile = f.replace(old_CASE,new_CASE)
				CMD = "mv "+ifile+" "+ofile
				#print "  "+CMD
				print "    "+f.replace(SAM_DIR+new_CASE+"/OUT_STAT/","").ljust(55)+"  >  " \
				            +f.replace(SAM_DIR+new_CASE+"/OUT_STAT/","")
				if debug!=True and os.path.isfile(f) : os.system(CMD)
			#-----------------------------------------------------------------------------------
			print 
			idir = SAM_DIR+new_CASE+"/OUT_LPT/"+old_CASE+"*"
			print("  "+idir+":")
			for f in sorted(glob.glob(idir)):
				ifile = f
				ofile = f.replace(old_CASE,new_CASE)
				CMD = "mv "+ifile+"  "+ofile
				print "    "+f.replace(SAM_DIR+new_CASE+"/OUT_LPT/","").ljust(55)+"  >  "\
					    +f.replace(SAM_DIR+new_CASE+"/OUT_LPT/","").replace(old_CASE,new_CASE)
				if debug!=True and os.path.isfile(f) : os.system(CMD)
			#-----------------------------------------------------------------------------------
			if skip_3D==False :
				print 
				idir = SAM_DIR+new_CASE+"/OUT_3D/"+old_CASE+"*"
				print("  "+idir+":")
				for f in sorted(glob.glob(idir)):
					ifile = f
					ofile = f.replace(old_CASE,new_CASE)
					CMD = "mv "+ifile+"  "+ofile
					print "    "+f.replace(SAM_DIR+new_CASE+"/OUT_3D/","").ljust(55)+"  >  "\
						    +f.replace(SAM_DIR+new_CASE+"/OUT_3D/","").replace(old_CASE,new_CASE)
					if debug!=True and os.path.isfile(f) : os.system(CMD)
				#-----------------------------------------------------------------------------------
				print 
				idir = SAM_DIR+new_CASE+"/OUT_3D/*ENTRAIN*"+old_CASE_TAIL+"*"
				print("  "+idir+":")
				for f in sorted(glob.glob(idir)):
					ifile = f
					ofile = f.replace(old_CASE_TAIL,new_CASE_TAIL)
					CMD = "mv "+ifile+"  "+ofile
					print "    "+f.replace(SAM_DIR+new_CASE+"/OUT_3D/","").ljust(55)+"  >  "\
						    +f.replace(SAM_DIR+new_CASE+"/OUT_3D/","").replace(old_CASE_TAIL,new_CASE_TAIL)
					if debug!=True and os.path.isfile(f) : os.system(CMD)
				#-----------------------------------------------------------------------------------

print
#========================================================================================================================
#========================================================================================================================


