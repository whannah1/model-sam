#!/usr/bin/env python
#========================================================================================================================
#   This script runs an ensemble of SAM runs with various initial   
#   anomaly values for a bubble at the center of the domain     
#
#    Jul, 2013  Walter Hannah       Colorado State University
#========================================================================================================================
import datetime
import sys
import os
import numpy as np
#========================================================================================================================
#========================================================================================================================
RUN,POST, CLEAR, MOVE = False,False,False,False

RUN   = True
POST  = True
# CLEAR = True
MOVE  = True

CASENAME = "BUBBLE"

ens_case_tail = "_ens1"

restart_code = 0   # 0=start  1=continue   2=continue with new namelist

qsig = 0.0      # factor to increase environmental moisture    (in % RH)
tsig = 0.0      # factor to increase environmental temperature (in std. deviations)

# nx = 128        # number of points in the x & y directions
# nz = 128        # number of vertical levels
# dx = 200        # Horizontal resolution [m]
# dt =   2        # timestep      [s]

nx = 256        # number of points in the x & y directions
nz = 256        # number of vertical levels
dx = 100        # Horizontal resolution [m]
dt =   1        # timestep      [s]

# BUB_DX = list(range(10,50,2))
# BUB_DX = [float(i)/1e1 for i in BUB_DX]
#BUB_DX.remove(1.0)
#BUB_DX.remove(2.0)
#BUB_DX.remove(3.0)
#BUB_DX.remove(4.0)

#BUB_DX  = [5.,3.,2.,1.]
# BUB_DX  = [1.,2.,4.]
BUB_DX  = [2.0]

BUB_DT  = [ 1.25 ]
BUB_DQ  = [ 1.9 ]

# BUB_DT = [ 1.0, 1.25, 0.75 ]
# BUB_DQ  = [ 2.0, 1.9 , 2.1  ]

# BUB_DT  = [0.5,1.5,1.0,0.5,1.5]
# BUB_DQ  = [1.0,3.0,2.0,3.0,1.0]
# BUB_DT  = [0.5,0.5,0.5  \
#           ,1.0,1.0,1.0  \
#           ,1.5,1.5,1.5  ]
# BUB_DQ  = [1.0,2.0,3.0  \
#           ,1.0,2.0,3.0  \
#           ,1.0,2.0,3.0  ]

# BUB_DT  = [0.5,1.0,1.0,1.5]
# BUB_DQ  = [2.0,1.0,3.0,2.0]


nstop   =  3600/dt
data_dt =  30/dt
#data_dt =  60/dt
start3D =  0
start2D =  0


shear_ideal = True
# umax = [10,20,30,40]          # max u-wind value for idealized shear profile
umax = [ 30 ]

debug = False
# quick run setup for debugging
if debug :
    nstop   =  1*60/dt
    #data_dt =  5/dt
    start3D =  nstop+100
    start2D =  nstop+100

#SAM_DIR    = "/maloney-scratch/whannah/SAM/6.9.5_ent_LP/"
SAM_DIR = "/home/whannah/Model/SAM/6.9.5_ent_LP/"
SCR_DIR = "/home/whannah/Data/Model/SAM/scratch/"
SAM_EXE = "SAM_ADV_MPDATA_RAD_CAM_MICRO_SAM1MOM_"+str(nx)+"x"+str(nz)
UTL_DIR = SAM_DIR+"UTIL/"
UTL_LPT = SAM_DIR+"UTIL_LPT/"
LOG_DIR = SAM_DIR+"logs/"
#========================================================================================================================
#========================================================================================================================
nproc = 16

if not shear_ideal : umax = [0]

ndx = len(BUB_DX)
ndt = len(BUB_DT)
ndq = len(BUB_DQ)
ndu = len(umax)

with open(SAM_DIR+"CaseName", "w") as CaseName_file :    # make sure CaseName indicates
    CaseName_file.write("BUBBLE\n")                      # we are running the BUBBLE case
#========================================================================================================================
#========================================================================================================================
print
print
print "Running SAM BUBBLE ensemble  (",datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),")"
print
print
q = 0
# u = 0
# x = 0
for x in xrange(ndx):
    for t in range(ndt):
        # for q in range(ndq):
        for u in range(ndu):
            #-----------------------------------------------------------------------------------
            # Set the case name
            #-----------------------------------------------------------------------------------
            #CDX=format(BUB_DX[x],"02.0f")
            CDX = str(BUB_DX[x])
            CDT=str(BUB_DT[t])
            # CDQ=str(BUB_DQ[q])
            CDQ=str(BUB_DQ[t])
            qsigstr = format(qsig,"01.1f")
            tsigstr = format(tsig,"01.1f")
            shstr   = format(umax[u],"02d")
            #CASE_TAIL = "LP_"+str(dx)+"_"+str(nx)+"x"+str(nz)
            #CASE_TAIL = "301K_LP_"+str(dx)+"_"+str(nx)+"x"+str(nz)
            CASE_TAIL = "301K_"+str(dx)+"_"+str(nx)+"x"+str(nz)
            if shear_ideal == True:
                CASE_TAIL = CASE_TAIL+"_shear"+shstr    #umax
            CASE_TAIL = CASE_TAIL+"_q"+qsigstr+"_t"+tsigstr             # probably don't need this anymore
            CASE_TAIL = CASE_TAIL+"_"+CDX+"km_"+CDT+"k_"+CDQ+"g"
            CASE_TAIL = CASE_TAIL+ens_case_tail
            CASE = "BUBBLE_"+CASE_TAIL
            print "  CASE = "+CASE
            #-----------------------------------------------------------------------------------
            # Edit the namelist file
            #-----------------------------------------------------------------------------------
            #ifile = SAM_DIR+"BUBBLE/prm_template_"+str(dx)
            ofile = SAM_DIR+"BUBBLE/prm"
            
            os.system( "sed -i \"s/day0 =.*"        +"/day0      = "     +str("0.0")        +",/\"  "   +ofile )
            os.system( "sed -i \"s/caseid =.*"      +"/caseid = \'"      +CASE_TAIL         +"\',/\"  " +ofile )
            os.system( "sed -i \"s/nrestart =.*"    +"/nrestart = "      +str(restart_code) +",/\" "    +ofile )
            os.system( "sed -i 's/dx =.*"           +"/dx = "            +str(dx)           +"./' "     +ofile )
            os.system( "sed -i 's/dy =.*"           +"/dy = "            +str(dx)           +"./' "     +ofile )
            os.system( "sed -i 's/dt =.*"           +"/dt = "            +str(dt)           +"/'  "     +ofile )
            os.system( "sed -i 's/nrad .*"          +"/nrad       = "    +str(3*60/dt)      +"/'  "     +ofile )
            os.system( "sed -i 's/nstop.*"          +"/nstop      = "    +str(nstop)        +"/'  "     +ofile )
            os.system( "sed -i 's/nprint .*"        +"/nprint     = "    +str(nstop)        +"/'  "     +ofile )
            os.system( "sed -i 's/dolpt.*"          +"/dolpt          = .false. "           +"/'  "     +ofile )
            os.system( "sed -i 's/doentrainment.*"  +"/doentrainment  = .true. "            +"/'  "     +ofile )
            # output data intervals
            # os.system( "sed -i 's/nstatfrq .*"      +"/nstatfrq  = "        +str(data_dt)   +"/'  "     +ofile )
            os.system( "sed -i 's/nstatfrq .*"      +"/nstatfrq  = "        +str(1)   +"/'  "     +ofile )
            os.system( "sed -i 's/nstat .*"         +"/nstat     = "        +str(data_dt)   +"/'  "     +ofile )
            os.system( "sed -i 's/nsave2D .*"       +"/nsave2D        = "   +str(data_dt)   +"/'  "     +ofile )
            os.system( "sed -i 's/nsave3D .*"       +"/nsave3D        = "   +str(data_dt)   +"/'  "     +ofile )
            os.system( "sed -i 's/nent3D .*"        +"/nent3D         = "   +str(data_dt)   +"/'  "     +ofile )
            os.system( "sed -i 's/nsave2Dstart .*"  +"/nsave2Dstart   = "   +str(start2D)   +"/'  "     +ofile )
            os.system( "sed -i 's/nsave3Dstart .*"  +"/nsave3Dstart   = "   +str(start3D)   +"/'  "     +ofile )
            os.system( "sed -i 's/nent3Dstart .*"   +"/nent3Dstart    = "   +str(start3D)           +"/'  "     +ofile )
            #os.system( "sed -i 's/nent3Dstart .*"   +"/nent3Dstart    = "   +"99999"           +"/'  "     +ofile )
            # bubble settings
            os.system( "sed -i 's/bubble_x0 .*"        +"/bubble_x0         = "+str(nx*dx/2)           +"/'  " +ofile )
            os.system( "sed -i 's/bubble_y0 .*"        +"/bubble_y0         = "+str(nx*dx/2)           +"/'  " +ofile )
            os.system( "sed -i 's/bubble_z0 .*"        +"/bubble_z0         = "+str(500)               +"/'  " +ofile )
            os.system( "sed -i 's/bubble_dtemp .*"     +"/bubble_dtemp      = "+str(BUB_DT[t])         +"/'  " +ofile )
            # os.system( "sed -i 's/bubble_dq .*"        +"/bubble_dq         = "+str(BUB_DQ[q]/1000.)   +"/'  " +ofile )
            os.system( "sed -i 's/bubble_dq .*"        +"/bubble_dq         = "+str(BUB_DQ[t]/1000.)   +"/'  " +ofile )
            
            os.system( "sed -i 's/bubble_radius_hor .*"+"/bubble_radius_hor = "+str(BUB_DX[x]*1000.)   +"/'  " +ofile )
            os.system( "sed -i 's/bubble_radius_ver .*"+"/bubble_radius_ver = 1500.0 "                 +"/'  " +ofile )
            

            #with open(ofile, 'w') as file:
            #    file.writelines( data )
            #-----------------------------------------------------------------------------------
            # Run the Case
            #-----------------------------------------------------------------------------------
            
            # Specify snd file based on shear flag
            os.system("rm -f "+SAM_DIR+"BUBBLE/snd ")
            snd_file = "BUBBLE/snd.v1"
            if shear_ideal == True:
                snd_file = snd_file+".shear_"+shstr+"_ideal"
            else:
                snd_file = snd_file+".no_wind"
            snd_file = snd_file+".qsig_"+qsigstr+".tsig_"+tsigstr
            print("  snd  = "+snd_file)
            print("")

            # exit()

            snd_file = SAM_DIR+snd_file
            CMD = "cp "+snd_file+"  "+SAM_DIR+"BUBBLE/snd"
            print CMD
            os.system(CMD)
            
            # Specify grd file based on dx
            #grd_file = "BUBBLE/grd_"+str(nz)+"_"+str(dx)+"dx"
            grd_file = "BUBBLE/grd_"+str(dx)+"dx"
            print "  grd  = "+grd_file
            grd_file = SAM_DIR+grd_file
            CMD = "cp "+grd_file+"  "+SAM_DIR+"BUBBLE/grd"
            print CMD
            os.system(CMD)
            
            if RUN == True:
                print datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                CMD = "time mpirun -n "+str(nproc)+" ./"+SAM_EXE+"_B"+CDX+"km"+"  &>  "+LOG_DIR+CASE+".out"
                #CMD = "time mpirun -n "+str(nproc)+" ./"+SAM_EXE+"_B"+CDX+"km"
                print CMD
                os.system(CMD)
                
                print
                print datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                print "Finished running case: "+CASE+"  ( "+LOG_DIR+CASE+".out )"
            #-----------------------------------------------------------------------------------
            # Process the output
            #-----------------------------------------------------------------------------------
            if POST == True:
                print "Post-processing "+CASE+" (",datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),")"
                print
                print
                #--------------------------------------
                # STAT output
                #--------------------------------------
                for f in os.listdir(SAM_DIR+"OUT_STAT/"):
                    if f.endswith(".stat") and (CASE in f): 
                        CMD = UTL_DIR+"stat2nc "+SCR_DIR+"OUT_STAT/"+f+"  >  "+LOG_DIR+"post."+CASE+".stat.out"
                        print CMD
                        os.system(CMD)
                #--------------------------------------
                # LPT output
                #--------------------------------------
                # for f in os.listdir(SAM_DIR+"OUT_LPT/"):
                #     if f.endswith(".bin") and (CASE in f): 
                #         CMD = UTL_LPT+"bin2nc_lpt "+SCR_DIR+"OUT_LPT/"+f+"  >  "+LOG_DIR+"post."+CASE+".lpt.out"
                #         print CMD
                #         os.system(CMD)
                #--------------------------------------
                # 2D output
                #--------------------------------------
                for f in os.listdir(SAM_DIR+"OUT_2D/"):
                    if f.endswith(".2Dcom") and (CASE in f): 
                        CMD = UTL_DIR+"2Dcom2nc "+SCR_DIR+"OUT_2D/"+f+"  >  "+LOG_DIR+"post."+CASE+".2D.out"
                        print CMD
                        os.system(CMD)
                #--------------------------------------
                # 3D output (normal & tet. entrainment)
                #--------------------------------------
                logfile = LOG_DIR+"post."+CASE+".3D.out"
                os.system("rm "+logfile)
                os.system("touch "+logfile)

                #SCR_DIR = "/home/whannah/Data/SAM/"+CASE+"/"

                print
                print "  3D output log file: "+logfile
                for f in os.listdir(SCR_DIR+"OUT_3D/"):
                    if f.endswith(".com3D") and not os.path.isfile(SCR_DIR+"OUT_SAM_DIR3D/"+f.replace(".com3D",".nc")) and (CASE in f):
                        CMD = UTL_DIR+"com3D2nc "+SCR_DIR+"OUT_3D/"+f+"  >>  "+logfile
                        #print CMD
                        os.system(CMD)
                    if f.endswith(".com3D") and not os.path.isfile(SCR_DIR+"OUT_3D/"+f.replace(".com3D",".nc")) and ("ENTRAIN" in f):
                        CMD = UTL_DIR+"com3D2nc_alt "+SCR_DIR+"OUT_3D/"+f+"  >>  "+logfile
                        #print CMD
                        os.system(CMD)
            #-----------------------------------------------------------------------------------
            # Delete the 3D binary files (to save space)
            #-----------------------------------------------------------------------------------
            if CLEAR == True:
                CMD = "rm -f "+SCR_DIR+"OUT_3D/*"+CASE_TAIL+"*com3D"
                print CMD
                os.system(CMD)
            #-----------------------------------------------------------------------------------
            # Move to permanant output directory
            #-----------------------------------------------------------------------------------
            if MOVE == True:
                logfile = LOG_DIR+"mv."+CASE+".out"
                CMD = 'ncl \'case_stub="'+CASENAME+'"\' \'case_tail="'+CASE_TAIL+'"\'  '+SAM_DIR+'../move_data_LP.ncl  > '+logfile
                print
                print CMD
                os.system(CMD)
            print
            #-----------------------------------------------------------------------------------
            # Process the output
            #if POST == True:
            #   CMD = SAM_DIR+"postprocess_case.sh "+CASE_TAIL+"  > "+LOG_DIR+"post."+CASE_TAIL+".out"
            #   print CMD
            #   os.system(CMD)
            #
            #-----------------------------------------------------------------------------------
            # Delete the 3D binary files (to save space)
            #if POST == True:
            #   CMD = "rm -f "+SAM_DIR+"OUT_3D/*"+CASE_TAIL+"*com3D"
            #   print CMD
            #   os.system(CMD)
            #
            #-----------------------------------------------------------------------------------
            # Move to permanant output directory
            #if POST == True:
            #   CMD = 'ncl \'case_stub="BUBBLE"\' \'case_tail="'+CASE_TAIL+'"\'  '+SAM_DIR+'../move_data_LP.ncl'
            #   print CMD
            #   os.system(CMD)
print
#========================================================================================================================
#========================================================================================================================


# 1  0 'QTSTORCLD'    'Total water storage','g/kg/day'
# 1  0 'QTSTORCOR'    'Total water storage','g/kg/day'
# 1  0 'QTSTORCDN'    'Total water storage','g/kg/day'
# 1  0 'QTSTORSUP'    'Total water storage','g/kg/day'
# 1  0 'QTSTORSDN'    'Total water storage','g/kg/day'
# 1  0 'QTSTORENV'    'Total water storage','g/kg/day'

# 1  0 'QTSINKCLD'    'Rain/Snow Source due to rain evaporation','g/kg/day'
# 1  0 'QTSINKCOR'    'Rain/Snow Source due to rain evaporation','g/kg/day'
# 1  0 'QTSINKCDN'    'Rain/Snow Source due to rain evaporation','g/kg/day'
# 1  0 'QTSINKSUP'    'Rain/Snow Source due to rain evaporation','g/kg/day'
# 1  0 'QTSINKSDN'    'Rain/Snow Source due to rain evaporation','g/kg/day'
# 1  0 'QTSINKENV'    'Rain/Snow Source due to rain evaporation','g/kg/day'

# 1  0 'QTDIFFCLD'    'Diffusive Transport of QT','g/kg/day'
# 1  0 'QTDIFFCOR'    'Diffusive Transport of QT','g/kg/day'
# 1  0 'QTDIFFCDN'    'Diffusive Transport of QT','g/kg/day'
# 1  0 'QTDIFFSUP'    'Diffusive Transport of QT','g/kg/day'
# 1  0 'QTDIFFSDN'    'Diffusive Transport of QT','g/kg/day'
# 1  0 'QTDIFFENV'    'Diffusive Transport of QT','g/kg/day'
