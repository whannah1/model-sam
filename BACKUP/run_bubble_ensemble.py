#!/usr/bin/env python
#========================================================================================================================
# 	This script runs an ensemble of SAM runs with various initial	
# 	anomaly values for a bubble at the center of the domain		
#
#    Jul, 2013	Walter Hannah 		Colorado State University
#========================================================================================================================
import datetime
import sys
import os
import numpy as np
#========================================================================================================================
#========================================================================================================================

RUN   = True
POST  = True
MOVE  = True
CLEAR = False

CASENAME = "BUBBLE"

shear_ideal = False

umax = [5]		# max u-wind value for idealized shear profile
#umax = [2,4,6]	# max u-wind value for idealized shear profile

qsig = 1.0		# factor to increase moisture (in std. deviations)
tsig = 0.0		# factor to increase temperature (in std. deviations)

#nx = 180		# number of points in the x & y directions
#nz = 220		# number of vertical levels
#dx = 100		# Horizontal resolution [m]
#dt =   1		# timestep		[s]

nx = 144		# number of points in the x & y directions
nz = 128		# number of vertical levels
dx = 200		# Horizontal resolution [m]
dt =   2		# timestep		[s]


#BUB_DX	= [1,2,3,4,5]
#BUB_DX	= [1.5,2.5,3.5,4.5,5.5]
BUB_DX	= [3.5,4.5,5.5]

#BUB_DT	= [0.5,1.5]
BUB_DT	= [1.0]
BUB_DQ	= [2]

nstop   =  3600/dt
data_dt =  30/dt
start3D =  0
start2D =  0

# quick run setup for debugging
#nstop   =  1*60/dt
#start3D =  nstop+100
#start2D =  nstop+100

SAM_DIR	= "/maloney-scratch/whannah/SAM/6.9.5_ent_LP/"
SAM_EXE = "SAM_ADV_MPDATA_RAD_CAM_MICRO_SAM1MOM_"+str(nx)+"x"+str(nz)
UTL_DIR = SAM_DIR+"UTIL/"
UTL_LPT = SAM_DIR+"UTIL_LPT/"
#========================================================================================================================
#========================================================================================================================
ndx = len(BUB_DX)
ndt = len(BUB_DT)
ndq = len(BUB_DQ)
ndu = len(umax)

with open(SAM_DIR+"CaseName", "w") as CaseName_file:	# make sure CaseName indicates
    CaseName_file.write("BUBBLE\n")			# we are running the BUBBLE case
#========================================================================================================================
#========================================================================================================================
print
print
print "Running SAM BUBBLE ensemble  (",datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),")"
print
print
for x in xrange(ndx):
	for t in range(ndt):
		#for q in range(ndq):
		for u in range(ndu):
			q = 0
			#-----------------------------------------------------------------------------------
			# Set the case name
			#-----------------------------------------------------------------------------------
			#CDX=format(BUB_DX[x],"02.0f")
			CDX = str(BUB_DX[x])
			CDT=str(BUB_DT[t])
			CDQ=str(BUB_DQ[q])
			qsigstr = format(qsig,"01.1f")
			tsigstr = format(tsig,"01.1f")
			shstr   = format(umax[u],"02d")
			CASE_TAIL = "LP_"+str(dx)+"_"+str(nx)+"x"+str(nz)
			if shear_ideal == True:
				CASE_TAIL = CASE_TAIL+"_shear"+shstr	#umax
			CASE_TAIL = CASE_TAIL+"_q"+qsigstr+"_t"+tsigstr
			CASE_TAIL = CASE_TAIL+"_"+CDX+"km_"+CDT+"k_"+CDQ+"g"
			CASE = "BUBBLE_"+CASE_TAIL
			print "  CASE = "+CASE
			#-----------------------------------------------------------------------------------
			# Edit the namelist file
			#-----------------------------------------------------------------------------------
			#ifile = SAM_DIR+"BUBBLE/prm_template_"+str(dx)
			ofile = SAM_DIR+"BUBBLE/prm"
			
			os.system( "sed -i \"s/day0 =.*"		+"/day0      = "		+str("0.0")				+",/\"  "	+ofile )
			os.system( "sed -i \"s/caseid =.*"		+"/caseid = \'"			+CASE_TAIL				+"\',/\"  "	+ofile )
			os.system( "sed -i \"s/nrestart =.*"	+"/nrestart = "			+str(0)					+",/\" "	+ofile )
			os.system( "sed -i 's/dx =.*"			+"/dx = "				+str(dx)				+"./' "		+ofile )
			os.system( "sed -i 's/dy =.*"			+"/dy = "				+str(dx)				+"./' "		+ofile )
			os.system( "sed -i 's/dt =.*"			+"/dt = "				+str(dt)				+"/'  "		+ofile )
			os.system( "sed -i 's/nrad .*"			+"/nrad      = " 		+str(3*60/dt)			+"/'  "		+ofile )
			os.system( "sed -i 's/nstop.*"			+"/nstop     = "		+str(nstop)				+"/'  "		+ofile )
			os.system( "sed -i 's/nprint .*"		+"/nprint    = " 		+str(nstop)				+"/'  "		+ofile )
			os.system( "sed -i 's/nstatfrq .*"		+"/nstatfrq  = " 		+str(data_dt)			+"/'  "		+ofile )
			os.system( "sed -i 's/nstat .*"			+"/nstat     = " 		+str(data_dt)			+"/'  "		+ofile )
			os.system( "sed -i 's/nsave2D .*"		+"/nsave2D        = " 	+str(data_dt)			+"/'  "		+ofile )
			os.system( "sed -i 's/nsave3D .*"		+"/nsave3D        = " 	+str(data_dt)			+"/'  "		+ofile )
			os.system( "sed -i 's/nent3D .*"		+"/nent3D         = "   +str(data_dt)			+"/'  "		+ofile )
			os.system( "sed -i 's/nsave2Dstart .*"	+"/nsave2Dstart   = "	+str(start2D)			+"/'  "		+ofile )
			os.system( "sed -i 's/nsave3Dstart .*"	+"/nsave3Dstart   = "	+str(start3D)			+"/'  "		+ofile )
			os.system( "sed -i 's/nent3Dstart .*"	+"/nent3Dstart    = "  	+str(start3D)			+"/'  "		+ofile )
			os.system( "sed -i 's/bubble_x0 .*"		+"/bubble_x0         = "+str(nx*dx/2)			+"/'  "		+ofile )
			os.system( "sed -i 's/bubble_y0 .*"		+"/bubble_y0         = "+str(nx*dx/2)			+"/'  "		+ofile )
			os.system( "sed -i 's/bubble_z0 .*"		+"/bubble_z0         = "+str(500)				+"/'  "		+ofile )
			os.system( "sed -i 's/bubble_dtemp .*"	+"/bubble_dtemp      = "+str(BUB_DT[t])			+"/'  "		+ofile )
			os.system( "sed -i 's/bubble_dq .*"		+"/bubble_dq         = "+str(BUB_DQ[q]/1000.)	+"/'  "	+ofile )
			
			#with open(ofile, 'w') as file:
			#    file.writelines( data )
			#-----------------------------------------------------------------------------------
			# Run the Case
			#-----------------------------------------------------------------------------------
			
			# Specify snd file based on shear flag
			os.system("rm -f "+SAM_DIR+"BUBBLE/snd ")
			if shear_ideal == True:
				snd_file = "BUBBLE/snd.shear_"+shstr+"_ideal"
			else:
				snd_file = "BUBBLE/snd.no_wind"
			snd_file = snd_file+".qsig_"+qsigstr+".tsig_"+tsigstr
			print "  snd  = "+snd_file
			snd_file = SAM_DIR+snd_file
			CMD = "cp "+snd_file+"  "+SAM_DIR+"BUBBLE/snd"
			print CMD
			os.system(CMD)
			
			# Specify grd file based on dx
			#grd_file = "BUBBLE/grd_"+str(nz)+"_"+str(dx)+"dx"
			grd_file = "BUBBLE/grd_"+str(dx)+"dx"
			print "  grd  = "+grd_file
			grd_file = SAM_DIR+grd_file
			CMD = "cp "+grd_file+"  "+SAM_DIR+"BUBBLE/grd"
			print CMD
			os.system(CMD)
			
			if RUN == True:
				print datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
				#CMD = "mpirun -n 36 ./"+SAM_EXE+"_B"+str(BUB_DX[x])+"k  >  "+SAM_DIR+"TERM.out/"+CASE+".out"
				CMD = "mpirun -n 36 ./"+SAM_EXE+"_B"+CDX+"km  >  "+SAM_DIR+"TERM.out/"+CASE+".out"
				print CMD
				os.system(CMD)
				
				print
				print datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
				print "Finished running case: "+CASE+"  ( "+SAM_DIR+"TERM.out/"+CASE+".out )"
			#----------------------------------------------scratch-------------------------------------
			# Process the output
			#-----------------------------------------------------------------------------------
			if POST == True:
				print "Post-processing "+CASE+" (",datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),")"
				print
				print
				#--------------------------------------
				# STAT output
				#--------------------------------------
				for f in os.listdir(SAM_DIR+"OUT_STAT/"):
					if f.endswith(".stat"): 
						CMD = UTL_DIR+"stat2nc "+SAM_DIR+"OUT_STAT/"+f+"  >  "+SAM_DIR+"TERM.out/post."+CASE+".stat.out"
						print CMD
						os.system(CMD)
				#--------------------------------------
				# LPT output
				#--------------------------------------
				for f in os.listdir(SAM_DIR+"OUT_LPT/"):
					if f.endswith(".bin"): 
						CMD = UTL_LPT+"bin2nc_lpt "+SAM_DIR+"OUT_LPT/"+f+"  >  "+SAM_DIR+"TERM.out/post."+CASE+".lpt.out"
						print CMD
						os.system(CMD)
				#--------------------------------------
				# 2D output
				#--------------------------------------
				for f in os.listdir(SAM_DIR+"OUT_2D/"):
					if f.endswith(".2Dcom"): 
						CMD = UTL_DIR+"2Dcom2nc "+SAM_DIR+"OUT_2D/"+f+"  >  "+SAM_DIR+"TERM.out/post."+CASE+".2D.out"
						print CMD
						os.system(CMD)
				#--------------------------------------
				# 3D output (normal & tet. entrainment)
				#--------------------------------------
				logfile = SAM_DIR+"TERM.out/post."+CASE+".3D.out"
				os.system("rm "+logfile)
				os.system("touch "+logfile)
				print
				print "  3D output log file: "+logfile
				for f in os.listdir(SAM_DIR+"OUT_3D/"):
					if f.endswith(".com3D") and not os.path.isfile(SAM_DIR+"OUT_SAM_DIR3D/"+f.replace(".com3D",".nc")) and (CASE in f):
						CMD = UTL_DIR+"com3D2nc "+SAM_DIR+"OUT_3D/"+f+"  >>  "+logfile
						#print CMD
						os.system(CMD)
					if f.endswith(".com3D") and not os.path.isfile(SAM_DIR+"OUT_3D/"+f.replace(".com3D",".nc")) and ("ENTRAIN" in f):
						CMD = UTL_DIR+"old_com3D2nc "+SAM_DIR+"OUT_3D/"+f+"  >>  "+logfile
						#print CMD
						os.system(CMD)
			#-----------------------------------------------------------------------------------
			# Delete the 3D binary files (to save space)
			#-----------------------------------------------------------------------------------
			if CLEAR == True:
				CMD = "rm -f "+SAM_DIR+"OUT_3D/*"+CASE_TAIL+"*com3D"
				print CMD
				os.system(CMD)
			#-----------------------------------------------------------------------------------
			# Move to permanant output directory
			#-----------------------------------------------------------------------------------
			if MOVE == True:
				logfile = SAM_DIR+"TERM.out/mv."+CASE+".out"
				CMD = 'ncl \'case_stub="'+CASENAME+'"\' \'case_tail="'+CASE_TAIL+'"\'  '+SAM_DIR+'../move_data_LP.ncl  > '+logfile
				print
				print CMD
				os.system(CMD)
			print
			#-----------------------------------------------------------------------------------
			# Process the output
			#if POST == True:
			#	CMD = SAM_DIR+"postprocess_case.sh "+CASE_TAIL+"  > "+SAM_DIR+"TERM.out/post."+CASE_TAIL+".out"
			#	print CMD
			#	os.system(CMD)
			#
			#-----------------------------------------------------------------------------------
			# Delete the 3D binary files (to save space)
			#if POST == True:
			#	CMD = "rm -f "+SAM_DIR+"OUT_3D/*"+CASE_TscratchAIL+"*com3D"
			#	print CMD
			#	os.system(CMD)
			#
			#-----------------------------------------------------------------------------------
			# Move to permanant output directory
			#if POST == True:
			#	CMD = 'ncl \'case_stub="BUBBLE"\' \'case_tail="'+CASE_TAIL+'"\'  '+SAM_DIR+'../move_data_LP.ncl'
			#	print CMD
			#	os.system(CMD)
print
#========================================================================================================================
#========================================================================================================================


