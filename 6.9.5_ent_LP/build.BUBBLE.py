#!/usr/bin/env python
#========================================================================================================================
# 	This script builds an ensemble of SAM executables	
#
#    Jul, 2013	Walter Hannah 		Colorado State University
#========================================================================================================================
import datetime
import sys
import os
import numpy as np
#========================================================================================================================
#========================================================================================================================
TOP_DIR = "/raid601/Model/SAM/scratch/"
SRC_DIR = "/home/whannah/Model/SAM/6.9.5_ent_LP/SRC/"

# nx = 128	# number of points in the x & y directions
# nz = 128	# number of vertical levels

nx = 256	# number of points in the x & y directions
nz = 256	# number of vertical levels

#nsub = 6 	# number of subdomains for parallel
nsubx = 4
nsuby = 4


# BUB_DX = [10,20,30,40]
# BUB_DX = list(range(10,52,2))
# BUB_DX = [float(i)/1e1 for i in BUB_DX]

# BUB_DX = list(range(10,50,2))
# BUB_DX = [float(i)/1e1 for i in BUB_DX]
#BUB_DX.remove(1.0)
#BUB_DX.remove(2.0)
#BUB_DX.remove(3.0)
#BUB_DX.remove(4.0)

# BUB_DX = [4.1]
BUB_DX = [2.0,4.0]
#========================================================================================================================
#========================================================================================================================
print
print
print "Building SAM executables  (",datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),")"
print
print
ndx = len(BUB_DX)
for x in xrange(ndx):
	CDX=format(BUB_DX[x],"02.1f")
	print "========================================================================================================="
	print "Editing Bubble size parameters in params.f90"
	CMD = "sed -i \'s/bubble_radius_hor =.*"+"/bubble_radius_hor = "+str(BUB_DX[x]*1000.)+"/\'  "+SRC_DIR+"params.f90"
	print "    "+CMD
	os.system( CMD )
	CMD = "sed -i \'s/bubble_radius_ver =.*"+"/bubble_radius_ver = 1500.0/\'  "+SRC_DIR+"params.f90"
	print "    "+CMD
	os.system( CMD )
	print "========================================================================================================="
	print "Editing executable name in Makefile"
	CMD = "sed -i \'s/SAM = SAM_$(ADV_DIR)_$(RAD_DIR)_$(MICRO_DIR).*"+"/SAM = SAM_$(ADV_DIR)_$(RAD_DIR)_$(MICRO_DIR)_"+str(nx)+"x"+str(nz)+"_B"+str(BUB_DX[x])+"km/\'  "+"./Makefile"
	print "    "+CMD
	os.system( CMD )
	print "========================================================================================================="
	print "Editing domain.f90"
	CMD = "sed -i \'s/YES3D =.*"+"/YES3D = 1  ! Domain dimensionality: 1 - 3D, 0 - 2D/\'  "+SRC_DIR+"domain.f90"
	print "    "+CMD
	os.system( CMD )
	CMD = "sed -i \'s/nx_gl =.*"+"/nx_gl = "+str(nx)+"/\'  "+SRC_DIR+"domain.f90"
	print "    "+CMD
	os.system( CMD )
	CMD = "sed -i \'s/ny_gl =.*"+"/ny_gl = "+str(nx)+"/\'  "+SRC_DIR+"domain.f90"
	print "    "+CMD
	os.system( CMD )
	CMD = "sed -i \'s/nz_gl =.*"+"/nz_gl = "+str(nz)+"/\'  "+SRC_DIR+"domain.f90"
	print "    "+CMD
	os.system( CMD )
	CMD = "sed -i \'s/ntracers =.*/ntracers = 1/\' "+SRC_DIR+"domain.f90"
	print "    "+CMD
	os.system( CMD )
	CMD = "sed -i \'s/nsubdomains_x.*"+"/nsubdomains_x  = "+str(nsubx)+"/\'  "+SRC_DIR+"domain.f90"
	print "    "+CMD
	os.system( CMD )
	CMD = "sed -i \'s/nsubdomains_y.*"+"/nsubdomains_y  = "+str(nsuby)+"/\'  "+SRC_DIR+"domain.f90"
	print "    "+CMD
	os.system( CMD )
	print "========================================================================================================="
	print "Clearing OBJ directory"
	CMD = "rm -f "+TOP_DIR+"OBJ/*"
	print "    "+CMD
	os.system(CMD)
	print "Building SAM..."
	#CMD = "nohup ./Build > logs/build_bubble."+CDX+".out"
	CMD = "./Build"
	print "    "+CMD
	os.system(CMD)
	print "========================================================================================================="
	print "========================================================================================================="


#========================================================================================================================
#========================================================================================================================
