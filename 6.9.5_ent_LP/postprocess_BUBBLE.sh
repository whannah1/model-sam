#! /bin/bash

	TOP_DIR='/maloney-scratch/whannah/SAM/6.9.5_ent_LP'
	UTL_DIR=$TOP_DIR'/UTIL'
	CASE='BUBBLE_'$1

#=================================================
# 3D output
#=================================================
  OUT_DIR=$TOP_DIR'/OUT_3D'
  SCRIPT='com3D2nc'
  for f in $OUT_DIR/$CASE*.com3D
  do
    if [ ! -e ${f:0:$(expr ${#f} - 5)}"nc" ]
    then
      $UTL_DIR'/'$SCRIPT $f
    fi
  done

  OUT_DIR=$TOP_DIR'/OUT_3D'
  SCRIPT='old_com3D2nc'
  for f in $OUT_DIR/*ENTRAIN*$1*com3D
  do
    if [ ! -e ${f:0:$(expr ${#f} - 5)}"nc" ]
    then
      $UTL_DIR'/'$SCRIPT $f
    fi
  done
#=================================================
# 2D output
#=================================================
  OUT_DIR=$TOP_DIR'/OUT_STAT'
  SCRIPT='stat2nc'
  for f in $OUT_DIR/$CASE*.stat
  do
    $UTL_DIR'/'$SCRIPT $f
  done
#=================================================
# Statistics file
#=================================================
  OUT_DIR=$TOP_DIR'/OUT_2D'
  SCRIPT='2Dcom2nc'
  for f in $OUT_DIR/$CASE*.2Dcom*
  do
    $UTL_DIR'/'$SCRIPT $f
    echo $UTL_DIR'/'$SCRIPT $f
  done
#=================================================
# LPTM output
#=================================================
  OUT_DIR=$TOP_DIR'/OUT_LPT'
  SCRIPT='bin2nc_lpt'
  for f in $OUT_DIR/$CASE*.bin
  do
    $TOP_DIR'/UTIL_LPT/'$SCRIPT $f
  done
#=================================================
#=================================================
echo 'all done!'
