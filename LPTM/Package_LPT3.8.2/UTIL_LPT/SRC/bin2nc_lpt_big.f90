PROGRAM bin2nc_lpt_big

! This program converts LPT output file from the binary format to the netcdf format.
! This program also divide output netcdf file if the size of output is larger than 2 GB.
! This program only works with nhistory = 1.
! This program has not been tested with nhistory > 1.
! This program allocates an array with npxnt elements.

	IMPLICIT NONE
	INCLUDE 'netcdf.inc'
	
	INTEGER :: nsubdomains, np, nv0, nv1, nv, nhistory, nt, npsub
	REAL :: xwidth, ywidth, ztop, zbottom, day
	CHARACTER (LEN=80), ALLOCATABLE, DIMENSION(:) :: var_name
	CHARACTER (LEN=10), ALLOCATABLE, DIMENSION(:) :: var_shrt
	CHARACTER (LEN=10), ALLOCATABLE, DIMENSION(:) :: var_unit
	REAL, ALLOCATABLE, DIMENSION(:) :: time, tmp_r
	INTEGER, ALLOCATABLE, DIMENSION(:) :: tags, tmp_i
	INTEGER, ALLOCATABLE, DIMENSION(:,:) :: nvis
	REAL, ALLOCATABLE, DIMENSION(:,:,:) :: vars
	REAL, ALLOCATABLE, DIMENSION(:,:) :: buff
	
	! netCDF stuff
	REAL :: datasize
	INTEGER :: nfile, nvsub
	CHARACTER(LEN=2) :: ifilechar
	CHARACTER(LEN=200) :: filename
	INTEGER :: ncid, err, xyzid, pid, tid, varid
	INTEGER, DIMENSION(2) :: vdimids
	
	! Other
	INTEGER :: i, j, k, n, n1, n2, m, tmp
	
	! External functions
	INTEGER, EXTERNAL :: iargc
	
	
	! Check if input file is appropriate
	i = iargc()
	IF (i == 0) THEN
		PRINT*,'No input file name is specified.'
		PRINT*,'Format: bin2nc_lpt_big input_LPT.bin'
		stop
	ENDIF
	CALL getarg(1,filename)
	
	
	! Open and read header
	OPEN(123, FILE=TRIM(filename), STATUS='old', FORM='unformatted')
	READ(123) nsubdomains, np, nv0, nv1, nv, nhistory, xwidth, ywidth, ztop, zbottom
	PRINT*,'np, nv0, nv1, nv, nhistory, xwidth, ywidth, ztop, zbottom'
	PRINT*, np, nv0, nv1, nv, nhistory, xwidth, ywidth, ztop, zbottom
	ALLOCATE(var_name(0:nv), var_shrt(0:nv), var_unit(0:nv))
	READ(123) var_name, var_shrt, var_unit
	DO i = 0, nv
		PRINT*, var_shrt(i),' ',var_unit(i),' ',trim(var_name(i))
	ENDDO
	
	
	! Find nt
	IF (nhistory == -1) THEN   ! one file for all data
		i = 1
		DO WHILE(i >= 0)
			READ(123,IOSTAT=i) nt, day
			IF (i < 0) EXIT
			DO j = 1, nsubdomains
				READ(123)   ! tag
				READ(123)   ! nvi
				DO k = 1, nv
					READ(123)   ! variables
				ENDDO
			ENDDO
		ENDDO
		REWIND(123)   ! back to file top
		READ(123)   ! nsubdomains, np, nv0, ...
		READ(123)   ! var_name, ...
	ELSE IF (nhistory == 1) THEN   ! one time record in one file
		nt = nhistory
	ELSE   ! nhistory time records. Number of data in the last file can be less than nhistory.
		READ(123) nt, day
		REWIND(123)
		READ(123)
		READ(123)
		n = nt / nhistory   ! nt can be more than 1
		i = 1
		DO WHILE(i >= 0)
			READ(123,IOSTAT=i) nt, day
			IF (i < 0) EXIT
			DO j = 1, nsubdomains
				READ(123)   ! tag
				READ(123)   ! nvi
				DO k = 1, nv
					READ(123)   ! variables
				ENDDO
			ENDDO
		ENDDO
		REWIND(123)   ! back to file top
		READ(123)   ! nsubdomains, np, nv0, ...
		READ(123)   ! var_name, ...
		nt = nt - n * nhistory
	ENDIF
	
	
	! Read time, tag, nvi
	npsub = np / nsubdomains
	ALLOCATE(time(nt), tags(np), nvis(np,nt), tmp_i(npsub))
	time(:) = 0.
	tags(:) = 0
	nvis(:,:) = 0
	DO k = 1, nt
		READ(123) tmp, day
		time(k) = day
		DO j = 1, nsubdomains
			IF ( k == 1 ) THEN
				tmp_i(:) = 0
				READ(123) tmp_i   ! tag
				tags(1+(j-1)*npsub:npsub*j) = tmp_i(:)
			ELSE
				READ(123)
			ENDIF
			tmp_i(:) = 0
			READ(123) tmp_i   ! nvi
			nvis(1+(j-1)*npsub:npsub*j,k) = tmp_i(:)
			DO i = 1, nv
				READ(123)   ! variables
			ENDDO
		ENDDO
	ENDDO
	REWIND(123)   ! back to file top
	READ(123)   ! nsubdomains, np, nv0, ...
	READ(123)   ! var_name, ...
	PRINT*,'time (day)', time
	PRINT*,'tags(upto 50):', tags(1:min(np,50))
	
	
	! Convert bin to netCDF
	! How many output files?
	datasize = REAL(np) * REAL(nt) * REAL(nv+2) * 4. / (1024.**3)   ! total data size if one file, GB
		! +2 of nv+2 is for tag & nvi
	
	IF ( datasize >= 2. ) THEN
		! Multiple output file
		nfile = CEILING( datasize / ( 2. - REAL(np) * 4. / (1024.**3) ) )
			! denominator is smaller than 2 GB because each file contains tag.
		IF ( nfile > 10 ) PRINT*,'nfile has to be less than 10. Modify the program.'
		PRINT*,'# of output file will be created:', nfile
		
		nvsub = CEILING( REAL(nv+1) / REAL(nfile) ) ! number of variable in each file
		
		DO m = 1, nfile
		
			! Output filename
			IF ( m == 1 ) THEN
				WRITE(ifilechar,'(I2.2)') m
				DO i = 1, 195
					IF (filename(i:i+5) == '.bin  ') THEN
						filename(i:i+5) = '_'//ifilechar//'.nc'
						EXIT
					ELSE IF (i == 195) THEN
						PRINT*,'Wrong filename extension!'
						STOP
					ENDIF
				ENDDO
			ELSE
				DO i = 1, 195
					IF (filename(i:i+5) == '_'//ifilechar//'.nc') THEN
						WRITE(ifilechar,'(I2.2)') m
						filename(i:i+5) = '_'//ifilechar//'.nc'
						EXIT
					ELSE IF (i == 195) THEN
						PRINT*,'Wrong filename extension!'
						STOP
					ENDIF
				ENDDO
			ENDIF
			PRINT*,TRIM(filename)
			
			! Initialize netCDF stuff
			err = NF_CREATE(TRIM(filename), NF_CLOBBER, ncid)
			err = NF_REDEF(ncid)
			
			! Define independent variables
			err = NF_DEF_DIM(ncid, 'np', np, pid)
			err = NF_DEF_VAR(ncid, 'tag', NF_INT, 1, pid, varid)
			err = NF_PUT_ATT_TEXT(ncid, varid, 'units', 3, '[ ]')
			err = NF_PUT_ATT_TEXT(ncid, varid, 'long_name', 19, 'number of perticles')
			
			err = NF_DEF_DIM(ncid, 'nt', nt, tid)
			err = NF_DEF_VAR(ncid, 'time', NF_FLOAT, 1, tid, varid)
			err = NF_PUT_ATT_TEXT(ncid, varid, 'units', 5, '[day]')
			
			err = NF_DEF_DIM(ncid, 'xyz', 1, xyzid)
			err = NF_DEF_VAR(ncid, 'xwidth', NF_FLOAT, 1, xyzid, varid)
			err = NF_PUT_ATT_TEXT(ncid, varid, 'units', 3, '[m]')
			err = NF_PUT_ATT_TEXT(ncid, varid, 'long_name', 27, 'Domain width in x direction')
			
			err = NF_DEF_VAR(ncid, 'ywidth', NF_FLOAT, 1, xyzid, varid)
			err = NF_PUT_ATT_TEXT(ncid, varid, 'units', 3, '[m]')
			err = NF_PUT_ATT_TEXT(ncid, varid, 'long_name', 27, 'Domain width in y direction')
			
			err = NF_DEF_VAR(ncid, 'ztop', NF_FLOAT, 1, xyzid, varid)
			err = NF_PUT_ATT_TEXT(ncid, varid, 'units', 3, '[m]')
			err = NF_PUT_ATT_TEXT(ncid, varid, 'long_name', 22, 'Highest altitude limit')
			
			err = NF_DEF_VAR(ncid, 'zbottom', NF_FLOAT, 1, xyzid, varid)
			err = NF_PUT_ATT_TEXT(ncid, varid, 'units', 3, '[m]')
			err = NF_PUT_ATT_TEXT(ncid, varid, 'long_name', 21, 'Lowest altitude limit')
			
			err = NF_ENDDEF(ncid)
			
			! Write
			err = NF_INQ_VARID(ncid, 'tag', varid)
			err = NF_PUT_VAR_INT(ncid, varid, tags)
			err = NF_INQ_VARID(ncid, 'time', varid)
			err = NF_PUT_VAR_REAL(ncid, varid, time)	
			
			err = NF_INQ_VARID(ncid, 'xwidth', varid)
			err = NF_PUT_VAR_REAL(ncid, varid, xwidth)
			
			err = NF_INQ_VARID(ncid, 'ywidth', varid)
			err = NF_PUT_VAR_REAL(ncid, varid, ywidth)
			
			err = NF_INQ_VARID(ncid, 'ztop', varid)
			err = NF_PUT_VAR_REAL(ncid, varid, ztop)
			
			err = NF_INQ_VARID(ncid, 'zbottom', varid)
			err = NF_PUT_VAR_REAL(ncid, varid, zbottom)
			
			vdimids(1) = pid
			vdimids(2) = tid
			
			IF ( m == 1 ) THEN
				! nvi
				PRINT*, var_shrt(0),' ',trim(var_name(0)),' ',var_unit(0)
				PRINT*,'(upto 5 steps for tag 1):', nvis(1,:)
				err = NF_REDEF(ncid)
				err = NF_DEF_VAR(ncid, TRIM(var_shrt(0)), NF_INT, 2, vdimids, varid)
				err = NF_PUT_ATT_TEXT(ncid, varid, 'long_name',LEN_TRIM(var_name(0)),TRIM(var_name(0)) )
				err = NF_PUT_ATT_TEXT(ncid, varid, 'units', LEN_TRIM(var_unit(0)), TRIM(var_unit(0)) )
				err = NF_ENDDEF(ncid)
				err = NF_PUT_VAR_INT(ncid, varid, nvis)
				DEALLOCATE(nvis, tmp_i)
				n1 = 1
				n2 = nvsub-1
			ELSE
				n1 = n2 + 1
				IF ( m < nfile ) THEN
					n2 = n1 + nvsub
				ELSE
					n2 = nv
				ENDIF
			ENDIF
			
			! variables
			ALLOCATE(vars(np,nt,n1:n2), tmp_r(npsub), buff(np,nt))
			! read
			vars(:,:,:) = 0.
			DO k = 1, nt
				READ(123)   ! nt, day
				DO j = 1, nsubdomains
					READ(123)   ! tag
					READ(123)   ! nvi
					DO i = 1, nv
						IF ( i >= n1 .AND. i <= n2 ) THEN
							tmp_r(:) = 0.
							READ(123) tmp_r 
							vars(1+(j-1)*npsub:npsub*j,k,i) = tmp_r(:)
						ELSE
							READ(123)
						ENDIF
					ENDDO
				ENDDO
			ENDDO
			rewind(123)   ! back to file top
			READ(123)   ! nsubdomains, np, nv0, ...
			READ(123)   ! var_name, ...
			! write
			DO n = n1, n2
				buff(:,:) = 0.
				buff(:,:) = vars(:,:,n)
				PRINT*, var_shrt(n),' ',TRIM(var_name(n)),' ',var_unit(n)
				PRINT*,'(upto 5 steps for tag 1):', buff(1,:)
				err = NF_REDEF(ncid)
				err = NF_DEF_VAR(ncid, TRIM(var_shrt(n)), NF_FLOAT, 2, vdimids, varid)
				err = NF_PUT_ATT_TEXT(ncid, varid, 'long_name',LEN_TRIM(var_name(n)),TRIM(var_name(n)) )
				err = NF_PUT_ATT_TEXT(ncid, varid, 'units', LEN_TRIM(var_unit(n)), TRIM(var_unit(n)) )
				err = NF_ENDDEF(ncid)
				err = NF_PUT_VAR_REAL(ncid, varid, buff)
			ENDDO
			DEALLOCATE(vars, tmp_r, buff)
			
			! Close output file
			err = NF_CLOSE(ncid)
			
		ENDDO
		
	ELSE IF ( datasize > 0 .AND. datasize < 2. ) THEN
		! One output file
		! Output filename
		DO i = 1, 197
			IF (filename(i:i+3) == '.bin') THEN
				filename(i:i+3) = '.nc '
				EXIT
			ELSE if(i == 198) THEN
				PRINT*,'Wrong filename extension!'
				STOP
			ENDIF
		ENDDO
		PRINT*,TRIM(filename)
	
		!	Initialize netCDF stuff
		err = NF_CREATE(TRIM(filename), NF_CLOBBER, ncid)
		err = NF_REDEF(ncid)
	
		!	Define independent variables
		err = NF_DEF_DIM(ncid, 'np', np, pid)
		err = NF_DEF_VAR(ncid, 'tag', NF_INT, 1, pid, varid)
		err = NF_PUT_ATT_TEXT(ncid, varid, 'units', 3, '[ ]')
		err = NF_PUT_ATT_TEXT(ncid, varid, 'long_name', 19, 'number of perticles')
	
		err = NF_DEF_DIM(ncid, 'nt', nt, tid)
		err = NF_DEF_VAR(ncid, 'time', NF_FLOAT, 1, tid, varid)
		err = NF_PUT_ATT_TEXT(ncid, varid, 'units', 5, '[day]')
	
		err = NF_DEF_DIM(ncid, 'xyz', 1, xyzid)
		err = NF_DEF_VAR(ncid, 'xwidth', NF_FLOAT, 1, xyzid, varid)
		err = NF_PUT_ATT_TEXT(ncid, varid, 'units', 3, '[m]')
		err = NF_PUT_ATT_TEXT(ncid, varid, 'long_name', 27, 'Domain width in x direction')
	
		err = NF_DEF_VAR(ncid, 'ywidth', NF_FLOAT, 1, xyzid, varid)
		err = NF_PUT_ATT_TEXT(ncid, varid, 'units', 3, '[m]')
		err = NF_PUT_ATT_TEXT(ncid, varid, 'long_name', 27, 'Domain width in y direction')
	
		err = NF_DEF_VAR(ncid, 'ztop', NF_FLOAT, 1, xyzid, varid)
		err = NF_PUT_ATT_TEXT(ncid, varid, 'units', 3, '[m]')
		err = NF_PUT_ATT_TEXT(ncid, varid, 'long_name', 22, 'Highest altitude limit')
	
		err = NF_DEF_VAR(ncid, 'zbottom', NF_FLOAT, 1, xyzid, varid)
		err = NF_PUT_ATT_TEXT(ncid, varid, 'units', 3, '[m]')
		err = NF_PUT_ATT_TEXT(ncid, varid, 'long_name', 21, 'Lowest altitude limit')
		
		err = NF_ENDDEF(ncid)
	
		!	Write
		err = NF_INQ_VARID(ncid, 'tag', varid)
		err = NF_PUT_VAR_INT(ncid, varid, tags)
		err = NF_INQ_VARID(ncid, 'time', varid)
		err = NF_PUT_VAR_REAL(ncid, varid, time)	
	
		err = NF_INQ_VARID(ncid, 'xwidth', varid)
		err = NF_PUT_VAR_REAL(ncid, varid, xwidth)
	
		err = NF_INQ_VARID(ncid, 'ywidth', varid)
		err = NF_PUT_VAR_REAL(ncid, varid, ywidth)
	
		err = NF_INQ_VARID(ncid, 'ztop', varid)
		err = NF_PUT_VAR_REAL(ncid, varid, ztop)
	
		err = NF_INQ_VARID(ncid, 'zbottom', varid)
		err = NF_PUT_VAR_REAL(ncid, varid, zbottom)
	
		vdimids(1) = pid
		vdimids(2) = tid
		
		!	nvi
		PRINT*, var_shrt(0),' ',TRIM(var_name(0)),' ',var_unit(0)
		PRINT*,'(upto 5 steps for tag 1):', nvis(1,:)
		err = NF_REDEF(ncid)
		err = NF_DEF_VAR(ncid, TRIM(var_shrt(0)), NF_INT, 2, vdimids, varid)
		err = NF_PUT_ATT_TEXT(ncid, varid, 'long_name', LEN_TRIM(var_name(0)), TRIM(var_name(0)) )
		err = NF_PUT_ATT_TEXT(ncid, varid, 'units', LEN_TRIM(var_unit(0)), TRIM(var_unit(0)) )
		err = NF_ENDDEF(ncid)
		err = NF_PUT_VAR_INT(ncid, varid, nvis)
		DEALLOCATE(nvis, tmp_i)
	
		!	variables
		ALLOCATE(vars(np,nt,nv), tmp_r(npsub), buff(np,nt))
		! read
		vars(:,:,:) = 0.
		DO k = 1, nt
			READ(123)   ! nt, day
			DO j = 1, nsubdomains
				READ(123)   ! tag
				READ(123)   ! nvi
				DO i = 1, nv
					tmp_r(:) = 0.
					READ(123) tmp_r
					vars(1+(j-1)*npsub:npsub*j,k,i) = tmp_r(:)
				ENDDO
			ENDDO
		ENDDO
		! write
		DO n = 1, nv
			buff(:,:) = 0.
			buff(:,:) = vars(:,:,n)
			PRINT*, var_shrt(n),' ',TRIM(var_name(n)),' ',var_unit(n)
			PRINT*,'(upto 5 steps for tag 1):', buff(1,:)
			err = NF_REDEF(ncid)
			err = NF_DEF_VAR(ncid, TRIM(var_shrt(n)), NF_FLOAT, 2, vdimids, varid)
			err = NF_PUT_ATT_TEXT(ncid, varid, 'long_name', LEN_TRIM(var_name(n)), TRIM(var_name(n)) )
			err = NF_PUT_ATT_TEXT(ncid, varid, 'units', LEN_TRIM(var_unit(n)), TRIM(var_unit(n)) )
			err = NF_ENDDEF(ncid)
			err = NF_PUT_VAR_REAL(ncid, varid, buff)
		ENDDO
		DEALLOCATE(vars, tmp_r, buff)
		
		! Close output file
		err = NF_CLOSE(ncid)
		
	ENDIF
	
	! Close raw data file
	CLOSE(123)
	
	! Deallocate
	DEALLOCATE( var_name, var_shrt, var_unit, time, tags )
	
END PROGRAM bin2nc_lpt_big
