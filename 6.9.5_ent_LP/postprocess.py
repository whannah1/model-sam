#!/usr/bin/env python
#========================================================================================================================
#   This script automates the postprocessing and transferring of a SAM run on Yellowstone
#
#    Jun, 2015  Walter Hannah       North Carolina State University
#========================================================================================================================
import datetime
import sys
import os
import numpy as np
home = os.getenv("HOME")
#========================================================================================================================
#========================================================================================================================
POST, CLEAR, MOVE, dostat, dolpt, do2D, do3D = False,False,False,False,False,False,False

#POST  = True
MOVE  = True
#CLEAR = True

dostat  = True
#dolpt   = True
#do2D    = True
#do3D    = True

CASENAME = "RCE"
CASENUM  = "00"

nx = 256        # number of points in the x & y directions
nz = 128        # number of vertical levels
dx = 200        # Horizontal resolution [m]
dt =   2        # timestep      [s]

sst = "301"

SAM_DIR = home+"/Model/SAM/SAM6.10.8_ENT_LP/"       # used to locate processing scripts
#SAM_DIR = home+"/Model/SAM/6.9.5_ent_LP/"
#========================================================================================================================
#========================================================================================================================
nproc = 16#64

#CASE_TAIL = "LP_"+str(dx)+"_"+str(nx)+"x"+str(nz)+"_"+sst+"K"
CASE_TAIL = ""+str(dx)+"_"+str(nx)+"x"+str(nz)+"_"+sst+"K"+"_"+CASENUM
CASE = CASENAME+"_"+CASE_TAIL
print "  CASE = "+CASE

#DATA_DIR = home+"/Data/SAM/"+CASE+"/"
DATA_DIR = "/raid601/Model/SAM/scratch/"

SAM_EXE = "SAM_ADV_MPDATA_SGS_TKE_RAD_CAM_MICRO_SAM1MOM_"+str(nx)+"x"+str(nz)
UTL_DIR = SAM_DIR+"UTIL/"
UTL_LPT = SAM_DIR+"UTIL_LPT/"
LOG_DIR = SAM_DIR+"logs/"

with open(SAM_DIR+"CaseName", "w") as CaseName_file:    # make sure CaseName indicates
    CaseName_file.write("RCE\n")                        # we are running the RCE case
#========================================================================================================================
#========================================================================================================================
print
print "Running SAM RCE 3D (",datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),")"
print

#-----------------------------------------------------------------------------------
# Process the output
#-----------------------------------------------------------------------------------
if POST == True:
    print "Post-processing "+CASE+" (",datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),")"
    print
    print
    #--------------------------------------
    # STAT output
    #--------------------------------------
    if dostat :
        for f in os.listdir(DATA_DIR+"OUT_STAT/"):
            if f.endswith(".stat"): 
                CMD = UTL_DIR+"stat2nc "+DATA_DIR+"OUT_STAT/"+f+"  >  "+LOG_DIR+"post."+CASE+".stat.out"
                print CMD
                os.system(CMD)
    
    print
    #print "Exiting!"
    #exit()
    #--------------------------------------
    # LPT output
    #--------------------------------------
    if dolpt : 
        for f in os.listdir(DATA_DIR+"OUT_LPT/"):
            if f.endswith(".bin"): 
                CMD = UTL_LPT+"bin2nc_lpt "+DATA_DIR+"OUT_LPT/"+f+"  >  "+LOG_DIR+"post."+CASE+".lpt.out"
                print CMD
                os.system(CMD)
    #--------------------------------------
    # 2D output
    #--------------------------------------
    if do2D :
        for f in os.listdir(DATA_DIR+"OUT_2D/"):
            if f.endswith(".2Dcom"): 
                CMD = UTL_DIR+"2Dcom2nc "+DATA_DIR+"OUT_2D/"+f+"  >  "+LOG_DIR+"post."+CASE+".2D.out"
                print CMD
                os.system(CMD)
    #--------------------------------------
    # 3D output (normal & tet. entrainment)
    #--------------------------------------
    if do3D :
        logfile = LOG_DIR+"post."+CASE+".3D.out"
        #os.system("rm "+logfile)
        #os.system("touch "+logfile)
        print
        print "  3D output log file: "+logfile
        TMP_DIR = DATA_DIR+"OUT_3D/"
        for f in os.listdir(TMP_DIR) :
            if f.endswith(".com3D") and not os.path.isfile(TMP_DIR+f.replace(".com3D",".nc")) and (CASE in f):
                CMD = UTL_DIR+"com3D2nc "+TMP_DIR+f+"  >>  "+logfile
                #print CMD
                os.system(CMD)
            if f.endswith(".com3D") and not os.path.isfile(TMP_DIR+f.replace(".com3D",".nc")) and ("ENTRAIN" in f):
                #CMD = UTL_DIR+"old_com3D2nc "+TMP_DIR+f+"  >>  "+logfile
                CMD = UTL_DIR+"com3D2nc_alt "+TMP_DIR+f+"  >>  "+logfile
                #print CMD
                os.system(CMD)
#-----------------------------------------------------------------------------------
# Delete the 3D binary files (to save space)
#-----------------------------------------------------------------------------------
#if CLEAR == True:
#    CMD = "rm -f "+SCR_DIR+"OUT_3D/*"+CASE_TAIL+"*com3D"
#    print CMD
#    os.system(CMD)
#-----------------------------------------------------------------------------------
# Move to permanant output directory (on scylla)
#-----------------------------------------------------------------------------------
if MOVE == True:
    #src_dir = "/glade/u/home/whannah/Model/SAM/"
    #dst_dir = "scylla.meas.ncsu.edu:~/Data/SAM/"+CASE+"/"
    src_dir = "/raid601/Model/SAM/scratch/"
    dst_dir = "/raid601/SAM/"+CASE+"/" 
    
    logfile = LOG_DIR+"mv."+CASE+".out"
    
    #cmd = "ssh whannah@"+server+" \' mkdir "
    cmd = "mkdir "
    print "MOVE: "
    print "    creating output directories...   "
    os.system(cmd+dst_dir+" ")
    os.system(cmd+dst_dir+" ")
    os.system(cmd+dst_dir+"/OUT_2D   ")
    os.system(cmd+dst_dir+"/OUT_3D   ")
    os.system(cmd+dst_dir+"/OUT_STAT ")
    os.system(cmd+dst_dir+"/RESTART  ")
    #os.system("ssh whannah@"+server+" \' mkdir "+odir+"/OUT_LPT  \'")
    
    print "    transferring data...   "
    #cmd = "scp -p "
    cmd = "mv "
    if dostat : os.system(cmd + src_dir+"/OUT_STAT/*" +CASE+"* "    + dst_dir+"OUT_STAT/" )
    if dostat : os.system(cmd + src_dir+"/RESTART/*"  +CASE+"* "    + dst_dir+"RESTART/" )
    if do2D   : os.system(cmd + src_dir+"/OUT_2D/*"   +CASE+"* "    + dst_dir+"OUT_2D/" )
    if dolpt  : os.system(cmd + src_dir+"/OUT_LPT/*"  +CASE+"* "    + dst_dir+"OUT_LPT/" )
    if do3D   : os.system(cmd + src_dir+"/OUT_3D/*"   +CASE+"* "    + dst_dir+"OUT_3D/" )
    if do3D   : os.system(cmd + src_dir+"/OUT_3D/*"   +CASENAME+"*ENTRAIN*"+CASE_TAIL+"* " + dst_dir+"OUT_3D/" )
    
    print "    done."
    print
print

#-----------------------------------------------------------------------------------
# Delete the 3D binary files (to save space)
#if POST == True:
#   CMD = "rm -f "+SAM_DIR+"OUT_3D/*"+CASE_TAIL+"*com3D"
#   print CMD
#   os.system(CMD)
#
#-----------------------------------------------------------------------------------
print
#========================================================================================================================
#========================================================================================================================
