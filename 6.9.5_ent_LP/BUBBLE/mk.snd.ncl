load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/skewt_func.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions_ERAi.ncl"

begin
	sa = "ssa"
	
	odir = "/maloney-scratch/whannah/SAM/6.9.5_ent/BUBBLE/"
	
	ofile = odir+"snd"
	
	fig_type = "png"
	fig_file = odir+"sounding"
	
;======================================================================================
;======================================================================================
      opt  = True
      opt@lat1 = -10.
      opt@lat2 =  10.
      opt@lon1 =   0.
      opt@lon2 = 360.
      opt@yr1  = 2000
      opt@yr2  = 2007
      lat = LoadERAilat(opt)
      lon = LoadERAilon(opt)	
        print("")
        print("  "+ofile)
        print("    lat = 	"+sprintf("%6.4g",min(lat))+"	:  "+sprintf("%6.4g",max(lat)))
        print("    lon = 	"+sprintf("%6.4g",min(lon))+"	:  "+sprintf("%6.4g",max(lon)))
        print("")
;======================================================================================
;======================================================================================
	ps  =                           avg( LoadERAi("Ps" ,opt) )
	print("Loading T")
	T   = dim_avg_n_Wrap(dim_avg_n_Wrap( LoadERAi("T"  ,opt) ,(/0/)),(/1,2/))
	print("Loading Q")
	q   = dim_avg_n_Wrap(dim_avg_n_Wrap( LoadERAi("Q"  ,opt) ,(/0/)),(/1,2/))
	print("Loading Z")
	z   = dim_avg_n_Wrap(dim_avg_n_Wrap( LoadERAi("GEO",opt) ,(/0/)),(/1,2/))
	print("done")
	u   = 0.*T
        v   = 0.*T
        p = T&lev

        Th = T
        Th = (/ T*(1000./p)^(Rd/cpd) /)
        qsat = mixhum_ptrh(p,T,100.+p*0.,2)
        RH = 100.*q/qsat
        dp = dewtemp_trh(T,RH)-273.15
        
        z = z/9.81
        
        ps = ps/100.
        
        q = q*1000.
        
        Tc = T-273.15
print("         p"+"	"+"     T (C)"+"	"+"     dewpt"+"	"+"         q"+"	"+"      qsat"+"	"+"        RH"  )        
print(sprintf("%10.4f",p)+"	"+sprintf("%10.4f",Tc)+"	"+sprintf("%10.4f",dp)+"	"+sprintf("%10.4f",q)+"	"+sprintf("%10.4f",qsat)+"	"+sprintf("%10.4f",RH)  )
;======================================================================================
;======================================================================================
  if True then 
    wks = gsn_open_wks(fig_type,fig_file)
    skewtOpts                 = True
    skewtOpts@tiMainString    = "Tropical Mean"
    dataOpts   = False        
    skewt_bkgd = skewT_BackGround (wks, skewtOpts)
    skewt_data = skewT_PlotData   (wks, skewt_bkgd, p,Tc,dp,z, 0.*p,0.*p, dataOpts)
    draw (skewt_bkgd)
    draw (skewt_data)
    frame(wks)
    
        print("")
    	print("	"+fig_file+"."+fig_type)
    	print("")
  end if
;======================================================================================
; write out to an ascii file
;======================================================================================
  time  = (/0,999/)
  num_t = dimsizes(time)
  num_z = dimsizes(p)
  num_r = num_z+1
  str = new((num_t)*num_r+1,string)
  str(0) = " z[m] p[mb] tp[K] q[g/kg] u[m/s] v[m/s]"
  
  do t = 0,num_t-1
    tt = 0
    data = "  "+sprintf("%10.2f",z (::-1))  +"    "+ \
                sprintf("%10.2f",p (::-1))  +"    "+ \
         	sprintf("%10.2f",Th(::-1))  +"    "+ \
         	sprintf("%10.2f",q (::-1))  +"    "+ \
         	sprintf("%10.2f",u (::-1))  +"    "+ \
         	sprintf("%10.2f",v (::-1))
    header = " "+time(t)+",   "+num_z+",  "+ps+"  day,levels,pres0"
    tt = 1+(t)*num_r
    str(tt             ) = header
    str(tt+1:tt+num_r-1) = data
      delete(data)
  end do
  
  print("")
  print(""+str)	
  
  asciiwrite(ofile,str)
  
  print("")
  print("  "+ofile)
  print("")

;======================================================================================
;======================================================================================
end
