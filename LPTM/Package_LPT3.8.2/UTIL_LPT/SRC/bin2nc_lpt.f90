PROGRAM bin2nc_lpt

! This program converts LPT output file from the binary format to the netcdf format.

	IMPLICIT NONE
	INCLUDE 'netcdf.inc'
	
	INTEGER :: nsubdomains, np, nv0, nv1, nv, nhistory, nt
	REAL :: xwidth, ywidth, ztop, zbottom, day
	CHARACTER (len=80), ALLOCATABLE, DIMENSION(:) :: var_name
	CHARACTER (len=10), ALLOCATABLE, DIMENSION(:) :: var_shrt
	CHARACTER (len=10), ALLOCATABLE, DIMENSION(:) :: var_unit
	REAL, ALLOCATABLE, DIMENSION(:) :: time, tmp_r
	REAL, ALLOCATABLE, DIMENSION(:,:) :: var
	INTEGER, ALLOCATABLE, DIMENSION(:) :: tags, nvi, tmp_i
	INTEGER, ALLOCATABLE, DIMENSION(:,:) :: nvis
	REAL, ALLOCATABLE, DIMENSION(:,:,:) :: vars
	
	! netCDF stuff
	CHARACTER(len=200) :: filename
	INTEGER :: ncid, err, xyzid, pid, tid, varid
	INTEGER, DIMENSION(2) :: vdimids
	REAL, ALLOCATABLE, DIMENSION(:,:) :: buff
	
	! Other
	INTEGER :: i, j, k, n, tmp
	
	! External functions
	INTEGER, EXTERNAL :: iargc
	
	
	!------------------------------------------------------------------------------------------
	! Read filename from the command line
	!------------------------------------------------------------------------------------------
	i = iargc()
	IF (i == 0) THEN
		PRINT*,'No input file name is specified.'
		PRINT*,'Format: bin2nc_lpt input_LPT.bin'
		STOP
	ENDIF
	CALL getarg(1,filename)
	
	
	! Open and read header
	OPEN(123, FILE=TRIM(filename), STATUS='old', FORM='unformatted')
	READ(123) nsubdomains, np, nv0, nv1, nv, nhistory, xwidth, ywidth, ztop, zbottom
	PRINT*,'np, nv0, nv1, nv, nhistory, xwidth, ywidth, ztop, zbottom'
	PRINT*, np, nv0, nv1, nv, nhistory, xwidth, ywidth, ztop, zbottom
	ALLOCATE(var_name(0:nv), var_shrt(0:nv), var_unit(0:nv))
	READ(123) var_name, var_shrt, var_unit
	DO i = 0, nv
		PRINT*, var_shrt(i),' ',var_unit(i),' ',TRIM(var_name(i))
	ENDDO
	
	! Find nt
	IF (nhistory == -1) THEN   ! one file for all data
		i = 1
		DO WHILE(i >= 0)
			READ(123,IOSTAT=i) nt, day
			IF (i < 0) EXIT
			DO j = 1, nsubdomains
				READ(123)   ! tag
				READ(123)   ! nvi
				DO k = 1, nv
					READ(123)   ! variables
				ENDDO
			ENDDO
		ENDDO
		REWIND(123)   ! back to file top
		READ(123)   ! nsubdomains, np, nv0, ...
		READ(123)   ! var_name, ...
	ELSE IF (nhistory == 1) THEN   ! one time record in one file
		nt = nhistory
	ELSE   ! nhistory time records. Number of data in the last file can be less than nhistory.
		READ(123) nt, day
		REWIND(123)
		READ(123)
		READ(123)
		n = nt / nhistory   ! nt can be more than 1
		i = 1
		DO WHILE(i >= 0)
			READ(123,IOSTAT=i) nt, day
			IF (i < 0) EXIT
			DO j = 1, nsubdomains
				READ(123)   ! tag
				READ(123)   ! nvi
				DO k = 1, nv
					READ(123)   ! variables
				ENDDO
			ENDDO
		ENDDO
		REWIND(123)   ! back to file top
		READ(123)   ! nsubdomains, np, nv0, ...
		READ(123)   ! var_name, ...
		nt = nt - n * nhistory
	ENDIF
	
	! Read data
	PRINT*,'Reading data. Be patient...'
	n = np / nsubdomains
	ALLOCATE(time(nt),var(np,nv),tags(np),nvi(np),nvis(np,nt),vars(np,nv,nt),tmp_i(n),tmp_r(n))
	DO k = 1, nt
		READ(123) tmp, day
		time(k) = day
		DO j = 1, nsubdomains
			READ(123) tmp_i
			tags(1+(j-1)*n:n*j) = tmp_i(:)
			READ(123) tmp_i
			nvi(1+(j-1)*n:n*j) = tmp_i(:)
			DO i = 1, nv
				READ(123) tmp_r 
				var(1+(j-1)*n:n*j,i) = tmp_r(:)
			ENDDO
		ENDDO
		nvis(:,k) = nvi(:)
		DO i = 1, nv
			vars(:,i,k) = var(:,i)
		ENDDO
	ENDDO
	CLOSE(123)
	DEALLOCATE(var, nvi, tmp_i, tmp_r)
	
	! Display some data
	PRINT*,'time (day)', time
	PRINT*, 'tags(upto 50):', tags(1:min(np,50))
	DO i = 1, min(nt,5)
		PRINT*,'nvi, vars (upto 5 steps for tag 1):', nvis(1,i), vars(1,:,i)
	ENDDO
	
	
	!------------------------------------------------------------------------------------------
	! Main routine: bin to netCDF
	!------------------------------------------------------------------------------------------
	PRINT*,'Saving data in netCDF format...'
	! Output filename
	DO i = 1, 197
		IF(filename(i:i+3) == '.bin') THEN
			filename(i:i+3) = '.nc '
			EXIT
		ELSE IF(i == 198) THEN
			PRINT*,'Wrong filename extension!'
			STOP
		ENDIF
	ENDDO
	
	! Initialize netCDF stuff
	err = NF_CREATE(TRIM(filename), NF_CLOBBER, ncid)
	err = NF_REDEF(ncid)
	
	! Define independent variables
	err = NF_DEF_DIM(ncid, 'np', np, pid)
	err = NF_DEF_VAR(ncid, 'tag', NF_INT, 1, pid, varid)
	err = NF_PUT_ATT_TEXT(ncid, varid, 'units', 3, '[ ]')
	err = NF_PUT_ATT_TEXT(ncid, varid, 'long_name', 19, 'number of perticles')
	
	err = NF_DEF_DIM(ncid, 'nt', nt, tid)
	err = NF_DEF_VAR(ncid, 'time', NF_FLOAT, 1, tid, varid)
	err = NF_PUT_ATT_TEXT(ncid, varid, 'units', 5, '[day]')
	
	err = NF_DEF_DIM(ncid, 'xyz', 1, xyzid)
	err = NF_DEF_VAR(ncid, 'xwidth', NF_FLOAT, 1, xyzid, varid)
	err = NF_PUT_ATT_TEXT(ncid, varid, 'units', 3, '[m]')
	err = NF_PUT_ATT_TEXT(ncid, varid, 'long_name', 27, 'Domain width in x direction')
	
	err = NF_DEF_VAR(ncid, 'ywidth', NF_FLOAT, 1, xyzid, varid)
	err = NF_PUT_ATT_TEXT(ncid, varid, 'units', 3, '[m]')
	err = NF_PUT_ATT_TEXT(ncid, varid, 'long_name', 27, 'Domain width in y direction')
	
	err = NF_DEF_VAR(ncid, 'ztop', NF_FLOAT, 1, xyzid, varid)
	err = NF_PUT_ATT_TEXT(ncid, varid, 'units', 3, '[m]')
	err = NF_PUT_ATT_TEXT(ncid, varid, 'long_name', 22, 'Highest altitude limit')
	
	err = NF_DEF_VAR(ncid, 'zbottom', NF_FLOAT, 1, xyzid, varid)
	err = NF_PUT_ATT_TEXT(ncid, varid, 'units', 3, '[m]')
	err = NF_PUT_ATT_TEXT(ncid, varid, 'long_name', 21, 'Lowest altitude limit')
	
	err = NF_ENDDEF(ncid)
	
	! Write
	err = NF_INQ_VARID(ncid, 'tag', varid)
	err = NF_PUT_VAR_INT(ncid, varid, tags)
	err = NF_INQ_VARID(ncid, 'time', varid)
	err = NF_PUT_VAR_REAL(ncid, varid, time)	
	
	err = NF_INQ_VARID(ncid, 'xwidth', varid)
	err = NF_PUT_VAR_REAL(ncid, varid, xwidth)
	
	err = NF_INQ_VARID(ncid, 'ywidth', varid)
	err = NF_PUT_VAR_REAL(ncid, varid, ywidth)
	
	err = NF_INQ_VARID(ncid, 'ztop', varid)
	err = NF_PUT_VAR_REAL(ncid, varid, ztop)
	
	err = NF_INQ_VARID(ncid, 'zbottom', varid)
	err = NF_PUT_VAR_REAL(ncid, varid, zbottom)
	
	vdimids(1) = pid
	vdimids(2) = tid
	
	! nvi
	err = NF_REDEF(ncid)
	err = NF_DEF_VAR(ncid, TRIM(var_shrt(0)), NF_INT, 2, vdimids, varid)
	err = NF_PUT_ATT_TEXT(ncid, varid, 'long_name', LEN_TRIM(var_name(0)), TRIM(var_name(0)) )
	err = NF_PUT_ATT_TEXT(ncid, varid, 'units', LEN_TRIM(var_unit(0)), TRIM(var_unit(0)) )
	err = NF_ENDDEF(ncid)
	err = NF_PUT_VAR_INT(ncid, varid, nvis)
	
	! variables
	ALLOCATE(buff(np,nt))
	DO n = 1, nv
		buff(:,:) = 0.
		PRINT*, var_shrt(n),' ',TRIM(var_name(n)),' ',var_unit(n)
		DO j = 1, nt
			DO i = 1, np
				buff(i,j) = vars(i,n,j)
			ENDDO	
		ENDDO
		err = NF_REDEF(ncid)
		err = NF_DEF_VAR(ncid, TRIM(var_shrt(n)), NF_FLOAT, 2, vdimids, varid)
		err = NF_PUT_ATT_TEXT(ncid, varid, 'long_name', LEN_TRIM(var_name(n)), TRIM(var_name(n)) )
		err = NF_PUT_ATT_TEXT(ncid, varid, 'units', LEN_TRIM(var_unit(n)), TRIM(var_unit(n)) )
		err = NF_ENDDEF(ncid)
		err = NF_PUT_VAR_REAL(ncid, varid, buff)
	ENDDO
	
	err = NF_CLOSE(ncid)
	
END PROGRAM bin2nc_lpt
