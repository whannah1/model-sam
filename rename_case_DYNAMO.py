#!/usr/bin/env python
#========================================================================================================================
# 	This script renames files of an ensemble of SAM runs 
#
#    Jul, 2013	Walter Hannah 		Colorado State University
#========================================================================================================================
import datetime
import sys
import os
import numpy as np
import glob
#========================================================================================================================
#========================================================================================================================

debug = False

skip_3D = False

PH = "wet"

CASENAME = "DYNAMO_IDEAL_NSA_"+str.upper(PH)

CID_old = ["00"]
CID_new = ["10"]


nx = 144	# number of points in the x & y directions
nz =  64	# number of vertical levels

dx = 500	# Horizontal resolution [m]
dt =   5	# timestep		[s]

SAM_DIR	= "/maloney-scratch/whannah/SAM/"

nCID = len(CID_old)
#========================================================================================================================
#========================================================================================================================
print
print
print "Renaming SAM DYNAMO Data  (",datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),")"
print
print
for c in xrange(nCID):
	#-----------------------------------------------------------------------------------
	# Set the old case name
	#-----------------------------------------------------------------------------------
	old_CASE_TAIL = str(dx)+"_"+str(nx)+"x"+str(nz)+"_"+CID_old[c]
	old_CASE = CASENAME+"_"+old_CASE_TAIL
	print "  old CASE = "+old_CASE
	#-----------------------------------------------------------------------------------
	# Set the old new name
	#-----------------------------------------------------------------------------------
	new_CASE_TAIL = str(dx)+"_"+str(nx)+"x"+str(nz)+"_"+CID_new[c]
	new_CASE = CASENAME+"_"+new_CASE_TAIL
	print "  new CASE = "+new_CASE
	print
	#-----------------------------------------------------------------------------------
	# Edit the file and folder names
	#-----------------------------------------------------------------------------------
	idir = SAM_DIR+old_CASE+"/"
	odir = SAM_DIR+new_CASE+"/"
	CMD = "mv "+idir+" "+odir
	print "    "+CMD
	if debug!=True and os.path.isdir(idir) :  os.system(CMD)
	#-----------------------------------------------------------------------------------
	print
	idir = SAM_DIR+new_CASE+"/OUT_2D/"+old_CASE+"*"
	print("  "+idir+":")
	for f in sorted(glob.glob(idir)):
		ifile = f
		ofile = f.replace(old_CASE,new_CASE)
		CMD = "mv "+ifile+" "+ofile
		#print "  "+CMD
		print "    "+f.replace(SAM_DIR+new_CASE+"/OUT_2D/","").ljust(55)+"  >  "\
		            +f.replace(SAM_DIR+new_CASE+"/OUT_2D/","").replace(old_CASE,new_CASE)
		if debug!=True and os.path.isfile(f) : os.system(CMD)
	#-----------------------------------------------------------------------------------
	print
	idir = SAM_DIR+new_CASE+"/OUT_STAT/"+old_CASE+"*"
	print("  "+idir+":")
	for f in sorted(glob.glob(idir)):
		ifile = f
		ofile = f.replace(old_CASE,new_CASE)
		CMD = "mv "+ifile+" "+ofile
		#print "  "+CMD
		print "    "+f.replace(SAM_DIR+new_CASE+"/OUT_STAT/","").ljust(55)+"  >  " \
		            +f.replace(SAM_DIR+new_CASE+"/OUT_STAT/","")
		if debug!=True and os.path.isfile(f) : os.system(CMD)
	#-----------------------------------------------------------------------------------
	print 
	idir = SAM_DIR+new_CASE+"/OUT_LPT/"+old_CASE+"*"
	print("  "+idir+":")
	for f in sorted(glob.glob(idir)):
		ifile = f
		ofile = f.replace(old_CASE,new_CASE)+"_"+new_CASE_TAIL
		CMD = "mv "+ifile+"  "+ofile
		print "    "+f.replace(SAM_DIR+new_CASE+"/OUT_LPT/","").ljust(55)+"  >  "\
			    +f.replace(SAM_DIR+new_CASE+"/OUT_LPT/","").replace(old_CASE,new_CASE)
		if debug!=True and os.path.isfile(f) : os.system(CMD)
	#-----------------------------------------------------------------------------------
	if skip_3D==False :
		print 
		idir = SAM_DIR+new_CASE+"/OUT_3D/"+old_CASE+"*"
		print("  "+idir+":")
		for f in sorted(glob.glob(idir)):
			ifile = f
			ofile = f.replace(old_CASE,new_CASE)
			CMD = "mv "+ifile+"  "+ofile
			print "    "+f.replace(SAM_DIR+new_CASE+"/OUT_3D/","").ljust(55)+"  >  "\
				    +f.replace(SAM_DIR+new_CASE+"/OUT_3D/","").replace(old_CASE,new_CASE)
			if debug!=True and os.path.isfile(f) : os.system(CMD)
		#-----------------------------------------------------------------------------------
		print 
		idir = SAM_DIR+new_CASE+"/OUT_3D/*ENTRAIN*"+old_CASE_TAIL+"*"
		print("  "+idir+":")
		for f in sorted(glob.glob(idir)):
			ifile = f
			ofile = f.replace(old_CASE_TAIL,new_CASE_TAIL)
			CMD = "mv "+ifile+"  "+ofile
			print "    "+f.replace(SAM_DIR+new_CASE+"/OUT_3D/","").ljust(55)+"  >  "\
				    +f.replace(SAM_DIR+new_CASE+"/OUT_3D/","").replace(old_CASE_TAIL,new_CASE_TAIL)
			if debug!=True and os.path.isfile(f) : os.system(CMD)
		#-----------------------------------------------------------------------------------

print
#========================================================================================================================
#========================================================================================================================


