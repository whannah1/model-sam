#! /bin/bash

	TOP_DIR='/maloney-scratch/whannah/SAM/6.9.5_ent'
	UTL_DIR=$TOP_DIR'/UTIL'



OUT_DIR=$TOP_DIR'/OUT_3D'
SCRIPT='com3D2nc'
for f in $OUT_DIR/*.com3D
do
  #len=${#f}
  #sub=$(expr ${#f} - 5)
  if [ ! -e ${f:0:$(expr ${#f} - 5)}"nc" ]
  then
    $UTL_DIR'/'$SCRIPT $f
  fi
done

OUT_DIR=$TOP_DIR'/OUT_STAT'
SCRIPT='stat2nc'
for f in $OUT_DIR/*.stat
do
  $UTL_DIR'/'$SCRIPT $f
done


OUT_DIR=$TOP_DIR'/OUT_2D'
SCRIPT='2Dcom2nc'
for f in $OUT_DIR/*.2Dcom*
do
  $UTL_DIR'/'$SCRIPT $f
  echo $UTL_DIR'/'$SCRIPT $f
done

echo 'all done!'

#OUT_DIR=$TOP_DIR'/OUT_MOVIES'
#$UTL_DIR'/glue_movie_raw' $OUT_DIR/GATE_IDEAL_32x28sp




#echo $UTL_DIR'/'$SCRIPT $OUT_DIR/*.2Dcom

