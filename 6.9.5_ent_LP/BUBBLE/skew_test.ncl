;***********************************************
;
;*************************************************
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/skewt_func.ncl"
;*************************************************
begin
	
	dir = "/maloney-scratch/whannah/SAM/6.9.5_ent/BUBBLE/"
	
	ifile = dir+"snd"
	
	fig_type = "x11"
	fig_file = dir+"sounding"
	
;======================================================================================
;======================================================================================
  infile = addfile(ifile,"r")
  

;======================================================================================
;======================================================================================
  opt  = True
  ;opt@lat1 = -10.
  ;opt@lat2 =  10.
  ;opt@lon1 =  50.
  ;opt@lon2 =  90.
  ;opt@yr1  = 2007
  ;opt@yr2  = 2007


  wks  = gsn_open_wks ("x11", "skewt")

; ---------------------(1) Draw Default Background ------------
  skewtOpts                  = True
  skewtOpts@tiMainString     = "Default Skew-T"

  skewt_bkgd     = skewT_BackGround (wks, skewtOpts)
  draw (skewt_bkgd)
  frame(wks)
  delete (skewtOpts)



 end
