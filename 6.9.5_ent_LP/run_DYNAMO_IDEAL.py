#!/usr/bin/env python
#========================================================================================================================
# 	This script runs SAM with the DYNAMO_IDEAL case on keto (linux)	
# 	
#
#    Oct, 2013	Walter Hannah 		Colorado State University
#========================================================================================================================
import datetime
import sys
import os
import numpy as np
#========================================================================================================================
#========================================================================================================================

MN = "oct"

PH = "wet"

CASENAME = "DYNAMO_IDEAL_NSA_"+str.upper(PH)

#CID = "00"
if (MN=="oct") : CID = "00"
if (MN=="nov") : CID = "12"

RUN   = True
POST  = True
MOVE  = True
CLEAR = False	# not working last time I checked!

shear_flag = False
dcsst_flag = False

RESTART = 0		# 0 - Startup ; 1 - Restart ; 2 - Branch

nx = 288  		# number of points in the x & y directions
nz = 128  		# number of vertical levels

dx = 200		# Horizontal resolution [m]
dt =   2		# timestep		[s]

nstop =  1*24*3600/dt

data_dt =  5*60/dt
start3D =  1*24*3600/dt
start2D =  1*24*3600/dt

if (MN=="oct") and (PH=="dry") : day0 = 273 +  8
if (MN=="oct") and (PH=="wet") : day0 = 273 + 28
if (MN=="nov") and (PH=="dry") : day0 = 273 + 31 + 15
if (MN=="nov") and (PH=="wet") : day0 = 273 + 31 + 25


SAM_DIR	= "/maloney-scratch/whannah/SAM/6.9.5_ent_LP/"
SAM_EXE = "SAM_ADV_MPDATA_RAD_CAM_MICRO_SAM1MOM_"+str(nx)+"x"+str(nz)  +"_B1km"

UTL_DIR = SAM_DIR+"UTIL/"
#========================================================================================================================
#========================================================================================================================

with open(SAM_DIR+"CaseName", "w") as CaseName_file:	    # make sure CaseName indicates
    CaseName_file.write(CASENAME+"\n")			    # we are running the correct case

print
print
if RUN == True:
	print "Running "+CASENAME+" (",datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),")"
	print
	print

#-----------------------------------------------------------------------------------
# Set the case name
CASE_TAIL = str(dx)+"_"+str(nx)+"x"+str(nz)+"_"+CID
if shear_flag == True:
	CASE_TAIL = CASE_TAIL
CASE = CASENAME+"_"+CASE_TAIL
print "  CASE = "+CASE
#-----------------------------------------------------------------------------------
# Edit the namelist file
#ifile = SAM_DIR+CASENAME+"/prm_template"
ofile = SAM_DIR+CASENAME+"/prm"

if RUN == True:
	os.system( "sed -i \"s/day0 =.*"		+"/day0     = "			+str(day0)				+",/\"  "	+ofile )	
	os.system( "sed -i \"s/nrestart =.*"	+"/nrestart = "			+str(RESTART)			+",/\" "	+ofile )
	os.system( "sed -i \"s/caseid =.*"		+"/caseid = \'"			+CASE_TAIL				+"\',/\"  "	+ofile )
	os.system( "sed -i 's/dx =.*"			+"/dx = "				+str(dx)				+"./' "		+ofile )
	os.system( "sed -i 's/dy =.*"			+"/dy = "				+str(dx)				+"./' "		+ofile )
	os.system( "sed -i 's/dt =.*"			+"/dt = "				+str(dt)				+"/'  "		+ofile )
	os.system( "sed -i 's/nrad .*"			+"/nrad      = " 		+str(3*60/dt)			+"/'  "		+ofile )
	os.system( "sed -i 's/nstop.*"			+"/nstop     = "		+str(nstop)				+"/'  "		+ofile )
	os.system( "sed -i 's/nprint .*"		+"/nprint    = " 		+str(6*3600/dt)			+"/'  "		+ofile )
	os.system( "sed -i 's/nstatfrq .*"		+"/nstatfrq  = " 		+str(data_dt)			+"/'  "		+ofile )
	os.system( "sed -i 's/nstat .*"			+"/nstat     = " 		+str(data_dt)			+"/'  "		+ofile )
	os.system( "sed -i 's/nsave2D .*"		+"/nsave2D        = " 	+str(data_dt)			+"/'  "		+ofile )
	os.system( "sed -i 's/nsave3D .*"		+"/nsave3D        = " 	+str(data_dt)			+"/'  "		+ofile )
	os.system( "sed -i 's/nent3D .*"		+"/nent3D         = "   +str(data_dt)			+"/'  "		+ofile )
	os.system( "sed -i 's/nsave2Dstart .*"	+"/nsave2Dstart   = "	+str(start2D)			+"/'  "		+ofile )
	os.system( "sed -i 's/nsave3Dstart .*"	+"/nsave3Dstart   = "	+str(start3D)			+"/'  "		+ofile )
	os.system( "sed -i 's/nent3Dstart .*"	+"/nent3Dstart    = "  	+str(start3D)			+"/'  "		+ofile )
	#exit()
#-----------------------------------------------------------------------------------
# Update the snd and lsf filess
if RUN == True:
	#os.system("rm -f "+SAM_DIR+CASENAME+"/snd ")
	if shear_flag == True:
		snd_file = CASENAME+"/snd."+MN+"."+(PH)+".sa_shear"
		lsf_file = CASENAME+"/lsf."+MN+"."+(PH)+".sa_shear"
	else:
		snd_file = CASENAME+"/snd."+MN+"."+(PH)+".no_shear"
		lsf_file = CASENAME+"/lsf."+MN+"."+(PH)+".no_shear"
	if dcsst_flag == True:
		sfc_file = CASENAME+"/sfc."+MN+"."+(PH)+".diurnal"
	else:
		sfc_file = CASENAME+"/sfc."+MN+"."+(PH)
	# Replace SND file
	snd_file = SAM_DIR+snd_file
	CMD = "cp "+snd_file+"  "+SAM_DIR+CASENAME+"/snd"
	os.system(CMD)
	# Replace LSF file
	lsf_file = SAM_DIR+lsf_file
	CMD = "cp "+lsf_file+"  "+SAM_DIR+CASENAME+"/lsf"
	os.system(CMD)
	# Replace SFC file
	sfc_file = SAM_DIR+sfc_file
	CMD = "cp "+sfc_file+"  "+SAM_DIR+CASENAME+"/sfc"
	os.system(CMD)
#-----------------------------------------------------------------------------------
# Run the Case
if RUN == True:
	print datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
	CMD = "mpirun -n 36 ./"+SAM_EXE  +"  >  "+SAM_DIR+"TERM.out/"+CASE+".out"
	print CMD
	os.system(CMD)
	
	print
	print datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
	print "Finished running case: "+CASE+"  ( "+SAM_DIR+"TERM.out/"+CASE+".out )"
	print
#-----------------------------------------------------------------------------------
# Process the output
if POST == True:
	print "Post-processing "+CASENAME+" (",datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),")"
	print
	print
	# old method : CMD = SAM_DIR+"postprocess_case.sh "+CASENAME+"  "+CASE_TAIL+"  > "+SAM_DIR+"TERM.out/post."+CASE+".out"
	#--------------------------------------
	# STAT output
	#--------------------------------------
	for f in os.listdir(SAM_DIR+"OUT_STAT/"):
		if f.endswith(".stat"): 
			CMD = UTL_DIR+"stat2nc "+SAM_DIR+"OUT_STAT/"+f+"  >  "+SAM_DIR+"TERM.out/post."+CASE+".stat.out"
			print CMD
			os.system(CMD)
	#--------------------------------------
	# 2D output
	#--------------------------------------
	for f in os.listdir(SAM_DIR+"OUT_2D/"):
		if f.endswith(".2Dcom"): 
			CMD = UTL_DIR+"2Dcom2nc "+SAM_DIR+"OUT_2D/"+f+"  >  "+SAM_DIR+"TERM.out/post."+CASE+".2D.out"
			print CMD
			os.system(CMD)
	#--------------------------------------
	# 3D output (normal & tet. entrainment)
	#--------------------------------------
	logfile = SAM_DIR+"TERM.out/post."+CASE+".3D.out"
	os.system("rm "+logfile)
	print
	print "  3D output log file: "+logfile
	for f in os.listdir(SAM_DIR+"OUT_3D/"):
		if f.endswith(".com3D") and not os.path.isfile(SAM_DIR+"OUT_3D/"+f.replace(".com3D",".nc")) and (CASE in f):
			CMD = UTL_DIR+"com3D2nc "+SAM_DIR+"OUT_3D/"+f+"  >>  "+logfile
			#print CMD
			os.system(CMD)
		if f.endswith(".com3D") and not os.path.isfile(SAM_DIR+"OUT_3D/"+f.replace(".com3D",".nc")) and ("ENTRAIN" in f):
			CMD = UTL_DIR+"old_com3D2nc "+SAM_DIR+"OUT_3D/"+f+"  >>  "+logfile
			#print CMD
			os.system(CMD)
#-----------------------------------------------------------------------------------
# Delete the 3D binary files (to save space)
if CLEAR == True:
	CMD = "rm -f "+SAM_DIR+"OUT_3D/*"+CASE_TAIL+"*com3D"
	print CMD
	os.system(CMD)

#-----------------------------------------------------------------------------------
# Move to permanant output directory
if MOVE == True:
	CMD = 'ncl \'case_stub="'+CASENAME+'"\' \'case_tail="'+CASE_TAIL+'"\'  '+SAM_DIR+'../move_data_LP.ncl'
	print
	print CMD
	os.system(CMD)
print
#========================================================================================================================
#========================================================================================================================


