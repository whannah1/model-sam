#!/usr/bin/env python
#========================================================================================================================
# 	This script builds an ensemble of SAM executables	
#
#    Jul, 2013	Walter Hannah 		Colorado State University
#========================================================================================================================
import datetime
import sys
import os
import numpy as np
#========================================================================================================================
#========================================================================================================================
TOP_DIR = "/raid6/Model/SAM/scratch/"
SRC_DIR = "/home/whannah/Model/SAM/6.9.5_ent_LP/SRC/"

nx = 512		# number of points in the x & y directions
nz = 96		# number of vertical levels

nsub = 8	# number of subdomains for parallel

#========================================================================================================================
#========================================================================================================================
print
print
print "Building SAM executables  (",datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),")"
print
print

print "========================================================================================================="
print "Editing executable name in Makefile"
CMD = "sed -i \'s/SAM = SAM_$(ADV_DIR)_$(RAD_DIR)_$(MICRO_DIR).*"+"/SAM = SAM_$(ADV_DIR)_$(RAD_DIR)_$(MICRO_DIR)_2D_"+str(nx)+"x"+str(nz)+"/\'  "+"./Makefile"
print "    "+CMD
os.system( CMD )
print "========================================================================================================="
print "Editing domain.f90"
CMD = "sed -i \'s/YES3D =.*"+"/YES3D = 0  ! Domain dimensionality: 1 - 3D, 0 - 2D/\'  "+SRC_DIR+"domain.f90"
print "    "+CMD
os.system( CMD )

CMD = "sed -i \'s/nx_gl.*"+"/nx_gl = "+str(nx)+"/\'  "+SRC_DIR+"domain.f90"
print "    "+CMD
os.system( CMD )
CMD = "sed -i \'s/ny_gl.*"+"/ny_gl = 1/\'  "+SRC_DIR+"domain.f90"
print "    "+CMD
os.system( CMD )
CMD = "sed -i \'s/nz_gl.*"+"/nz_gl = "+str(nz)+"/\'  "+SRC_DIR+"domain.f90"
print "    "+CMD
os.system( CMD )

CMD = "sed -i \'s/nsubdomains_x.*"+"/nsubdomains_x  = "+str(nsub)+"/\'  "+SRC_DIR+"domain.f90"
print "    "+CMD
os.system( CMD )
CMD = "sed -i \'s/nsubdomains_y.*"+"/nsubdomains_y  = 1/\'  "+SRC_DIR+"domain.f90"
print "    "+CMD
os.system( CMD )
CMD = "sed -i \'s/ntracers =.*/ntracers = 1/\' "+SRC_DIR+"domain.f90"
print "    "+CMD
os.system( CMD )
print "========================================================================================================="
print "Clearing OBJ directory"
CMD = "rm -f "+TOP_DIR+"OBJ/*"
print "    "+CMD
os.system(CMD)
print "Building SAM..."
#CMD = "nohup ./Build > TERM.out/build_2D.out"
CMD = " ./Build "
print "    "+CMD
os.system(CMD)
print "========================================================================================================="
print "========================================================================================================="


#========================================================================================================================
#========================================================================================================================
