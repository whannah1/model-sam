#!/usr/bin/env python
#========================================================================================================================
#   This script runs an ensemble of SAM runs with various initial   
#   anomaly values for a bubble at the center of the domain     
#
#    Jul, 2013  Walter Hannah       Colorado State University
#========================================================================================================================
import datetime
import sys
import os
import numpy as np
home = os.getenv("HOME")
#========================================================================================================================
#========================================================================================================================
RUN,POST,CLEAR,MOVE = False,False,False,False

RUN   = True
POST  = True
# MOVE = True

CASENUM  = "00"
CASENAME = "RCE"

restart_code = 0   # 0=start  1=continue   2=continue with new namelist

#nx = 256        # number of points in the x & y directions
#nz = 128        # number of vertical levels
#dx = 200        # Horizontal resolution [m]
#dt =   2        # timestep      [s]

# nx = 512      # number of points in the x directions
# ny = 128       # number of points in the y directions
# nz =  72       # number of vertical levels
# dx = 200       # Horizontal resolution [m]
# dt =   2       # timestep      [s]

nx =  64       # number of points in the x directions
ny =  64       # number of points in the y directions
nz =  72       # number of vertical levels
dx = 200       # Horizontal resolution [m]
dt =   2       # timestep      [s]

sst = 300.0
sst_str = "{0:.1f}".format(sst)


if CASENUM=="00" :
	#nstop   = (2*24+12)*3600/dt
	#nelapse = 12*3600/dt
	#data_dt = 10*60/dt
	#start3D = 1000*24*3600/dt
	#nrskip  = 0*3600/dt/data_dt
    nstop   = 6*3600/dt
    nelapse = nstop
    data_dt = 30/dt
    start3D = 1000*24*3600/dt
    nrskip  = nstop/data_dt
    # nrskip  = 24*3600/dt/data_dt

    nstop   = 30/dt                 #<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< DELETE THIS!!!!!
    nelapse = nstop
    data_dt = 10/dt
    start3D = 9999999
    nrskip  = 0 

if CASENUM=="01" :
	nstop   =  12*3600/dt
	nelapse =  nstop#12*3600/dt
	data_dt =  60/dt
	start3D =  00*3600/dt
	nrskip  =   3*3600/dt/data_dt

if CASENUM=="02" :
    nstop   =  12*3600/dt
    nelapse =  nstop
    data_dt =  60/dt
    start3D =  128*3600/dt
    nrskip  =  11*3600/dt/data_dt

SAM_DIR = home+"/Model/SAM/6.9.5_ent_LP/"
SCR_DIR = SAM_DIR+"scratch/"
#SAM_EXE = "SAM_ADV_MPDATA_SGS_TKE_RAD_CAM_MICRO_SAM1MOM_"+str(nx)+"x"+str(nz)
SAM_EXE = "SAM_ADV_MPDATA_RAD_CAM_MICRO_SAM1MOM_"+str(nx)+"x"+str(ny)+"x"+str(nz)
UTL_DIR = SAM_DIR+"UTIL/"
UTL_LPT = SAM_DIR+"UTIL_LPT/"
LOG_DIR = SAM_DIR+"logs/"
#========================================================================================================================
#========================================================================================================================
nproc = 16

dy = dx

BUB_DX = [4.0]
BUB_DT = [1.0]
BUB_DQ = [2]
#qsig = 0.0      # factor to increase moisture (in std. deviations)
#tsig = 0.0      # factor to increase temperature (in std. deviations)

#ndx = len(BUB_DX)
#ndt = len(BUB_DT)
#ndq = len(BUB_DQ)

with open(SAM_DIR+"CaseName", "w") as CaseName_file:    # make sure CaseName indicates
    CaseName_file.write("RCE\n")                        # we are running the RCE case
#========================================================================================================================
#========================================================================================================================
print
print "Running SAM RCE 3D (",datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),")"
print
q = 0
u = 0
x = 0
t = 0

#-----------------------------------------------------------------------------------
# Set the case name
#-----------------------------------------------------------------------------------
#CASE_TAIL = "LP_"+str(dx)+"_"+str(nx)+"x"+str(nz)+"_"+sst_str+"K_"+CASENUM
CASE_TAIL =        str(dx)+"_"+str(nx)+"x"+str(ny)+"x"+str(nz)+"_"+sst_str+"K_"+CASENUM
CASE = CASENAME+"_"+CASE_TAIL
print "  CASE = "+CASE
#-----------------------------------------------------------------------------------
# Edit the namelist file
#-----------------------------------------------------------------------------------
ofile = SAM_DIR+"RCE/prm"

os.system( "sed -i \"s/day0 =.*"        +"/day0      = "     +str("0.0")        +",/\"  "   +ofile )
os.system( "sed -i \"s/caseid =.*"      +"/caseid = \'"      +CASE_TAIL         +"\',/\"  " +ofile )
os.system( "sed -i \"s/nrestart .*"     +"/nrestart = "      +str(restart_code) +",/\" "    +ofile )
os.system( "sed -i 's/dx =.*"           +"/dx = "            +str(dx)           +"./' "     +ofile )
os.system( "sed -i 's/dy =.*"           +"/dy = "            +str(dx)           +"./' "     +ofile )
os.system( "sed -i 's/dt =.*"           +"/dt = "            +str(dt)           +"/'  "     +ofile )
os.system( "sed -i 's/nrad .*"          +"/nrad           = "+str(3*60/dt)      +"/'  "     +ofile )
os.system( "sed -i 's/nstop.*"          +"/nstop          = "+str(nstop)        +"/'  "     +ofile )
os.system( "sed -i 's/nelapse.*"        +"/nelapse        = "+str(nelapse)      +"/'  "     +ofile )
os.system( "sed -i 's/nprint .*"        +"/nprint         = "+str(nstop)        +"/'  "     +ofile )
os.system( "sed -i 's/nrestart_skip .*" +"/nrestart_skip  = "+str(nrskip)       +"/'  "     +ofile )
os.system( "sed -i 's/dolpt.*"          +"/dolpt          = .false. "           +"/'  "     +ofile )
os.system( "sed -i 's/doentrainment.*"  +"/doentrainment  = .true. "            +"/'  "     +ofile )
# output data intervals
os.system( "sed -i 's/nstatfrq .*"      +"/nstatfrq       = "+str(1)            +"/'  "     +ofile )
os.system( "sed -i 's/nstat .*"         +"/nstat          = "+str(data_dt)      +"/'  "     +ofile )
os.system( "sed -i 's/nsave2D .*"       +"/nsave2D        = "+str(data_dt)      +"/'  "     +ofile )
os.system( "sed -i 's/nsave3D .*"       +"/nsave3D        = "+str(data_dt)      +"/'  "     +ofile )
os.system( "sed -i 's/nent3D .*"        +"/nent3D         = "+str(data_dt)      +"/'  "     +ofile )
os.system( "sed -i 's/nsave2Dstart .*"  +"/nsave2Dstart   = "+str(data_dt)      +"/'  "     +ofile )
os.system( "sed -i 's/nsave3Dstart .*"  +"/nsave3Dstart   = "+str(start3D)      +"/'  "     +ofile )
os.system( "sed -i 's/nent3Dstart .*"   +"/nent3Dstart    = "+str(start3D)      +"/'  "     +ofile )
# bubble settings
os.system( "sed -i 's/bubble_x0 .*"        +"/bubble_x0         = "+str(nx*dx/2)           +"/'  "     +ofile )
os.system( "sed -i 's/bubble_y0 .*"        +"/bubble_y0         = "+str(ny*dy/2)           +"/'  "     +ofile )
os.system( "sed -i 's/bubble_z0 .*"        +"/bubble_z0         = "+str(500)               +"/'  "     +ofile )
os.system( "sed -i 's/bubble_dtemp .*"     +"/bubble_dtemp      = "+str(BUB_DT[t])         +"/'  "     +ofile )
os.system( "sed -i 's/bubble_dq .*"        +"/bubble_dq         = "+str(BUB_DQ[q]/1000.)   +"/'  " +ofile )
os.system( "sed -i 's/bubble_radius_hor .*"+"/bubble_radius_hor = "+str(BUB_DX[x]*1000.)   +"/'  " +ofile )
os.system( "sed -i 's/bubble_radius_ver .*"+"/bubble_radius_ver = 1500.0 "                 +"/'  " +ofile )
#-----------------------------------------------------------------------------------
# Run the Case
#-----------------------------------------------------------------------------------
#os.system("rm -f "+SAM_DIR+"RCE/snd ")
#snd_file = "BUBBLE/snd.no_wind"
#snd_file = snd_file+".qsig_"+qsigstr+".tsig_"+tsigstr
#print "  snd  = "+snd_file
#snd_file = SAM_DIR+snd_file
#CMD = "cp "+snd_file+"  "+SAM_DIR+"RCE/snd"
#print CMD
#os.system(CMD)

# Specify grd file based on dx
# grd_file = "RCE/grd_"+str(dx)+"dx"
# print "  grd  = "+grd_file
# grd_file = SAM_DIR+grd_file
# CMD = "cp "+grd_file+"  "+SAM_DIR+"RCE/grd"
# print CMD
# os.system(CMD)

sfc_file = SAM_DIR+"RCE/sfc"
file = open(sfc_file,'w') 
file.write("       day    sst(K)    H(W/m2)   LE(W/m2) TAU(m2/s2) \n")
file.write("     0.000    "+sst_str+"        0.0        0.0        0.0 \n")
file.write("   999.000    "+sst_str+"        0.0        0.0        0.0 \n")
file.close() 

if RUN == True:
    print datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    CMD = "time mpirun -n "+str(nproc)+" ./"+SAM_EXE+"  >  "+LOG_DIR+CASE+".out"
    #CMD = "time mpirun -n "+str(nproc)+" ./"+SAM_EXE+" "
    print CMD
    os.system(CMD)
    
    print
    print datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    print "Finished running case: "+CASE+"  ( "+LOG_DIR+CASE+".out )"
#-----------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------
# Process the output
#-----------------------------------------------------------------------------------
if POST == True:
    print "Post-processing "+CASE+" (",datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),")"
    print
    print
    #--------------------------------------
    # STAT output
    #--------------------------------------
    for f in os.listdir(SAM_DIR+"OUT_STAT/"):
        if f.endswith(".stat") and (CASE in f): 
            CMD = UTL_DIR+"stat2nc "+SCR_DIR+"OUT_STAT/"+f+"  >  "+LOG_DIR+"post."+CASE+".stat.out"
            print CMD
            os.system(CMD)
    #--------------------------------------
    # LPT output
    #--------------------------------------
    # for f in os.listdir(SAM_DIR+"OUT_LPT/"):
    #     if f.endswith(".bin") and (CASE in f): 
    #         CMD = UTL_LPT+"bin2nc_lpt "+SCR_DIR+"OUT_LPT/"+f+"  >  "+LOG_DIR+"post."+CASE+".lpt.out"
    #         print CMD
    #         os.system(CMD)
    #--------------------------------------
    # 2D output
    #--------------------------------------
    for f in os.listdir(SAM_DIR+"OUT_2D/"):
        if f.endswith(".2Dcom") and (CASE in f): 
            CMD = UTL_DIR+"2Dcom2nc "+SCR_DIR+"OUT_2D/"+f+"  >  "+LOG_DIR+"post."+CASE+".2D.out"
            print CMD
            os.system(CMD)
    #--------------------------------------
    # 3D output (normal & tet. entrainment)
    #--------------------------------------
    # logfile = LOG_DIR+"post."+CASE+".3D.out"
    # os.system("rm "+logfile)
    # os.system("touch "+logfile)

    # #SCR_DIR = "/home/whannah/Data/SAM/"+CASE+"/"

    # print
    # print "  3D output log file: "+logfile
    # for f in os.listdir(SCR_DIR+"OUT_3D/"):
    #     if f.endswith(".com3D") and not os.path.isfile(SCR_DIR+"OUT_SAM_DIR3D/"+f.replace(".com3D",".nc")) and (CASE in f):
    #         CMD = UTL_DIR+"com3D2nc "+SCR_DIR+"OUT_3D/"+f+"  >>  "+logfile
    #         #print CMD
    #         os.system(CMD)
    #     if f.endswith(".com3D") and not os.path.isfile(SCR_DIR+"OUT_3D/"+f.replace(".com3D",".nc")) and ("ENTRAIN" in f):
    #         CMD = UTL_DIR+"com3D2nc_alt "+SCR_DIR+"OUT_3D/"+f+"  >>  "+logfile
    #         #print CMD
    #         os.system(CMD)
#-----------------------------------------------------------------------------------
# Delete the 3D binary files (to save space)
#-----------------------------------------------------------------------------------
# if CLEAR == True:
#     CMD = "rm -f "+SCR_DIR+"OUT_3D/*"+CASE_TAIL+"*com3D"
#     print CMD
#     os.system(CMD)
#-----------------------------------------------------------------------------------
# Move to permanant output directory
#-----------------------------------------------------------------------------------
if MOVE == True:
    print "-----------------------------------------------------------------------------------"
    print "  Creating directory..."

    tdir = home+"/Data/SAM/"+CASE
    if not os.path.exists(tdir):
        os.makedirs(tdir)

    tdir = home+"/Data/SAM/"+CASE+"/OUT_STAT/"
    if not os.path.exists(tdir):
        os.makedirs(tdir)

    tdir = home+"/Data/SAM/"+CASE+"/OUT_STAT/"
    if not os.path.exists(tdir):
        os.makedirs(tdir)
    
    print "  Moving data..."

    for f in os.listdir(SAM_DIR+"OUT_STAT/"):
        #if f.endswith(".nc") and (CASE in f): 
        if (CASE in f): 
            CMD = "mv "+SCR_DIR+"OUT_STAT/"+f+"  ~/Data/SAM/"+CASE+"/OUT_STAT/"
            print CMD
            os.system(CMD)

    for f in os.listdir(SAM_DIR+"OUT_2D/"):
        #if f.endswith(".nc") and (CASE in f): 
        if (CASE in f): 
            CMD = "mv "+SCR_DIR+"OUT_2D/"+f+"  ~/Data/SAM/"+CASE+"/OUT_2D/"
            print CMD
            os.system(CMD)
    print "-----------------------------------------------------------------------------------"
#     logfile = LOG_DIR+"mv."+CASE+".out"
#     CMD = 'ncl \'case_stub="'+CASENAME+'"\' \'case_tail="'+CASE_TAIL+'"\'  '+SAM_DIR+'../move_data_LP.ncl  > '+logfile
#     print
#     print CMD
#     os.system(CMD)
# print
#========================================================================================================================
#========================================================================================================================


